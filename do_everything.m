%%% This is the main file. You will use it to run the bacteria tracking software on a sequence of images. Just call it from your matlab interpreter. It will read the configuration file, run, and the results will be loaded in your workspace. Further, it will save as text files all the velocities, angles, and positions of the various bacteria it found throughout the images. 

config = load_configuration;

%%start_parallel(config.number_of_workers);

%% Get all the angles and center points. 
[all_bacteria_paths,...
 path_lengths, ...
 all_frame_centers, ...
 all_frame_angles, ...
 all_frame_line_points, ...
 all_frame_bacteria_pixels, ...
 all_frame_densities] = call_process_folder(config.image_folder, config.how_many_files_to_process);

save_angles(fullfile(config.output_folder, config.angle_filename), all_bacteria_paths);
save_velocities(fullfile(config.output_folder, config.velocity_filename), all_bacteria_paths);
save_cm_points(config.output_folder, all_bacteria_paths);

%% Get the optical flow data.
data = run_optical_flow();

%% We must disregard the last frame, since the optical flow works with pairs of frames, and the last frame is unpaired.
[density_histogram, frame_indices] = histc(all_frame_densities(1:end-1), config.density_bins);


correlations = cell(1,length(density_histogram)-1);
for i = 1:(length(density_histogram)-1)
  if density_histogram(i) > 0
    frames = find(frame_indices == i);

    [average_correlations, average_correlations_by_neighbor] = average_correlation_in_sequence(all_bacteria_paths, frames);

    aac = average_correlations{1};
    avc = average_correlations{2};
    aacn = average_correlations_by_neighbor{1};
    avcn = average_correlations_by_neighbor{2};
    edges = linspace(0,1024*sqrt(2), config.correlation_bin_num + 1);
    edges = edges(1:end-1);

    correlations{i}.density = config.density_bins(i);
    
    correlations{i}.aac = aac;
    correlations{i}.aacn = aacn;
    correlations{i}.avc = avc;
    correlations{i}.avcn = avcn;
    correlations{i}.edges = edges;

    
    temp_data = data;
    temp_data.nFrames = length(frames);
    temp_data.pu = temp_data.pu(frames,:,:);
    temp_data.pv = temp_data.pv(frames,:,:);

    %% This will let you take the correlations of optical flow velocities only for blocks
    %% for which there were actually bacteria in the image. 
    mask = zeros(size(temp_data.pu));    
    for j = 1:length(frames)
      mask(j,:,:) = get_low_resolution_mask(frames(j));
    end    
    indices_to_take = find(mask > 0);
    
    
    [inSpeed, inDir, inVort, inSpeedW, inDirW, inVortW, ttt] = get_data_for_space_corr(temp_data, indices_to_take);
  
    correlations{i}.inDir = linearly_interpolate_zeros(inDir);    
    correlations{i}.inSpeed = linearly_interpolate_zeros(inSpeed);
    correlations{i}.inVort = linearly_interpolate_zeros(inVort);    
    correlations{i}.inDirW = linearly_interpolate_zeros(inDirW);
    correlations{i}.inSpeedW = linearly_interpolate_zeros(inSpeedW);
    correlations{i}.inVortW = linearly_interpolate_zeros(inVortW);

    [time_inSpeed, time_inDir, time_inVort, time_ttt] = get_data_for_time_corr(temp_data);

    correlations{i}.time_inSpeed = linearly_interpolate_zeros(time_inSpeed);
    correlations{i}.time_inVort = linearly_interpolate_zeros(time_inVort);    
    correlations{i}.time_inDir = linearly_interpolate_zeros(time_inDir);
    
  else
    correlations{i} = {};
  end  
end

save_correlations(correlations);
save_velocity_histograms_for_optical_flow(density_histogram, frame_indices, data);

out_filename = fullfile(config.output_folder, strrep([datestr(now) '.mat'], ':', '_'));
save(out_filename);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Drawing gifs.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Here are some examples of how to draw gifs.

%% This tracks all the bacteria in the first frame.
%%draw_frame_gif(1, fullfile(config.output_folder, 'bactrack.gif'), all_bacteria_paths, config.image_folder, all_frame_centers, all_frame_line_points, all_frame_bacteria_pixels, true);

%% This tracks all the bacteria which were successfully tracked across ALL the pictures.
%% idx = find(path_lengths >= max(path_lengths));
%% draw_path_gif(idx, fullfile(config.output_folder, 'bactrack.gif'), all_bacteria_paths, config.image_folder, all_frame_centers, all_frame_line_points, all_frame_bacteria_pixels, true);
