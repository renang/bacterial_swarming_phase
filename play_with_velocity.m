close all;

%% For the velocity of a single frame:
frame_num = 5;
v = frame_velocity(frame_num, all_bacteria_paths);
%% X vs Y plot:
figure; plot(v(:,1), v(:,2), '.');
xlim([min(v(:,1)) max(v(:,1))]);
ylim([min(v(:,2)) max(v(:,2))]);
%% Magnitude histogram:
mag =  sqrt(sum(v.*v,2));
figure; hist(mag,30);
xlim([min(mag) max(mag)]);

%% It's a bit boring to look just at integers, so you can add some noise.
%% (this heavily distorts the distribution for small sizes, yes?)
v = v + rand(size(v,1),2) - 0.5;
figure; plot(v(:,1), v(:,2), '.');
xlim([min(v(:,1)) max(v(:,1))]);
ylim([min(v(:,2)) max(v(:,2))]);
mag =  sqrt(sum(v.*v,2));
figure; hist(sqrt(sum(v.*v,2)),30);
xlim([min(mag) max(mag)]);

close all;

%% For an individual:
long_paths = find(path_lengths > 30,20)';
for b = long_paths
  v = all_bacteria_paths{b}.velocities;
  mag =  sqrt(sum(v.*v,2));
  figure;
  title(['Path number ' num2str(b)]);
  hold on; plot(mag,'b'); plot(mag, '.r'); hold off;
end

