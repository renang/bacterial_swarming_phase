%%% This function draws an animated gif which shows some specified tracked bacteria. You can only call it after you have done all the bacteria tracking and processing.
%%% Parameters:
%%% bacteria_indices - these are the indices of the bacteria which you will track. By indices, we mean "indices in the parameter all_bacteria_paths", which is also given to the function.
%%% gif_filename - this is the output filename, e.g 'bacteria.gif'.
%%% all_bacteria_paths - this is a cell array of all the bacteria paths, as returned by call_process_folder.
%%% input_folder - the name of the folder which contains all the bacteria images. This should be the same folder you gave the program to process.
%%% all_frame_centers, all_frame_line_points, all_frame_bacteria_pixels - these should all come from the result of call_process_folder.
%%% should_draw_rest - this tells you whether or not to draw the center of mass of the other bacteria, the ones you don't track.
%%% What do we mean by "tracking" in the gif? Tracked bacteria will be drawn in a different color than the other bacteria, and their trail (the history of their entire movement) will also be displayed. 
function draw_path_gif(bacteria_indices, gif_filename, all_bacteria_paths, input_folder, all_frame_centers, all_frame_line_points, all_frame_bacteria_pixels, should_draw_rest)

  filenames = get_all_image_filenames_from_folder(input_folder);
  
  if size(bacteria_indices,1) > size(bacteria_indices,2)
    bacteria_indices = bacteria_indices';
  end

  if ~exist('should_draw_rest', 'var')
    should_draw_rest = false;
  end

  %% Let us be able to give empty indices, in which case we just draw the pictures.
  if isempty(bacteria_indices)
    first_frame_min = 1; 
    last_frame_max = length(all_frame_centers);
  else
  
    all_first_frames = [];
    all_last_frames = [];
    for b = bacteria_indices
      all_first_frames(end+1) = all_bacteria_paths{b}.first_frame;
      all_last_frames(end+1) = all_bacteria_paths{b}.last_frame;
    end

    first_frame_min = min(all_first_frames);
    last_frame_max = max(all_last_frames);
  end

  circle_inserter = vision.ShapeInserter('Shape', 'Circles', 'Fill', true, 'FillColor', 'Custom','CustomFillColor',uint8([255 0 0]));
  line_inserter = vision.ShapeInserter('Shape', 'Lines', 'BorderColor', 'custom', 'CustomBorderColor', uint8([0 0 255]));
 
  
  figure(1);
  disp(['First frame: ' num2str(first_frame_min) '; last frame: ' num2str(last_frame_max)]);
  for f = first_frame_min:last_frame_max
    disp(['Currently on frame number ' num2str(f)]);
    im = imcomplement(imread(filenames{f}));
    %%[im, bw] = get_clean_bw_image(im);
    colored = uint8(zeros(size(im,1), size(im,2), 3));
    colored(:,:,1) = im;
    colored(:,:,2) = im;
    colored(:,:,3) = im;

    if should_draw_rest
      %% You can control how to draw all the bacteria using the flag parameters at the end - whether or not you want to draw the center of mass, the lines, or the actual body of the found bacteria.
      colored = create_colored_bacteria_image(all_frame_centers{f}, all_frame_line_points{f}, all_frame_bacteria_pixels{f}, im, false,true,false);
    end

    for bacteria_index = bacteria_indices
      first_frame_num = all_bacteria_paths{bacteria_index}.first_frame;
      last_frame_num = all_bacteria_paths{bacteria_index}.last_frame;
      if f >= first_frame_num && f <= last_frame_num
	cm_points = all_bacteria_paths{bacteria_index}.cm_points;
	angles = all_bacteria_paths{bacteria_index}.angles;
	frame_to_draw = f - first_frame_num + 1;

      	%% Draw the current center of mass
	colored = step(circle_inserter, colored, int32([cm_points(frame_to_draw,:) 3]));

	%% Draw the track up to here.
	if f - first_frame_num > 1
	  current_path = int32(cm_points(1:frame_to_draw,:));
	  l = size(current_path,1) * size(current_path,2);
	  colored = step(line_inserter, colored, int32(reshape(current_path', 1, l)));
       end
      end
    end
      
    imshow(colored);
    drawnow;

    gif_frame = getframe(1);
    imframe = frame2im(gif_frame);  
    [imind,cm] = rgb2ind(imframe,256);

    if f == first_frame_min
      imwrite(imind,cm, gif_filename','gif', 'DelayTime', 0.1, 'Loopcount',inf);
    else
      imwrite(imind,cm, gif_filename,'WriteMode','append', 'DelayTime', 0.1);
    end
  
  end

end