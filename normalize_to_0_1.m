function n = normalize_to_0_1(v)
  n = v - min(v);
  n = n / max(n);
end