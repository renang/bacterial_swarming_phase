function component_image = prune_component_by_skeleton(component_image)  
  %% Note: sometimes, especially in T junctions, you cut the wrong side of the T. Can you try to fix this? (maybe using the skeleton of the original component?)
  %% TODO: sometimes you accidentally cut bacteria in half, leaving a small and large part. Is there a way to find this out, and reconnect in this case?

  %% NOTE: you can decide if you want to keep this part or remove it. It increases runtime by about 20-30 seconds, which is quite a lot. However, it increases the accuracy when cutting.
  %% Some small spurs are made better when given larger borders. This fixes up some bad slicings, and doesn't seem to cause harm to good slicings.
  %% big_image = zeros(size(component_image) * 3);
  %% big_image( (size(component_image,1)+1):2*size(component_image,1), (size(component_image,2)+1):2*size(component_image,2)) = component_image;
  %% component_image = big_image;  
  
  outer_medial = bwmorph(imcomplement(component_image),'skel', Inf);
  no_spur = bwmorph(outer_medial, 'spur',1);
  no_spur2 = bwmorph(outer_medial, 'spur',2);
  spurs = outer_medial - no_spur;
  spurs2 = outer_medial - no_spur2;


  %% Ignore corner spurs: these are corners of size 1. Although sometimes we do want to act upon them, they appear to be more often than not false positives; in which case, we cut up bacteria when we don't want to. It's better not to cut and let the rectangle k-means fitting algorithm to take case of those parts instead. 
  %% Corner spurs will have only 1 spur when requesting the 2-spurs of the skeleton. You can use this to detect them.  
  spur_indices = find(spurs == 1);
  spurs2_indices = find(spurs2 == 1);
  no_isolated_points = bwmorph(spurs2, 'clean');  
  spur_indices = intersect(spur_indices, find(no_isolated_points == 1));

  white_line_inserter = vision.ShapeInserter('Shape', 'Lines', 'BorderColor', 'White');
  black_line_inserter = vision.ShapeInserter('Shape', 'Lines');
  
  
  [spur_y spur_x] = ind2sub(size(component_image), spur_indices);
 
  
  should_continue = ~isempty(spur_indices);

  was_empty = isempty(spur_indices);
  
  while should_continue
    should_continue = false;

    distances = pdist2([spur_x spur_y], [spur_x spur_y]);
    distances(logical(eye(size(distances)))) = Inf;
    %% Take the smallest available distance. But only do so if the line passes through the component_image.
    d_flat = distances(:);
    [sorted_d indices] = sort(d_flat);
    %% Everything appears twice (except for the infs, but we don't care about those). You can afford to delete the extra indices.
    indices = indices(1:2:end)';
    for index = indices
      [i j] = ind2sub(size(distances), index);
            
      p1 = [spur_x(i), spur_y(i)];
      p2 = [spur_x(j), spur_y(j)];

      %% Check that they go through the component image.
      line_image = zeros(size(component_image));
      line_image = step(white_line_inserter, line_image, int32([p1 p2]));
      does_go_through_component = any(any(line_image & component_image));

      %% Check if they don't intersect medial axis line (they shouldn't)
      does_go_through_skeleton = any(any(bwmorph(line_image, 'diag') & no_spur));

      %% TODO: additional constraint: look at the direction of the spur (it necessarily has a direction because we do not have corner spurs). Take only pairs of points which staisfy that at least one of the members is in the half plane defined by the direction of the other.
      
      if does_go_through_component && ~does_go_through_skeleton
	%% Success!
	component_image = step(black_line_inserter, component_image, int32([p1 p2]));
	should_continue = true;
	%% Now remove the pairs.
	spur_x([i j]) = [];
	spur_y([i j]) = [];
	break;
      end
    end
  end

  %% It may be that there are "leftover" spurs - ones that werenot paired up because they need to go in some direction to the edge, instead of to another spur. Deal with them!
  if ~isempty(spur_x)
    no_spur_indices = find(no_spur == 1);
    [no_spur_y no_spur_x] = ind2sub(size(component_image), no_spur_indices);

    for i = 1:size(spur_x,1)
      %% Not that if you simple "draw from the spur until the image border", you can accidentally cut across *several* bacteria, even though the ones "in the back" are fine. You must therefore extend the line only step by step until the first crossing. 
      %% Find the closest (say) 3-5 medial line points to the spur, and try continuing from there. 
      temp = repmat([spur_x(i) spur_y(i)], size(no_spur_x,1),1);
      distances = sum((temp-[no_spur_x no_spur_y]) .^ 2,2);
      [sorted_d indices] = sort(distances);
      %% We may not even have three neighbors...
      index_index = min(5, size(no_spur_x,1));
      p = [ mean(no_spur_x(indices(1:index_index))) mean(no_spur_y(indices(1:index_index)))];
      diff_vector = [spur_x(i) spur_y(i)] - p;
      diff_vector = diff_vector / norm(diff_vector);

      %% Go in this direction until you hit the edge of the image.
      current_color = 0;
      did_switch = 0;
      should_continue = true;
      current_pos = [spur_x(i) spur_y(i)];
      while should_continue
      	current_pos = current_pos + diff_vector;
      	rounded_x = int32(current_pos(1));
      	rounded_y = int32(current_pos(2));

	[rounded_x, rounded_y];
	
	%% Check that you are within bounds:
	if rounded_x <= 1 || rounded_y <= 1 || rounded_x >= size(component_image, 2) || rounded_y >= size(component_image, 1)
	  should_continue = false;	
	else
	  if current_color == 0 && component_image(rounded_y,rounded_x) == 1
	    current_color = 1;
	  end
	  if current_color == 1 && component_image(rounded_y,rounded_x) == 0
	    should_continue = false;
	  end	  
	end
      end

      %% The variables rounded_x and rounded_y should always be legal image coordinates at this point! Because we check for out of bounds on the border, not outside the border (and we never get prune points on the border, so we cannot go outside in a single step with current_pos).
      component_image = step(black_line_inserter, component_image, int32([spur_x(i) spur_y(i) rounded_x rounded_y]));   
    end
  end

  %% if ~was_empty  
  %%   tempim = imoverlay(component_image, outer_medial, [1 .3 .3]);
  %%   %%tempim = imoverlay(tempim, skel, [.3 .3 1]);
  %%   h = figure; imshow(tempim);
  %%   set(h, 'Position', [1 1 1424 860])
  %%   waitforbuttonpress;
  %% end

  %% component_image = component_image((size(component_image,1)/3+1):2*size(component_image,1)/3, (size(component_image,2)/3+1):2*size(component_image,2)/3);
  
end
