%%% This function saves *all* the velocities of all the bacteria found in a sequence of images. The output is a text file with two colums: the first is the x velocity, the second is the y velocity. 
%%% filename - the file to which the velocity data will be saved.
%%% all_bacteria_paths - a cell array of bacteria path structures (as returned by process_folder).
function save_velocities(filename, all_bacteria_paths)
  fid = fopen(filename, 'w');
  fprintf(fid, 'v_x\tv_y\n');
  for i = 1:length(all_bacteria_paths)
    if ~isempty(all_bacteria_paths{i}.velocities)
      fprintf(fid, '%f\t%f\n', all_bacteria_paths{i}.velocities');
    end
  end    
  fclose(fid);
end
  