function colored_bacteria = create_colored_bacteria_image(centers_of_mass, all_bacteria_line_points, all_bacteria_pixels, background_image, should_draw_lines, should_draw_cm, should_draw_bacteria)

  if ~exist('should_draw_lines', 'var')
    should_draw_lines = true;
    end

  if ~exist('should_draw_cm', 'var')
    should_draw_cm = true;
  end

  if ~exist('should_draw_bacteria', 'var')
    should_draw_bacteria = true;
  end
  
  colored_bacteria = uint8(zeros(size(background_image,1), size(background_image,2), 3));
  colored_bacteria(:,:,1) = background_image;
  colored_bacteria(:,:,2) = background_image;
  colored_bacteria(:,:,3) = background_image;

  if should_draw_bacteria 
    colored_bacteria = colored_bacteria + 3*draw_component_decomposition(all_bacteria_pixels, background_image);   
  end

  yellow_circle_inserter = vision.ShapeInserter('Shape', 'Circles', 'Fill', true, 'FillColor', 'Custom','CustomFillColor',uint8([255 255 0]));
  line_inserter = vision.ShapeInserter('Shape', 'Lines', 'BorderColor', 'custom', 'CustomBorderColor', uint8([0 255 0]));
  
  for i = 1:length(all_bacteria_line_points)
    if should_draw_cm
      colored_bacteria = step(yellow_circle_inserter, colored_bacteria, int32([centers_of_mass(i,:) 3]));
    end

    if should_draw_lines
      p = all_bacteria_line_points{i};
      p1 = p(1); p2 = p(2);
      [y1 x1] = quick_2d_ind2sub(size(background_image), p1);
      [y2 x2] = quick_2d_ind2sub(size(background_image), p2);
      colored_bacteria = step(line_inserter, colored_bacteria, int32([x1 y1 x2 y2]));
    end
      
  end

end