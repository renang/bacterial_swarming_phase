function [line_x, line_y] = line_from_point_and_angle(center_x, center_y, theta, line_length, component_image)
  line_half_length = line_length / 2;

  line_x = center_x + cos(theta) * (-ceil(line_half_length):ceil(line_half_length));
  line_y = center_y + sin(theta) * (-ceil(line_half_length):ceil(line_half_length));

  line_x = floor(line_x);
  line_y = floor(line_y);

  %% We don't want to go outside the boundaries of the picture.
  bad_indices = find(line_x < 1);
  line_x(bad_indices) = [];
  line_y(bad_indices) = [];
  bad_indices = find(line_y < 1);
  line_x(bad_indices) = [];
  line_y(bad_indices) = [];
  bad_indices = find(line_x > size(component_image,2));
  line_x(bad_indices) = [];
  line_y(bad_indices) = [];
  bad_indices = find(line_y > size(component_image,1));
  line_x(bad_indices) = [];
  line_y(bad_indices) = [];
end
  