function save_velocity_histograms_for_optical_flow(density_histogram, frame_indices, data)
  config = load_configuration();
  for i = 1:(length(density_histogram)-1)
    if density_histogram(i) > 0
      density = config.density_bins(i);
      
      frames = find(frame_indices == i);

      %% Mask out velocities for bacteria which aren't really there, as determined
      %% by the thresholding of the resized bw image.
      mask = zeros(size(data.pu));      
      for frame = frames
	mask(frame,:,:) = get_low_resolution_mask(frame);
      end
      indices_to_take = find(mask > 0);

      pu = data.pu(indices_to_take);
      pv = data.pv(indices_to_take);

      [hist_pu] = histc(pu, config.optical_flow_velocity_bins);
      [hist_pv] = histc(pv, config.optical_flow_velocity_bins);
      normalized_hist_pu = hist_pu ./ sum(hist_pu);
      normalized_hist_pv = hist_pv ./ sum(hist_pv);
      
      s = ['optical_flow_xy_velocity_density_' num2str(density, '%0.4f') '.txt'];
      output_filename = fullfile(config.output_folder, s);
      fid = fopen(output_filename, 'w');
      fprintf(fid, 'velocity\tx count\ty count\tnormalized x\tnormalized y\n');
      fprintf(fid, '%f\t%f\t%f\t%f\t%f\n', [config.optical_flow_velocity_bins', hist_pu, hist_pv, normalized_hist_pu, normalized_hist_pv]');
      fclose(fid);

      angles = atan2(pv, pu);      
      hist_angles = histc(angles, config.optical_flow_angle_bins);
      normalized_hist_angles = hist_angles ./ sum(hist_angles);

      magnitudes = sqrt(pu.*pu + pv.*pv);
      hist_magnitudes = histc(magnitudes, config.optical_flow_magnitude_bins);
      normalized_hist_magnitudes = hist_magnitudes ./ sum(hist_magnitudes);
      
      s = ['optical_flow_polar_velocity_density_' num2str(density, '%0.4f') '.txt'];
      output_filename = fullfile(config.output_folder, s);
      fid = fopen(output_filename, 'w');
      fprintf(fid, 'angle\tangle count\tnormalized angle count\n');
      
      fprintf(fid, '%f\t%f\t%f\n',[config.optical_flow_angle_bins', hist_angles,normalized_hist_angles]');

      fprintf(fid, '\n\nmagnitude\tmagnitude count\tnormalized magnitude count\n');
      fprintf(fid, '%f\t%f\t%f\n', [config.optical_flow_magnitude_bins',hist_magnitudes,normalized_hist_magnitudes]');
      fclose(fid);
    end
  end
end
