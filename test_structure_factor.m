function [power_law_fit] = test_structure_factor(io_struct, frames_data)
  tic;
  config = load_configuration();
  test_name = config.structure_factor_test_name;;
  display(['Now testing ' test_name]);
  create_directory_or_throw(io_struct.base_output_directory, test_name);
  create_directory_or_throw(fullfile(io_struct.base_output_directory, test_name), num2str(io_struct.bacteria_size));
  test_directory = fullfile(io_struct.base_output_directory, test_name, num2str(io_struct.bacteria_size));

  config = load_configuration();
  magnitude_range = config.magnitude_range;

  mag_structure_func = zeros(1, length(magnitude_range));
  for i = 1:length(magnitude_range)
    fprintf(['\r' num2str(i) '/' num2str(length(magnitude_range))]);
    magnitude = magnitude_range(i);
    mag_structure_func(i) = structure_func_for_magnitude(magnitude, frames_data.frame_centers_of_mass);
  end
  display(' ');
  
  figure;
  %% Fit an exponential plot
  f = fit(magnitude_range', mag_structure_func', 'exp1');
  hold on
  plot(magnitude_range, f.a*(exp(magnitude_range .* f.b)), 'red', 'linewidth',2);
  plot(magnitude_range, mag_structure_func, 'bx', 'linewidth',2);
  hold off
  xlabel('distance, in pixels');
  ylabel('Structure func magnitude');
  title('Structure func');
  grid
  legend(['s(r)=' num2str(f.a) 'e^{' num2str(f.b) '*x}'], 'Data','Location','ne')
  output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' exponential.png']);
  print(output_filename, '-dpng');
  close();
  text_output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' exponential.txt']);
  if size(magnitude_range,1)>size(magnitude_range,2)
    ddd=[magnitude_range,  mag_structure_func, f.a*(exp(magnitude_range .* f.b))];
  else
    ddd=[magnitude_range',  mag_structure_func', f.a*(exp(magnitude_range' .* f.b))];
  end
  save(text_output_filename,'ddd','-ascii');

  
  figure;
  %plot on a semilog
  hold on
  plot(magnitude_range, log10(f.a*(exp(magnitude_range .* f.b))), 'red', 'linewidth',2);
  plot(magnitude_range, log10(mag_structure_func), 'bx', 'linewidth',2);
  hold off
  xlabel('distance, in pixels');
  ylabel('log_{10} (Structure func magnitude)');
  title('Structure func');
  grid
  legend(['s(r)=' num2str(f.a) 'e^{' num2str(f.b) '*x}'], 'Data','Location','ne')
  output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' semilog.png']);
  print(output_filename, '-dpng');
  close();
  text_output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' semilog.txt']);
  if size(magnitude_range,1)>size(magnitude_range,2)
    ddd=[magnitude_range,  log10(mag_structure_func), log10(f.a*(exp(magnitude_range .* f.b)))];
  else
    ddd=[magnitude_range',  log10(mag_structure_func)', log10(f.a*(exp(magnitude_range' .* f.b)))];
  end
  save(text_output_filename,'ddd','-ascii');
  
  figure;
  %% Fit a powr law
  xxToFit=magnitude_range;
  yyToFit=abs(mag_structure_func);
  mask=~isnan(yyToFit) & xxToFit>0;
  yyToFit=yyToFit(mask);
  xxToFit=xxToFit(mask);
  f = fit(xxToFit', yyToFit', 'a*x^b');
  power_law_fit.a = f.a;
  power_law_fit.b = f.b;
  hold on
  plot(log10(xxToFit), log10(f.a*xxToFit .^ f.b), 'red', 'linewidth',2);
  plot(log10(xxToFit), log10(abs(yyToFit)), 'bx', 'linewidth',2);
  hold off
  xlabel('log_{10} (distance, in pixels)');
  ylabel('log_{10} (Structure func magnitude)');
  title('Structure func');
  grid
  legend(['s(r)=' num2str(f.a) '*x^' num2str(f.b)], 'Data','Location','ne')
  output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' power law.png']);
  print(output_filename, '-dpng');
  close();
  text_output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' power law.txt']);
  if size(xxToFit,1)>size(xxToFit,2)
    ddd=[log10(xxToFit),  log10(abs(yyToFit)), log10(f.a*xxToFit .^ f.b)];
  else
    ddd=[log10(xxToFit)',  log10(abs(yyToFit))', log10(f.a*xxToFit' .^ f.b)];
  end
  save(text_output_filename,'ddd','-ascii');
  
  toc;  
end

