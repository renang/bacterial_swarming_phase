function plot_large_fluctuations_summary_graphs(all_fluctuation_fits, base_output_directory, bacteria_size)
  config = load_configuration();
  mean_slopes = zeros(1, length(config.density_bins)-1);
  
  for i = 1:length(config.density_bins)-1
      density = config.density_bins(i);
      
      density_indices = find(arrayfun(@(RIDX) all_fluctuation_fits{RIDX}.density == density, 1:length(all_fluctuation_fits)));
      if ~isempty(density_indices)
	fits_of_this_density = all_fluctuation_fits(density_indices);
	fit_param = extractfield([fits_of_this_density{:}], 'fit');
	mean_slopes(i) = mean(fit_param);
      end
  end

  figure;
  plot(config.density_bins(1:end-1), mean_slopes, '.-');
  title(['Mean slope values for ' config.large_fluctuations_test_name], 'Interpreter', 'None');
  ylabel('Mean slope value of power law fit');
  xlabel('Density');
  output_filename = fullfile(base_output_directory, config.large_fluctuations_test_name, [num2str(bacteria_size) '_summary_mean_slope.png']);
  print(output_filename, '-dpng');
  close();

  text_output_filename = fullfile(base_output_directory, config.large_fluctuations_test_name, [num2str(bacteria_size) '_summary_mean_slope.txt']);
  if size(config.density_bins,1)>size(config.density_bins,2)
    ddd=[config.density_bins(1:end-1),  mean_slopes];
  else
    ddd=[config.density_bins(1:end-1)',  mean_slopes'];
  end
  save(text_output_filename,'ddd','-ascii');
  
end
