function test_bin_statistics(io_struct, all_bin_bacteria_nums, all_bin_unoriented_angle_polar_order_parameter, all_bin_unoriented_angle_nematic_order_parameter, all_bin_oriented_angle_polar_order_parameter, all_bin_oriented_angle_nematic_order_parameter)
  tic;
  config = load_configuration();
  test_names = config.bin_test_names;

  %% Required for Hartigans' test to produce meaningful results; otherwise there are not enough uniques.
  noised_bacteria_nums = all_bin_bacteria_nums + unifrnd(0,1,1,length(all_bin_bacteria_nums));
  
  data_to_plot = {noised_bacteria_nums, all_bin_unoriented_angle_polar_order_parameter, all_bin_unoriented_angle_nematic_order_parameter, all_bin_oriented_angle_polar_order_parameter, all_bin_oriented_angle_nematic_order_parameter};
  bin_sizes = {length(unique(all_bin_bacteria_nums)), 400, 400, 400, 400};

  
  for i = 1:length(test_names)
    test_name = test_names{i};
    display(['Now testing ' test_name]);
    create_directory_or_throw(io_struct.base_output_directory, test_name);
    create_directory_or_throw(fullfile(io_struct.base_output_directory, test_name), num2str(io_struct.bacteria_size));
    test_directory = fullfile(io_struct.base_output_directory, test_name, num2str(io_struct.bacteria_size));
    output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension '.png']);
    save_histogram_with_stats(remove_nans(data_to_plot{i}), bin_sizes{i}, test_name, output_filename);
    
    output_filename_text = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension '.txt']);
    [counts,centers]=hist(remove_nans(data_to_plot{i}), bin_sizes{i});
    counts=counts/sum(counts)/(centers(2)-centers(1));
    ddd=[centers' , counts'];
    save(output_filename_text,'ddd','-ascii');
  end	

  
  for i = 1:(length(test_names)-1)
    for j = (i+1):length(test_names)
      test_name = [test_names{i} '_vs_' test_names{j}];
      display(['Now testing ' test_name]);
      create_directory_or_throw(io_struct.base_output_directory, test_name);
      create_directory_or_throw(fullfile(io_struct.base_output_directory, test_name), num2str(io_struct.bacteria_size));
      test_directory = fullfile(io_struct.base_output_directory, test_name, num2str(io_struct.bacteria_size));

      xdata = data_to_plot{i};
      ydata = data_to_plot{j};

      bad_indices = find(isnan(xdata));
      xdata(bad_indices) = [];
      ydata(bad_indices) = [];
      bad_indices = find(isnan(ydata));
      xdata(bad_indices) = [];
      ydata(bad_indices) = [];
      
      coeffs = polyfit(xdata, ydata, 1);
      yfit = polyval(coeffs, xdata);
      yresid = ydata - yfit;
      SSresid = sum(yresid.^2);
      SStotal = (length(ydata)-1) * var(ydata);
      rsq = 1 - SSresid/SStotal;
      figure;
      hold on
      plot(xdata, ydata, '.');
      plot(xdata, yfit, 'red', 'linewidth',2);
      xlim([0 max(xdata)]);
      ylim([0 max(ydata)]);
      legend('Data', ['y(x)=' num2str(coeffs(1)) 'x + ' num2str(coeffs(2)) ', r^2 = ' num2str(rsq)],'Location','ne')	    
      title(test_name, 'Interpreter', 'None');
      hold off
      output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension '.png']);
      print(output_filename, '-dpng');
      close();
    end
  end
  toc;  
end
