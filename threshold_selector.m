function varargout = threshold_selector(varargin)
% THRESHOLD_SELECTOR MATLAB code for threshold_selector.fig
%      THRESHOLD_SELECTOR, by itself, creates a new THRESHOLD_SELECTOR or raises the existing
%      singleton*.
%
%      H = THRESHOLD_SELECTOR returns the handle to a new THRESHOLD_SELECTOR or the handle to
%      the existing singleton*.
%
%      THRESHOLD_SELECTOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in THRESHOLD_SELECTOR.M with the given input arguments.
%
%      THRESHOLD_SELECTOR('Property','Value',...) creates a new THRESHOLD_SELECTOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before threshold_selector_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to threshold_selector_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help threshold_selector

% Last Modified by GUIDE v2.5 30-Jun-2016 21:40:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @threshold_selector_OpeningFcn, ...
                   'gui_OutputFcn',  @threshold_selector_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function im = get_thresholded_image(handles)
  im = handles.im;
  if handles.threshold_type == 1
    im(handles.im > 255*get(handles.threshold_slider, 'Value')) = 0;
  elseif handles.threshold_type == 2
    max_entropy = max(max(handles.entropy));
    im(handles.entropy < max_entropy*get(handles.threshold_slider, 'Value')) = 0;
  end


function draw_modified_image(handles)  
  if handles.loaded_image
    axes(handles.axes_modified_image);

    im2 = get_thresholded_image(handles);
  
    if get(handles.bw_checkbox, 'Value')
      im2(im2 > 0) = 255;
    end
    imshow(im2);
    axis off;
    axis image;
  end


function update_texts(handles)
  if handles.loaded_image
    if handles.threshold_type == 1
      s = ['Threshold: ' num2str(get(handles.threshold_slider,'Value'))];
    elseif handles.threshold_type == 2
      max_entropy = max(max(handles.entropy));
      s = ['Threshold: ' num2str(max_entropy * get(handles.threshold_slider,'Value'))];
    end
    set(handles.text_threshold_value, 'String', s);

    im = get_thresholded_image(handles);
    s = ['Density: ' num2str(sum(sum(im > 0)) / prod(size(im)))];
    set(handles.text_density, 'String', s);
  end


function draw_initial_images(handles)
  if handles.loaded_image
    axes(handles.original_image_axes);
    imshow(handles.im);
    axis off;
    axis image;

    draw_modified_image(handles);

    update_texts(handles);
    axes(handles.axes_intensity_histogram);
    edges = 0.5 + -1:256;
    spectrum = histc(double(handles.im(:)),edges);
    smoothed_spectrum = smooth(spectrum, 'sgolay');
    [m, ind] = max(smoothed_spectrum);
    smoothed_spectrum = smoothed_spectrum / m;
    plot((edges+0.5)/256,smoothed_spectrum, 'g','linewidth',1.5);
    ylim([-0.05 1]);
    xlim([0 1]);

    axes(handles.axes_entropy_histogram);
    max_entropy = max(max(handles.entropy));
    edges = 0:0.02:ceil(max_entropy);
    spectrum = histc(handles.entropy(:),edges);
    smoothed_spectrum = smooth(spectrum, 'sgolay');
    [m, ind] = max(smoothed_spectrum);
    smoothed_spectrum = smoothed_spectrum / m;
    plot(edges+0.01,smoothed_spectrum, 'r','linewidth',1.5);
    ylim([-0.05 1]);
    xlim([0 max_entropy]);
  end
  

function handles = load_image(hObject, handles, filename)
  handles.im = imread(filename);
  handles.entropy = entropyfilt(handles.im, ones(9));
  handles.loaded_image = true;
  
  
% --- Executes just before threshold_selector is made visible.
function threshold_selector_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to threshold_selector (see VARARGIN)

  handles.loaded_image = false;

  if get(handles.radio_intensity,'Value') == 1
    handles.threshold_type = 1;
  else
    handles.threshold_type = 2;
  end
  
  draw_initial_images(handles);

  
% Choose default command line output for threshold_selector
  handles.output = hObject;

% Update handles structure
  guidata(hObject, handles);

% UIWAIT makes threshold_selector wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = threshold_selector_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function threshold_slider_Callback(hObject, eventdata, handles)
% hObject    handle to threshold_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
  draw_modified_image(handles);
  update_texts(handles)
  
% --- Executes during object creation, after setting all properties.
function threshold_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to threshold_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in button_load_image.
function button_load_image_Callback(hObject, eventdata, handles)
% hObject    handle to button_load_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
  [filename, pathname] = uigetfile('*.tif');
  if ~isequal(filename,0)
    handles = load_image(hObject, handles, [pathname filename]);
    draw_initial_images(handles);
    guidata(hObject, handles);
  end

% --- Executes on button press in bw_checkbox.
function bw_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to bw_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of bw_checkbox
  draw_modified_image(handles);

  
% --- Executes when selected object is changed in uipanel2.
function uipanel2_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel2 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
  if eventdata.NewValue == handles.radio_intensity
    handles.threshold_type = 1;
  elseif eventdata.NewValue == handles.radio_entropy
    handles.threshold_type = 2;
  end
  guidata(hObject, handles);
  draw_modified_image(handles);
  update_texts(handles);
