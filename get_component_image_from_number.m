function single_component = get_component_image_from_number(c, cc, regions, im)
  [x1 y1 x2 y2] = get_bounded_box_coordinates_from_region(regions(c));
  single_component = false(size(im));
  single_component(cc.PixelIdxList{c}) = true;      
  single_component = single_component(y1:y2, x1:x2);
end