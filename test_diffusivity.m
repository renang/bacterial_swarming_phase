function [power_law_fit] = test_diffusivity(io_struct, paths_of_this_density)
  tic;
  config = load_configuration();
  test_name = config.diffusivity_test_name;
  display(['Now testing ' test_name]);
  create_directory_or_throw(io_struct.base_output_directory, test_name);
  create_directory_or_throw(fullfile(io_struct.base_output_directory, test_name), num2str(io_struct.bacteria_size));
  test_directory = fullfile(io_struct.base_output_directory, test_name, num2str(io_struct.bacteria_size));

	
  %% Calculate diffusivity only for paths with length above the median
  this_density_path_lengths = extractfield([paths_of_this_density{:}], 'last_frame') - extractfield([paths_of_this_density{:}], 'first_frame');
  median_length = floor(median(this_density_path_lengths));
  power_law_fit.alpha = 0;
  if median_length >= 2

    above_median_indices = find(this_density_path_lengths >= median_length);
    above_median_paths = paths_of_this_density(above_median_indices);

    mean_square_displacement = zeros(1, median_length-1);
    for i = 1:length(above_median_paths)
      path = above_median_paths{i};
      mean_square_displacement = mean_square_displacement + get_mean_square_displacement_up_to_smax(path, median_length);
    end

    mean_square_displacement = mean_square_displacement / length(above_median_paths);

    %% Fit a linear plot, r(s) = D \cdot s
    srange = 1:(median_length-1);
    p1 = mean_square_displacement/srange;
    %% Fit a power plot
    %f = fit(srange', mean_square_displacement', 'b*x^alpha');
    f = fit(srange(1:end-3)', mean_square_displacement(1:end-3)', 'b*x^alpha');
    power_law_fit.alpha = f.alpha;
    hold on
    plot(srange, p1*srange, 'black', 'linewidth',2);
    plot(srange, f.b*(srange.^(f.alpha)), 'red', 'linewidth',2);
    plot(srange, mean_square_displacement, 'bx', 'linewidth',2);
    hold off
    xlabel('s, in pixels');
    ylabel('Mean square displacement');
    title('Diffusivity');
    grid
    legend(['r(s)=' num2str(p1) 's'], ['r(s)=' num2str(f.b) 's^{' num2str(f.alpha) '}'], 'Data','Location','nw')
    output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension '.png']);
    print(output_filename, '-dpng');
    close();
  end
  toc;
end
