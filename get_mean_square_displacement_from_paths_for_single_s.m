function msds = get_mean_square_displacement_from_paths_for_single_s(paths, path_lengths, s)
  %% Gets the mean square displacement for time difference s. Automatically
  %% filters paths with length < s.
  above_s_indices = find(path_lengths >= s);

  if ~isempty(above_s_indices)
    msds = zeros(1, length(above_s_indices));
    above_s_paths = paths(above_s_indices);
   
    for i = 1:length(above_s_paths)
      path = above_s_paths{i};
      current_sum = 0;
      for t = 1:(length(path.cm_points) - s)
	current_sum = current_sum + sum((path.cm_points(t+s)-path.cm_points(t))^2);
      end

      msds(i) = current_sum / (length(path.cm_points) - s);
    end
  else
    msds = [];
  end

end
