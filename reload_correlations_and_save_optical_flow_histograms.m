function reload_correlations_and_save_optical_flow_histograms(filename)
  load(filename);
  save_velocity_histograms_for_optical_flow(density_histogram, frame_indices, data);
end
