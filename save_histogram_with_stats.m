function save_histogram_with_stats(y, bin_num, label_of_x, output_filename)
  [dip, p_score] = HartigansDipSignifTest(y, 1000);
  figure;
  hist(y, bin_num);
  xlabel(label_of_x, 'Interpreter', 'None');
  ylabel('Counts');
  title({['Probability for unimodal: p = ' num2str(p_score)],
	 ['mean = ' num2str(mean(y))],
	 ['std = ' num2str(std(y))]}, 'Interpreter', 'None');
  print(output_filename, '-dpng');
  close();	
end
