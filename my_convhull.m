%%% the matlab convhull function fails when the points are colinear. This version does not fail in these cases, but instead returns the endpoints of the line. 
function k = my_convhull(x,y)
  if length(x) <= 1
    k = 1:length(x);
    return;
  end

  %% Check if we are colinear.
  is_colinear = true;
  for i = 3:length(x)
    if polyarea([x(1) x(2) x(i)], [y(1) y(2) y(i)]) ~= 0
      is_colinear = false;
      break;
    end
  end  
  
  if is_colinear
    %% First check for straight lines parallel to the y axis.
    if x(1) == x(2)
      [t1,i1] = min(y);
      [t2,i2] = max(y);
      k = [i1 i2];
    else
      %% This should work for both for lines that parallel to the x axis, and for sloped lines.       
      [t1,i1] = min(x);
      [t2,i2] = max(x);
      k = [i1 i2];      
    end
  else
    k = convhull(x,y);
  end
end
