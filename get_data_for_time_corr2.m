function [inSpeed, inDir, inVort, ttt] = get_data_for_time_corr(data, mask)

  data.skipFrames = 1;
  speeds=sqrt(data.pu.^2+data.pv.^2);
  nu=data.pu./speeds;
  nv=data.pv./speeds;
  nfr=data.nFrames;
  maxTime=floor(200/data.skipFrames);
  if maxTime>=nfr
    maxTime=floor(nfr/2/data.skipFrames);
  end

  vort=zeros(nfr,data.vfsize(1),data.vfsize(2));
  for f=1:nfr
    vort(f,:,:)=curl( reshape(data.pu(f,:,:),data.vfsize),reshape(data.pv(f,:,:),data.vfsize) )/data.dx;
  end
   
  inSpeed=zeros(maxTime,1);
  inDir=zeros(maxTime,1);
  inVort=zeros(maxTime,1);

  %% When calculating the mean velocities and vorticity, we need to consider only those
  %% entries corresponding to good indices (as given in the mask input parameter). 
  avSpeed = zeros(size(data.pu,1), 1);
  avU = zeros(size(data.pu,1), 1);
  avV = zeros(size(data.pu,1), 1);
  avC = zeros(size(data.pu,1), 1);
  for i = 1:size(data.pu,1)
    temp_mask = mask(i,:,:);
    good_indices = find(temp_mask);
    temp_speed = speeds(i,:,:);
    temp_u = nu(i,:,:);
    temp_v = nv(i,:,:);
    temp_c = vort(i,:,:);
    
    avSpeed(i) = mean(temp_speed(indices));
    avU(i) = mean(temp_u(good_indices));
    avV(i) = mean(temp_v(good_indices));
    avC(i) = mean(temp_c(good_indices));
  end
  
  
  bases=1:10:nfr/data.skipFrames-maxTime;
  for b=bases
    for fr=1:maxTime

      current_mask = mask(b,:,:);
      next_mask = mask(b+fr,:,:);

      current_good_indices = find(current_mask);
      next_good_indices = find(next_mask);
      
      current_speed = speeds(b,:,:);
      next_speed = speeds(b+fr,:,:);

      current_nu = nu(b,:,:);
      next_nu = nu(b+fr,:,:);
      current_nv = nv(b,:,:);
      next_nv = nv(b+fr,:,:);
      current_vort = vort(b,:,:);
      next_vort = vort(b+fr,:,:);         
      
      inSpeed(fr)=inSpeed(fr)+mean(mean(speeds(b,:,:).*speeds(b+fr,:,:),3),2)-avSpeed(b)*avSpeed(b+fr);
      inDir(fr)=inDir(fr)+mean(mean(nu(b,:,:).*nu(b+fr,:,:)+nv(b,:,:).*nv(b+fr,:,:),3),2)-avU(b)*avU(b+fr)-avV(b)*avV(b+fr);
      inVort(fr)=inVort(fr)+mean(mean(vort(b,:,:).*vort(b+fr,:,:),3),2)-avC(b)*avC(b+fr);
    end
  end
  
  inSpeed=inSpeed/length(bases);
  inSpeed=inSpeed/inSpeed(1);
  inDir=inDir/inDir(1);
  inVort=inVort/inVort(1);
  ttt=((0:maxTime-1)/data.fps*data.skipFrames)';
end
