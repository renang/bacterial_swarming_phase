function s = structure_func_for_magnitude(magnitude, all_frame_centers)
    structure_func_for_frame = zeros(1, length(all_frame_centers));
    for i = 1:length(all_frame_centers)
        frame_centers = all_frame_centers{i};
        frame_distances = dist(frame_centers');
	%% The -1 at the end of this calculation is because of the 0 distance on the diagonal.
        structure_func_for_frame(i) = (sum(sum(sinc(magnitude * frame_distances)))) / length(frame_centers) - 1;
    end
   s = mean(structure_func_for_frame);
   return
end
