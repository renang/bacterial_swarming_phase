function [binned_correlations, distance_histogram, correlations_by_neighbor] = single_bacteria_correlation(bac_index, centers_of_mass, trait, correlation_func)

  x = centers_of_mass(bac_index,1);
  y = centers_of_mass(bac_index,2);  
  dists = pdist2([x y], centers_of_mass);  
  config = load_configuration;

  counter = 0;
  [sorted_distances, idx] = sort(dists);
  correlations_by_neighbor = zeros(1,length(idx));
  for i = idx
    counter = counter + 1;
    correlations_by_neighbor(counter) = correlation_func(bac_index,i,trait);
  end

  %% We want that the way we save it in memory be uniform for all bacteria, so we must bin the distances.
  num_of_bins = config.correlation_bin_num;
  edges = linspace(0,1024*sqrt(2), num_of_bins+1);
  binned_correlations = zeros(1,num_of_bins);
  [distance_histogram, bins] = histc(sorted_distances, edges);
  %% Remove the last element - it's not interesting (b.c of how histc works).
  distance_histogram = distance_histogram(1:end-1);
  
  u = unique(bins);
  for i = 1:num_of_bins
    indices = find(bins == i);
    if ~isempty(indices)
      binned_correlations(i) = mean(correlations_by_neighbor(indices),2);
    end
  end

end
