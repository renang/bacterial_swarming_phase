%%% This function tries to tell whether a connected component in a black and white image is a single bacteria or not. It uses various heuristics to do so. It is never 100% accurate, and some internal values might need to change depending on the types of bacteria you are checking (long / short, etc).
%%% Parameters:
%%% im - the original image from which the component came from.
%%% pixel_list - a list of pixel indices which tell where the connected component is in the original image.
%%% region - a region data structure, as given by regionprops(cc,'basic').
%%% componant_image - an image containing just the component.
function a = is_component_single_bacteria(im, pixel_list, region, component_image)

  %% First check if its too small
  if length(pixel_list) < 20
    a = true;
    return;
  end

  %% Check if the component is convex (convex components are almost always bacteria). 
  if is_component_convex(im, pixel_list, region)
    a = true;
    return
  end

  %% Check the ratio between height and width of bounding box
  indices = find(component_image == 1);
  [y x] = ind2sub(size(component_image), indices);
  bounding_box = min_bounding_box([x y]');

  [p1 p2 p3 p4] = sorted_points_of_bounding_rectangle(bounding_box);
 
  l1 = p3 - p1;
  l2 = p4 - p2;
  
  mmax = max(norm(l1), norm(l2));
  mmin = min(norm(l1), norm(l2));

  %% TYPE = 1; % SHORT
  %% %TYPE = 2; % LONG

  %% if TYPE == 1
  %%   multiplier = 5;
  %% else
  %%   multiplier = 5;
  %% end
  
  %% TODO: differentiate between short and long ones!
  if mmax > 5 * mmin
    a = true;
  else
    a = false;
  
end