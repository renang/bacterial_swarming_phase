function frames_data = prepare_frames_test_data(frames, frame_indices, density_index, file_data)
  frames = find(frame_indices == density_index);

  display('Prcessing frames');
  frame_centers_of_mass = cell(1, length(frames));
  frame_angles = cell(1, length(frames));
  frame_velocities = cell(1, length(frames));
  all_relevant_velocities = [];	
  for i = 1:length(frames)
    frame_number = frames(i);
    fprintf(['\r' num2str(i) '/' num2str(length(frames))]);
    [centers_of_mass, angles, velocities, path_lengths] = frame_data_from_paths(frame_number, file_data.all_bacteria_paths);
    frame_centers_of_mass{i} = centers_of_mass;
    frame_angles{i} = angles;
    frame_velocities{i} = velocities;
    all_relevant_velocities = cat(1, all_relevant_velocities, frame_velocities{i});
  end
  display(' ');
  where_are_nans = isnan(all_relevant_velocities);
  where_are_nans = where_are_nans(:,1);
  all_relevant_velocities(find(where_are_nans), :) = [];

  frames_data.number_of_frames = length(frames);
  frames_data.frame_centers_of_mass = frame_centers_of_mass;
  frames_data.frame_angles = frame_angles;
  frames_data.frame_velocities = frame_velocities;
  frames_data.all_relevant_velocities = all_relevant_velocities;
end
