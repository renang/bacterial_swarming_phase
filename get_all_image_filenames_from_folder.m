function filenames = get_all_image_filenames_from_folder(foldername)
  all_image_files = dir([foldername '/*.tif']);
  num_of_files = length(all_image_files);
  filenames = {};
  [filenames{1:length(all_image_files), 1}] = deal(all_image_files.name);
  filenames = sortrows(filenames);
  for i = 1:length(filenames)
    filenames{i} = fullfile(foldername, filenames{i});
  end
end