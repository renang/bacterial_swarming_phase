function bacteria_num_variances = prepare_large_fluctuations_test_data(frames_data)
  config = load_configuration();
  bacteria_num_variances = zeros(1, length(config.large_fluctuations_bin_sizes));
  for grid_size_index = 1:length(config.large_fluctuations_bin_sizes)    
    grid_size = config.large_fluctuations_bin_sizes(grid_size_index);    
    mn = [0 0]; mx = [1024 1024];  % mn = min possible of centers; mx = max possible of centers
    edges = linspace(mn(1), mx(1), grid_size+1);
    frame_bacteria_nums = zeros(frames_data.number_of_frames, grid_size, grid_size);

    variances_for_this_grid_size = zeros(1, frames_data.number_of_frames);
    
    for i = 1:frames_data.number_of_frames
      [~,subs] = histc(frames_data.frame_centers_of_mass{i}, edges, 1); 
      subs(subs==grid_size+1) = grid_size;
      for j = 1:grid_size
	for k = 1:grid_size
	  indices_of_this_bin  = find(all(bsxfun(@eq, subs, [j,k]), 2));
	  frame_bacteria_nums(i,j,k) = length(indices_of_this_bin);
	end
      end
      variances_for_this_grid_size(i) = var(reshape(frame_bacteria_nums(i,:,:), [1, grid_size * grid_size]));
    end
    bacteria_num_variances(grid_size_index) = mean(variances_for_this_grid_size);
  end  
end
