%% Returns the velocities of all the bacteria at a given interval number (meaning, when going from frame = frame_number to frame = frame_number+1).
function [centers_of_mass, angles, velocities, path_lengths] = frame_data_from_paths(frame_number, all_bacteria_paths)

  velocities = [];
  centers_of_mass = [];
  angles = [];
  path_lengths = [];
  
  for i = 1:length(all_bacteria_paths)
    bp = all_bacteria_paths{i};
    if bp.first_frame <= frame_number && bp.last_frame > frame_number
      velocities(end+1,:) = bp.velocities(frame_number - bp.first_frame + 1,:);
    elseif (bp.last_frame == frame_number)
      %% The bacteria only appears in this single frame /  this is the last frame in the sequence; it doesn't really have a velocity. However, we must still return something...
      velocities(end+1,:) = [nan nan];
    end

    if bp.first_frame <= frame_number && bp.last_frame >= frame_number
      centers_of_mass(end+1,:) = bp.cm_points(frame_number - bp.first_frame+1,:);
      angles(end+1,:) = bp.angles(frame_number - bp.first_frame+1);
      path_lengths(end+1,:) = size(bp.velocities,1)+1;
    end
    
  end
  
end  
