input_dir = 'YOUR INPUT DIRECTORY HERE';
input_filename = 'YOUR MAT FILENAME HERE ';

load([input_dir '/' input_filename '.mat'], 'all_frame_centers', 'all_bacteria_paths');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Curvaturing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
total_curvatures = 0;
total_curvatures_squared = 0;
total_counts = 0;

for i = 1:length(all_bacteria_paths)
  path = all_bacteria_paths{i};
  curvatures = get_curvature_from_velocities(path.velocities);
  curvatures = min(curvatures, 100); % Remove anomalies. You decide what counts as an anomaly.
  total_curvatures = total_curvatures + sum(curvatures);
  total_curvatures_squared = total_curvatures_squared + sum(curvatures.^2);
  total_counts = total_counts + length(curvatures);
end

mean_curvature = total_curvatures / total_counts;
std_curvature = sqrt( total_curvatures_squared / total_counts - mean_curvature^2 );

%output_filename = ['curvature ' input_filename '.mat'];
%save(output_filename, 'mean_curvature', 'std_curvature');

disp(['mean = ' num2str(mean_curvature) '   std = ' num2str(std_curvature)]);
