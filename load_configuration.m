function config = load_configuration

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% Parameters for phase transition analysis
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  config.clustering_test_names = {'clustering_distance','clustering_combined_dist_vel'};
  config.speed_distribution_test_name = 'speed_distribution';
  config.structure_factor_test_name = 'structure_factor';
  config.diffusivity_test_name = 'diffusivity';
  config.autocorrelation_test_name = 'autocorrelation';
  config.direction_correlation_test_name = 'direction_correlation';
  config.angle_correlation_test_name = 'angle_correlation';
  config.large_fluctuations_test_name = 'large_fluctuations';

  config.bin_order_names = {'unoriented_angle_polar_order', 'unoriented_angle_nematic_order', 'oriented_angle_polar_order', 'oriented_angle_nematic_order'};
  config.bin_test_names = {'bacteria_in_bins' config.bin_order_names{:}};
  
  %% Parameters for binning tests
  config.binning_tests_grid_size = 10; %10 for unimodality, 20 for correlations;

  %% Paramerters for large fluctuation tests
  %%config.large_fluctuations_bin_sizes = [2,4,8,16,32];
  config.large_fluctuations_bin_sizes = 2:32;
  
  %% Parameters for testing the shape of the structure factor.
  config.structure_factor_min_magnitude = 0;
  config.structure_factor_max_magnitude = 0.1;
  config.structure_factor_magnitude_ticks = 3;
  config.magnitude_range = linspace(config.structure_factor_min_magnitude, config.structure_factor_max_magnitude, config.structure_factor_magnitude_ticks);
  
  %% How many pixels is a micrometer
  config.pixels_per_micrometer = 8;
  
  config.diffusivity_summary_path_length = 20;

  config.diffusivity_path_minimal_distance = 50;
  
  %% When further smoothing velocities for curvature, this tells you how far away
  %% to look in the moving average.
  config.smoothing_for_curvature = 10;

  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  %% The different ways of checking for density. Do not change these values, they are constants.
  config.INTENSITY_THRESHOLD = 1;
  config.ENTROPY_THRESHOLD = 2;

  %% When smoothing velocities, this tells you how far away to look in the moving average.
  %% For "no smoothing", use smooth_span = 0.
  %% Otherwise, just denote the smoothing span (integer). Must be at least 4.
  config.smooth_span = 4;

  %% The folder which contains all the photos of the bacteria. We assume that sorting by filename corresponds to sorting by time. 
  config.image_folder = 'YOUR INPUT IMAGE DIRECTORY HERE';

  %% How many files to take from the folder. If this is 0, then all files will be taken.
  config.how_many_files_to_process = 3;

  %% The folder to which you will output all the text files.
  config.output_folder = 'YOUR OUTPUT DIRECTORY HERE';

  %% The filenames which contain the saved data.
  config.angle_filename = 'angles.txt';
  config.velocity_filename = 'velocities.txt';
  config.angle_correlation_filename = 'angle_correlation.txt';
  config.velocity_correlation_filename = 'velocity_correlation.txt';
  config.angle_correlation_by_neighbor_filename = 'angle_correlation_by_neighbor.txt';
  config.velocity_correlation_by_neighbor_filename = 'velocity_correlation_by_neighbor.txt';
  config.velocity_optical_flow_correlation_filename = 'velocity_optical_flow_correlation.txt';
  config.speed_optical_flow_correlation_filename = 'speed_optical_flow_correlation.txt';
  config.vorticity_optical_flow_correlation_filename = 'vorticity_optical_flow_correlation.txt';
  config.time_velocity_optical_flow_correlation_filename = 'time_velocity_optical_flow_correlation.txt';
  config.time_speed_optical_flow_correlation_filename = 'time_speed_optical_flow_correlation.txt';
  config.time_vorticity_optical_flow_correlation_filename = 'time_vorticity_optical_flow_correlation.txt';

  %% When calculating the spatial variance in density, this is the upper bound of how many blocks
  %% to divide the image to.
  config.maximum_block_num_for_spatial_variance = 32;
  
  %% The opitcal flow code divides the image into large blocks. When calculating
  %% correlations and histograms, blocks with less than this ratio of pixels (as determined
  %% by bw thresholding) will not be considered.
  %% Set this to zero to ignore the effect and just take all the optical flow data.
  config.optical_flow_pixel_count_threshold = 0.13;
  
  %% How to divide the optical flow velocity histogram bins.
  config.optical_flow_velocity_bins = -100:5:100;

  %% How to divide the optical flow angle histogram bins.
  config.optical_flow_angle_bins = -pi:0.1:pi;

  %% How to divide the optical flow velocity magnitude bins.
  config.optical_flow_magnitude_bins = 0:2.5:100;
  
  %% How many bins to divide the correlation into.
  config.correlation_bin_num = 200;

  %% How many frames to jump over between correlation calculation.
  config.correlation_frame_skip = 1000;

  %% How many neighbors to save when correlating between neighbor *num*
  config.correlation_neighbor_num = 300;

  %% Should you ignore small velocities when calculating velocity correlation?
  %% If so, set this to the desired velocity magnitude. Otherwise, put 0.
  config.ignore_small_speed = 1;

  %% Ignore large velocities. When calculating correlations, you will ignore velocities which are larger than this.
  %% If you don't want to ignore anything, set this to Inf (or to a speed larger than the size of the image).
  config.ignore_large_speed = 4.1;

  %% Ignore small paths. When calculating correlations, you will ignore bacteria whose path length is smaller than this number.
  config.ignore_small_paths = 20;

  %% When analyzing movies, bin the fames by densities according to these bins.
  config.density_bins = 0:0.02:1;

  %% This is just for the histc to work properly.
  config.density_bins(end) = config.density_bins(end)+0.01;

  %% Put "1" here if you want to automatically calculate the threshold density by looking at the local minimum of the image intensity histogram.
  %% This is recommended for short / sparse bacteria, but not for long / dense.
  config.should_calculate_threshold_with_local_minimum = 1;
  %% Only used if you *don't* calculate the threshold with the local minimum method.
  config.intensity_threshold = 0.315;

  %% Which one of the above ^ methods do you want to use in order to calculate the density?
  config.density_check_method = config.INTENSITY_THRESHOLD;

  %% Only used if the density check method is ENTROPY_THRESHOLD
  config.entropy_threshold = 4.5;

  %% For parallel computation, how many workers you want to work at a time.
  config.number_of_workers = 2;
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% Parameters related to optical flow
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  config.optical_flow.fps = 100;  % Frames per second
  config.optical_flow.ppf = 1024; % Pixles per frame
  config.optical_flow.windowSize = 100; % window size in microns
  config.optical_flow.diluteQuiverFactor = 32;   % must be even. This is the block size.
  config.optical_flow.dx = config.optical_flow.diluteQuiverFactor*config.optical_flow.windowSize/config.optical_flow.ppf;
  config.optical_flow.crop = 0; %[1 800 201 1000];
  config.optical_flow.mainDirectory = config.image_folder;

  %% numberOfSubsection=1; % must be a power of 2
  %% subSection=[1,1];
  %% distanceForVortiticySlope=5;
  %% distanceForDirectionalSlope=5;
  %% timeForCorrInTimeSlopes=0.3;
end
