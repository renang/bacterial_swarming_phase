function plot_mean_speed_summary_graphs(all_mean_speeds, base_output_directory, bacteria_size)
  config = load_configuration();

  final_speeds = zeros(1, length(config.density_bins)-1);
  for i = 1:length(config.density_bins)-1
    density = config.density_bins(i);
    density_indices = find(arrayfun(@(RIDX) all_mean_speeds{RIDX}.density == density, 1:length(all_mean_speeds)));
    if ~isempty(density_indices)
      speeds_of_this_density = all_mean_speeds(density_indices);
      density_speeds = extractfield([speeds_of_this_density{:}], 'mean_speed');
      bacteria_nums = extractfield([speeds_of_this_density{:}], 'bacteria_num');

      final_speeds(i) = (density_speeds * bacteria_nums') / sum(bacteria_nums);
    end
  end
  
  figure;
  plot(config.density_bins(1:end-1), final_speeds, '.-');
  ylabel('Speeds [pixel per frame]');
  xlabel('Density');
  title('Mean speeds per density');
  output_filename = fullfile(base_output_directory, config.speed_distribution_test_name, [num2str(bacteria_size) '_summary_speeds.png']);
  print(output_filename, '-dpng');
  close();							     
end
