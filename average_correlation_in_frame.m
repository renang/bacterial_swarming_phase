function [average_correlation average_correlation_by_neighbor] = average_correlation_in_frame(indices, centers_of_mass, trait, correlation_func)
  config = load_configuration;

  bacteria_num = size(indices, 2);
  if bacteria_num > 0
    all_binned_correlations = zeros(bacteria_num, config.correlation_bin_num);
    all_distance_histogram = zeros(bacteria_num, config.correlation_bin_num);
    all_correlations_by_neighbor = zeros(bacteria_num, length(indices));
    actual_cms = centers_of_mass(indices,:);
    actual_trait = trait(indices,:);

    for i = 1:bacteria_num
      [all_binned_correlations(i,:) ...
       all_distance_histograms(i,:) ...
       all_correlations_by_neighbor(i,:)] = single_bacteria_correlation(i, actual_cms, actual_trait, correlation_func);
    end
    
    %% There may be some bacteria that were excluded; these are identified by the fact that all their correlations are 0. Do not consider these bacteria in your calculations.
    idx = find(sum(abs(all_binned_correlations),2)~=0);
    if ~isempty(idx)  
      average_correlation = sum(all_binned_correlations(idx,:) .* all_distance_histograms(idx,:), 1) ./ sum(all_distance_histograms(idx,:),1);
    else
      %% It only happens if all of the correlations are exactly 0!
      average_correlation = zeros(1, config.correlation_bin_num);
    end
    average_correlation(isnan(average_correlation)) = 0;
    
    idx = find(sum(abs(all_correlations_by_neighbor),2)~=0);
    if ~isempty(idx)
      average_correlation_by_neighbor = mean(all_correlations_by_neighbor(idx,:),1);
    else
      %% If they are all 0, you can return just the first of them.
      average_correlation_by_neighbor = all_correlations_by_neighbor(1,:);
    end
  else
    average_correlation = zeros(1,config.correlation_bin_num);
    average_correlation_by_neighbor = 0;
  end
  
end
