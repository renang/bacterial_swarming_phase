%%% This function gets an image filename, and a threshold level by which to turn it into a bw image.
%%% It returns both the original (enhanced) image and the black and white image, after performing *all* the cuts and thresholdings available.
%%% In other words, it returns the best bw image we know how to produce. 
function [im, bw] = acquire_image(filename, threshold_level)
  [im, bw] = get_clean_images(filename, threshold_level);
  bw = filter_components_individually(im, bw, 1);
  %% Close small 1-holes in black before proceeding.
  bw = imcomplement(bwareaopen(imcomplement(bw),2,4));
  bw = cut_components_by_skeleton(bw);
  bw = bwareaopen(bw, 75, 4);
end
  