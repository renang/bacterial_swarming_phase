%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Structure func-ing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
input_dir = 'YOUR INPUT DIRECTORY HERE';
input_filename = 'YOUR MAT FILENAME HERE ';

load([input_dir '/' input_filename '.mat'], 'all_frame_centers');

min_magnitude = 0.001;
max_magnitude = 0.1;
magnitude_ticks = 100;
magnitude_range = linspace(min_magnitude, max_magnitude, magnitude_ticks);

mag_structure_func = zeros(1, length(magnitude_range));
for i = 1:length(magnitude_range)
  magnitude = magnitude_range(i);
  mag_structure_func(i) = structure_func_for_magnitude(magnitude, all_frame_centers);  
end

output_filename = ['structure ' input_filename '.mat'];
save(output_filename, 'mag_structure_func', 'magnitude_range');
