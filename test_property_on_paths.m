function [power_law_fit] = test_property_on_paths(io_struct, paths_of_this_density, property_to_test, test_name, should_draw_log_scale, should_draw_linear_fit)
  tic;
  config = load_configuration();
  display(['Now testing ' test_name]);
  create_directory_or_throw(io_struct.base_output_directory, test_name);
  create_directory_or_throw(fullfile(io_struct.base_output_directory, test_name), num2str(io_struct.bacteria_size));
  test_directory = fullfile(io_struct.base_output_directory, test_name, num2str(io_struct.bacteria_size));

	
  %% Calculate the property only for paths with length above the median
  this_density_path_lengths = extractfield([paths_of_this_density{:}], 'last_frame') - extractfield([paths_of_this_density{:}], 'first_frame');
  median_length = floor(median(this_density_path_lengths));

  power_law_fit.alpha = 0;
  
  if median_length >= 2

    above_median_indices = find(this_density_path_lengths >= median_length);
    above_median_paths = paths_of_this_density(above_median_indices);
    
    
    property = zeros(1, median_length-1);
    for i = 1:length(above_median_paths)
      path = above_median_paths{i};
      property = property + property_to_test(path, median_length);
    end

    srange = 1:(median_length-1);
    property = property / length(above_median_paths);

    %% Fit a power law
    %f = fit(srange', property', 'b*x^alpha');
    mask=property>0;
    mask(1:3)=false;
    mask(end-3:end)=false;
    %f = fit(srange(mask)', property(mask)', 'b*x^alpha');
    pp=polyfit(log(srange(mask))',log(property(mask))',1);
    f.alpha=pp(1);
    f.b=exp(pp(2));
    power_law_fit.alpha = f.alpha;
    
    %fit an exponential
    pExp=polyfit(srange(mask)', log(property(mask))',1);
    fExp.b=exp(pExp(2));
    fExp.alpha=pExp(1);
    %fExp = fit(srange(mask)', property(mask)', 'b*exp(alpha*x)');
    exponential_law_fit.alpha = -fExp.alpha;
    exponential_law_fit.c = exp(fExp.b);
    
    %loglog plot
    hold off
    plot(log10(srange), log10(property), 'bx', 'linewidth',2);
    hold on
    plot(log10(srange), log10(f.b)+f.alpha*log10(srange), 'red', 'linewidth',2);
    xlabel('log_{10} (s, in frames)');
    ylabel(['log_{10} (' test_name ')']);
    title(test_name);
    grid
    set(gca,'fontsize',16);
    legend('Data',['r(s)=' num2str(f.b) 's^{' num2str(f.alpha) '}'], 'Location','se');
    output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' loglog.png']);
    print(output_filename, '-dpng');
    close();
    output_filename_txt = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' loglog.txt']);
    ddd=[log10(srange)', log10(property)', log10(f.b)+f.alpha*log10(srange)'];
    save(output_filename_txt,'ddd','-ascii');
    
    %semi-log plot
    hold off
    plot(srange, log10(property), 'bx', 'linewidth',2);
    hold on
    plot(srange, log10(fExp.b)+fExp.alpha*srange*log10(exp(1)), 'red', 'linewidth',2);
    xlabel('s, in frames');
    ylabel(['log_{10} (' test_name ')']);
    title(test_name);
    grid
    set(gca,'fontsize',16);
    legend('Data',['r(s)=' num2str(f.b) 'e^{' num2str(f.alpha) 's}'],'Location','se');
    output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' semilog.png']);
    print(output_filename, '-dpng');
    close();
    output_filename_txt = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' semilog.txt']);
    ddd=[srange', log10(property)', log10(fExp.b)+fExp.alpha*log10(srange)'];
    save(output_filename_txt,'ddd','-ascii');
    
    
    
    
%     plot(srange, p1*srange, 'black', 'linewidth',2);
%     end
%     if should_draw_log_scale
%         plot(srange, exp(fExp.b+srange.*(fExp.alpha)), 'black', 'linewidth',2);
%     end
%     
%     plot(srange, property, 'bx', 'linewidth',2);
%     hold off
%     xlabel('s, in frames');
%     ylabel(test_name);
%     title(test_name);
%     grid
%     if ~should_draw_log_scale
%         legend(['r(s)=' num2str(p1) 's'], ['r(s)=' num2str(f.b) 's^{' num2str(f.alpha) '}'], 'Data','Location','se');
%     else
%         legend(['r(s)=' num2str(exp(fExp.b)) '*e^{' num2str(fExp.alpha) 's}'], ['r(s)=' num2str(f.b) 's^{' num2str(f.alpha) '}'], 'Data','Location','sw')
%     end
%     
%     
%     
%     if should_draw_log_scale
%         set(gca,'yscale','log');
%         set(gca,'xscale','log');
%         axis([min(srange) max(srange) min(property) max(property)]);
%     end
%     
%     output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension '.png']);
%     print(output_filename, '-dpng');
%     close();
  end
  toc;
end
