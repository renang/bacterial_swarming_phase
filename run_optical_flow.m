function data = run_optical_flow()

  
  addpath('optical_flow');
  config = load_configuration;
  filenames = get_all_image_filenames_from_folder(config.image_folder);
  data = config.optical_flow;
  
  max_iteration = length(filenames);
  if config.how_many_files_to_process ~= 0
    max_iteration = config.how_many_files_to_process;
  end

  %% For backcompatibility; the two vars are the same.
  numberOfFrames = max_iteration;
    
  data.maxSpeed = 100;
  frwrd = 1;
  memory = 0.7;
  centeralWeight = 0.6;


  data.nFrames = numberOfFrames-frwrd;
  bins = 0:0.1:data.maxSpeed;
  data.bins = bins;
		
  
  %% open one test image for size
  im1 = imread(filenames{1});
  if data.crop(1)>0
    im1 = im1(data.crop(1):data.crop(2),data.crop(3):data.crop(4));
  end

  data.frameSize = size(im1);
  is = data.diluteQuiverFactor/2:data.diluteQuiverFactor:size(im1,1);
  js = data.diluteQuiverFactor/2:data.diluteQuiverFactor:size(im1,2);
  [data.iii,data.jjj] = meshgrid(js,is);
  data.vfsize = [size(data.iii,1) size(data.iii,2)];
  data.pu = zeros(data.nFrames,data.vfsize(1),data.vfsize(2),'single');
  data.pv = zeros(data.nFrames,data.vfsize(1),data.vfsize(2),'single');


  %% calculate flow maps
  %% ------------------------

  for i = 1:max_iteration-1

    im1 = imread(filenames{i});
    im2 = imread(filenames{i+1});
    if data.crop(1)>0
      im1 = im1(data.crop(1):data.crop(2),data.crop(3):data.crop(4));
      im2=im2(data.crop(1):data.crop(2),data.crop(3):data.crop(4));
    end
   
    [u,v] = HS(im1,im2,1,100,zeros(size(im1(:,:,1))),zeros(size(im2(:,:,1))),0);
    
    u=(u+circshift(u,[0 1]) + circshift(u,[0 -1]) + circshift(u,[1 0]) + circshift(u,[-1 0]) )/5;
    v=(v+circshift(v,[0 1]) + circshift(v,[0 -1]) + circshift(v,[1 0]) + circshift(v,[-1 0]) )/5;
    u=(u+circshift(u,[0 1]) + circshift(u,[0 -1]) + circshift(u,[1 0]) + circshift(u,[-1 0]) )/5;
    v=(v+circshift(v,[0 1]) + circshift(v,[0 -1]) + circshift(v,[1 0]) + circshift(v,[-1 0]) )/5;
    u=(u+circshift(u,[0 1]) + circshift(u,[0 -1]) + circshift(u,[1 0]) + circshift(u,[-1 0]) )/5;
    v=(v+circshift(v,[0 1]) + circshift(v,[0 -1]) + circshift(v,[1 0]) + circshift(v,[-1 0]) )/5;
    
    pu=u(is,js);
    pv=v(is,js);
    
    pu=centeralWeight*pu + (1-centeralWeight)*( circshift(pu,[0 1]) + circshift(pu,[0 -1]) + circshift(pu,[1 0]) + circshift(pu,[-1 0]) )/4;
    pv=centeralWeight*pv + (1-centeralWeight)*( circshift(pv,[0 1]) + circshift(pv,[0 -1]) + circshift(pv,[1 0]) + circshift(pv,[-1 0]) )/4;
    pu=pu*(data.windowSize*data.fps/frwrd/data.ppf);
    pv=pv*(data.windowSize*data.fps/frwrd/data.ppf);
    
    norms=sqrt(pu.^2+pv.^2);
    pu(norms>data.maxSpeed)=(pu(norms>data.maxSpeed)./norms(norms>data.maxSpeed))*data.maxSpeed;
    pv(norms>data.maxSpeed)=(pv(norms>data.maxSpeed)./norms(norms>data.maxSpeed))*data.maxSpeed;
    
    if i > 1
      pv=(1-memory)*pv+memory*pastPv;
      pu=(1-memory)*pu+memory*pastPu;
    end
    
    data.pu(i,:,:)=pu;
    data.pv(i,:,:)=pv;
    
    pastPu=pu;
    pastPv=pv;
        
    disp(i);   
    
  end %for fr=1:1:data.nFrames

  speeds=sqrt(data.pu.^2+data.pv.^2);
  speedsArray=reshape(speeds,(numberOfFrames-1)*data.vfsize(1)*data.vfsize(2),1);
  data.h=hist(speedsArray,bins);  
end
