function ac = get_autocorrelation_up_to_smax(path, smax)
  ac = zeros(1, smax-1);
  for s = 1:(smax-1)
    current_sum = 0;
    for t = 1:(smax-s)
      current_sum = current_sum + sum(path.velocities(t+s,:) .* path.velocities(t,:));
    end
    ac(s) = current_sum / (smax-s);
  end
end
