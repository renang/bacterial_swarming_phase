function plot_bins_summary_graphs(binning_summary, base_output_directory, bacteria_size)
  config = load_configuration();  
  test_names = config.bin_order_names;

  for i = 1:length(test_names)
    test_name = test_names{i};
    means = zeros(1, length(config.density_bins)-1);
    stds = zeros(1, length(config.density_bins)-1);

    for density_index = 1:length(config.density_bins)-1
      means(density_index) = mean(extractfield(binning_summary{density_index}, test_name));
      stds(density_index) = std(extractfield(binning_summary{density_index}, test_name));
    end

    figure;
    plot(config.density_bins(1:end-1), means, '.-');
    title(['mean ' test_name ' order parameter'], 'Interpreter', 'None');
    xlabel('Density')
    xlim([0 1])
    ylim([0 1])
    output_filename = fullfile(base_output_directory, test_name, [num2str(bacteria_size) '_mean.png']);
    print(output_filename, '-dpng');
    close();
    
   
    figure;
    plot(config.density_bins(1:end-1), stds, '.-');
    title(['std ' test_name ' order parameter'], 'Interpreter', 'None');
    xlabel('Density')
    xlim([0 1])
    ylim([0 1])
    output_filename = fullfile(base_output_directory, test_name, [num2str(bacteria_size) '_std.png']);
    print(output_filename, '-dpng');
    close();
    
  end

  means = zeros(1, length(config.density_bins)-1);
  vars = zeros(1, length(config.density_bins)-1);
  for density_index = 1:length(config.density_bins)-1
    means(density_index) = mean(binning_summary{density_index}.bacteria_nums);
    vars(density_index) = var(binning_summary{density_index}.bacteria_nums);
  end
  means(isnan(means)) = [];
  vars(isnan(vars)) = [];

  figure;
  plot(means, vars, '.');
  title('Variance of bacteria num in bins vs. mean');
  test_name = 'bacteria_in_bins';
  xlabel('Mean bacteria num');
  ylabel('Variance bacteria num');
  output_filename = fullfile(base_output_directory, test_name, [num2str(bacteria_size) '_var_vs_mean.png']);
  print(output_filename, '-dpng');  
  close();
  
  output_filename_text = fullfile(base_output_directory, test_name, [num2str(bacteria_size) '_var_vs_mean.txt']);
  if size(means,1)<size(means,2)
      ddd=[means', vars'];
  else
      ddd=[means, vars];
  end
  save(output_filename_text,'ddd','-ascii');
  
end
