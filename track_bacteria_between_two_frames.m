%%% c1 - the position of the centers of mass of all the bacteria in the first frame.
%%% c2 - the same, but for the second frame.
%%% Returns a "matchings" list: a list which, for each bacteria in c1, tells what is the corresponding bacteria in c2. If 0, then no correspondence was found.
function matchings = track_bacteria_between_two_frames(c1, c2)  

  %% For each point in the first image, look at ALL the points in a rectangle around it. Now look at a *larger* rectangle which contains all those points in the second image, and try to match (combinatorically) all the points in the first image to those in the second image.  It may be that the first rectangle has more points, or less points. Take the matching with the smallest distances, taking into consideration that individual distances can't be too large. Remove the two points from our lists.

  %% Let's say that the germs move about 10 pixels max. So we expect (with errors) the center of mass not to move more than 16...
  small_window = 16;
  large_window = 32;

  
  c2_bak = c2(:,:);
  matchings = zeros(size(c1, 1), 1);

  for counter = 1:size(c1, 1)

    fprintf(['\r' num2str(counter) '/' num2str(size(c1,1))]); 
    x = c1(1,1);
    y = c1(1,2);

    bac1 = all_bacteria_in_window(x, y, small_window, c1);
    bac2 = all_bacteria_in_window(x, y, large_window, c2);

    %% Actually, this should always be 1 since "intersect" sorts the list, but I do not want to rely on this feature.
    original_bacteria_index = find(bac1 == 1, 1);
    %% If you found any other bacteria to match against.
    if size(bac2,1) ~=0
      [i1, i2] = find_best_one_to_one_correspondence(c1(bac1,:), c2(bac2,:), original_bacteria_index,1, 12);
    
      if ~isempty(i1)
	%% We now have the best permutation at hand.
	current_bacteria_index = find(i1 == original_bacteria_index);

	%% Maybe the best matching didn't involve the current bacteria.
	if ~isempty(current_bacteria_index)
	  matching_bacteria = bac2(i2(current_bacteria_index));

	  %% Because we constantly delete things out, we need to reference and find in the original image 2 bacteria.
	  [~,indx] = ismember(c2(matching_bacteria,:), c2_bak, 'rows');	  
	  matchings(counter) = indx;
	  c2(matching_bacteria,:) = [];
	end

      end
	
    end
    c1(1,:) = [];
  end
  disp ' '
end


function b = all_bacteria_in_window(x, y, window_size, c)
  low_x = x - window_size; high_x = x + window_size;
  low_y = y - window_size; high_y = y + window_size;

  f1 = find(c(:,1) >= low_x);
  f2 = find(c(:,1) <= high_x);
  f3 = find(c(:,2) >= low_y);
  f4 = find(c(:,2) <= high_y);
    
  f = intersect(f1, f2);
  f = intersect(f, f3);
  b = intersect(f, f4);
end
