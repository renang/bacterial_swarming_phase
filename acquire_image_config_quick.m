%% Same as acquire image, but skips on the skeleton cutting so it will be quicker.
%% This can be used if you only need the bw image, and don't intend to do individual
%% bacteria parsing.
function [im,bw] = acquire_image_config_quick(filename)

  config = load_configuration;

  if config.should_calculate_threshold_with_local_minimum
    image_threshold = get_threshold_level_from_local_minimum(filename);
  else
    image_threshold = config.intensity_threshold;
  end

  [im, bw] = get_clean_images(filename, image_threshold);
  bw = imcomplement(bwareaopen(imcomplement(bw),2,4));
  bw = bwareaopen(bw, 75, 4);
end
