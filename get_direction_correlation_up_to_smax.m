function dc = get_direction_correlation_up_to_smax(path, smax)
  dc = zeros(1, smax-1);
  for s = 1:(smax-1)
    current_sum = 0;
    for t = 1:(smax-s)
      x = sum(path.velocities(t+s,:) .* path.velocities(t,:));
      if x == 0
	current_sum = current_sum + x;
      else
	v1 = sqrt(sum(path.velocities(t+s,:).^2));
	v2 = sqrt(sum(path.velocities(t,:).^2));
	current_sum = current_sum + x/(v1*v2);
      end
      
    end
    dc(s) = current_sum / (smax-s);
  end
end
