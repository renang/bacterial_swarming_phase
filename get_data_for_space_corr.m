function [inSpeed, inDir, inVort, inSpeedW, inDirW, inVortW, ttt] = get_data_for_space_corr(data, indices_to_take)
  config = load_configuration;

  speeds=sqrt(data.pu.^2+data.pv.^2);
  speedsArray=reshape(speeds,size(speeds,1)*data.vfsize(1)*data.vfsize(2),1);
  data.bins=0:0.5:data.maxSpeed;
  data.h=hist(speedsArray,data.bins);
  
  nu=data.pu./speeds;
  nv=data.pv./speeds;
  nfr=data.nFrames;

  vort=zeros(nfr,data.vfsize(1),data.vfsize(2));
  for f=1:nfr
    vort(f,:,:)=curl( reshape(data.pu(f,:,:),data.vfsize), reshape(data.pv(f,:,:),data.vfsize) )/data.dx;
  end 

  maxdist = max(data.vfsize);
  nbins = config.correlation_bin_num;
  pntsPerBin = 120000;
  db = maxdist/nbins;
  bins = 0:db:maxdist+db/10;
  nbins=length(bins)-1;
  cbins=(bins(1:nbins)+bins(2:nbins+1))/2;

  xs=reshape(data.iii/data.iii(1,1)/2,data.vfsize(1)*data.vfsize(2),1);
  ys=reshape(data.jjj/data.jjj(1,1)/2,data.vfsize(1)*data.vfsize(2),1);

  pos=[xs,ys];
  pdt=pdist(pos);
  pd=squareform(pdt);

  inSpeed=zeros(nbins,1);
  inDir=zeros(nbins,1);
  inVort=zeros(nbins,1);
  inSpeedW=zeros(nbins,1);
  inDirW=zeros(nbins,1);
  inVortW=zeros(nbins,1);
  
  for b=1:nbins
    [r,c]=find(pd>=bins(b) & pd<bins(b+1));
    r2=r(r<=c);
    c2=c(r<=c);
    
    k=length(r2);
    [rr,rc]=ind2sub(data.vfsize,r2);
    [cr,cc]=ind2sub(data.vfsize,c2);
    
    if k>0
      ls=randi(k,pntsPerBin,1);
      ts=randi(nfr,pntsPerBin,1);
      indr=sub2ind(size(speeds),ts,rr(ls),rc(ls));
      indc=sub2ind(size(speeds),ts,cr(ls),cc(ls));

      good_indr_indices = ismember(indr, indices_to_take);
      good_indc_indices = ismember(indc, indices_to_take);
      good_indices = good_indr_indices & good_indc_indices;
      
      indr = indr(good_indices);
      indc = indc(good_indices);
      
      dx=[rr(ls)-cr(ls) rc(ls)-cc(ls)];
      dxn=sqrt(dx(:,1).^2 + dx(:,2).^2);
      dx(:,1)=dx(:,1)./dxn;
      dx(:,2)=dx(:,2)./dxn;
      dx(isnan(dx))=0;

      temp_dx1 = dx(:,1);
      temp_dx2 = dx(:,2);
      temp_dx1 = temp_dx1(good_indices);
      temp_dx2 = temp_dx2(good_indices);
      dx = [temp_dx1, temp_dx2];
      
      weights=1-abs(dx(:,1).*nu(indr)+dx(:,2).*nv(indr));
      sumW=sum(weights);
      
      inSpeed(b)=mean(speeds(indr).*speeds(indc))-mean(speeds(indr))*mean(speeds(indc));
      inDir(b)=mean(nu(indr).*nu(indc)+nv(indr).*nv(indc))-mean(nu(indr))*mean(nu(indc))-mean(nv(indr))*mean(nv(indc));
      inVort(b)=mean(vort(indr).*vort(indc))-mean(vort(indr))*mean(vort(indc));
      
      inSpeedW(b)=sum(speeds(indr).*speeds(indc).*weights)/sumW-sum(speeds(indr).*weights)*sum(speeds(indc).*weights)/sumW/sumW;
      inDirW(b)=sum((nu(indr).*nu(indc)+nv(indr).*nv(indc)).*weights)/sumW-sum(nu(indr).*weights)*sum(nu(indc).*weights)/sumW/sumW-sum(nv(indr).*weights)*sum(nv(indc).*weights)/sumW/sumW;
      inVortW(b)=sum(vort(indr).*vort(indc).*weights)/sumW-sum(vort(indr).*weights)*sum(vort(indc).*weights)/sumW/sumW;
    else
      inSpeedW(b)=0;
      inDirW(b)=0;
      inVortW(b)=0;
    end    
  end
  
  inSpeed=inSpeed/inSpeed(1);
  inDir=inDir/inDir(1);
  inVort=inVort/inVort(1);
  inSpeedW=inSpeedW/inSpeedW(1);
  inDirW=inDirW/inDirW(1);
  inVortW=inVortW/inVortW(1);
  ttt=cbins'*data.windowSize/max(data.vfsize);

  %% dist3micros=find(ttt<=distanceForVortiticySlope,1,'last')+1;
  %% dist15micros=find(ttt<=distanceForDirectionalSlope,1,'last')+1;
end
