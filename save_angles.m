%%% This function saves *all* the angles of all the bacteria found in a sequence of images.
%%% filename - the file to which the angle data will be saved.
%%% all_bacteria_paths - a cell array of bacteria path structures (as returned by process_folder). 
function save_angles(filename, all_bacteria_paths)
  fid = fopen(filename, 'w');
  fprintf(fid, 'angle\n');
  for i = 1:length(all_bacteria_paths)
    fprintf(fid, '%f\n', all_bacteria_paths{i}.angles);
  end
  fclose(fid);
end
  