function create_directory_or_throw(parent_directory, directory_name)
  status = mkdir(parent_directory, directory_name);
  if status == 0
    msgID = 'create_directory_or_throw';
    msg = ['Unable to create directory ' directory_name ' in ' parent_directory];
    base_exception = MException(msgID, msg);
    throw(base_exception)
  end
end
