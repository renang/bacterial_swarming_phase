function fits = test_clustering(io_struct, frames_data, average_num_of_bacteria_in_frames, ...
			distance_clusters_sizes, ...
			combined_clusters_sizes, ...
			distance_largest_cluster, ...
			combined_largest_cluster)

  tic;
  config = load_configuration();
  %% NOTE: users of the test_clustering function rely on the specific ordering in
  %% cluster_sizes_array (i.e which comes first: distance or combined). Do not change
  %% it without changing callers in other files. Thanks!
  cluster_sizes_array = {distance_clusters_sizes, combined_clusters_sizes};
  test_names = config.clustering_test_names;
  average_largest_cluster_ratio = {mean(distance_largest_cluster) / average_num_of_bacteria_in_frames, mean(combined_largest_cluster) / average_num_of_bacteria_in_frames};

  fits = {};

  for i = 1:length(cluster_sizes_array)
    test_name = test_names{i};
    display(['Now testing ' test_name]);
    create_directory_or_throw(io_struct.base_output_directory, test_name);
    create_directory_or_throw(fullfile(io_struct.base_output_directory, test_name), num2str(io_struct.bacteria_size)); 
    test_directory = fullfile(io_struct.base_output_directory, test_name, num2str(io_struct.bacteria_size));
    output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' exponential.png']);    
    
    cluster_sizes = cluster_sizes_array{i};    
    
    [cluster_distribution, bins] = hist(cluster_sizes, max(2, max(cluster_sizes)-1));
    func_to_fit = @(p,xdata)p(1)*(xdata.^(-p(2))) .* exp(-xdata / p(3));
    p0 = [1 1 1];
    opts = optimset('Display','off');
    
    [p,resnorm,~,exitflag,output] = lsqcurvefit(func_to_fit,p0,bins,cluster_distribution, [],[], opts);
    
%     %linear plot
%     figure;
%     hold on;	  
%     plot(bins, log10(abs(cluster_distribution)), 'b.','markersize',13);
%     plot(bins, log10(abs(func_to_fit(p, bins))), 'black', 'linewidth', 2);
%     title({test_name, ['Largest cluster ratio: ' num2str(average_largest_cluster_ratio{i})]}, 'Interpreter', 'None');
%     xlabel('Cluster size');
%     ylabel('Count');
%     legend('Data', [num2str(p(1)) 'x ^{-' num2str(p(2)) '} * exp(-x/' num2str(p(3)) ')']);
%     hold off;
%     print(output_filename, '-dpng');
%     close();
    
    %semi log
    figure;
    hold on;	  
    plot(bins, log10(abs(cluster_distribution)), 'b.','markersize',13);
    plot(bins, log10(abs(func_to_fit(p, bins))), 'black', 'linewidth', 2);
    title({test_name, ['Largest cluster ratio: ' num2str(average_largest_cluster_ratio{i})]}, 'Interpreter', 'None');
    xlabel('Cluster size');
    ylabel('log_{10} (Count)');
    legend('Data', [num2str(p(1)) 'x ^{-' num2str(p(2)) '} * exp(-x/' num2str(p(3)) ')']);
    hold off;
    output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' semilog.png']);
    print(output_filename, '-dpng');
    close();
    
    %loglog
    figure;
    hold on;
    last=find(bins<=50,1,'last');
    xxToFit=bins(2:last);
    yyToFit=abs(cluster_distribution(2:last));
    mask=~isnan(yyToFit) & xxToFit>0 & yyToFit>0;
    yyToFit=yyToFit(mask);
    xxToFit=xxToFit(mask);
    plot(log10(bins), log10(cluster_distribution), 'b.','markersize',13);
    f.a=0;
    f.b=0;
    if sum(mask)>=2
        pp=polyfit(log10(xxToFit),log10(yyToFit),1);
        f.b=pp(1);
        f.a=pp(2);
        f.c=sum(cluster_distribution(last:end))/sum(cluster_distribution);
        f.name = test_name;
        fits{end+1} = f;
        %f = fit(xxToFit', yyToFit', 'a*x^b');
        plot(log10(xxToFit), f.a+f.b*log10(xxToFit), 'black', 'linewidth', 2);
        legend('Data', [num2str(f.a) 'x ^^ ' num2str(f.b)]);
    end
    title({test_name, ['Largest cluster ratio: ' num2str(average_largest_cluster_ratio{i})]}, 'Interpreter', 'None');
    xlabel('log_{10} (Cluster size)');
    ylabel('log_{10} (Count)');
    hold off;
    output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' loglog.png']);
    print(output_filename, '-dpng');
    close();
    
    output_filename_text = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' loglog.txt']);
    xx=bins;
    yy=cluster_distribution/sum(cluster_distribution)/(bins(2)-bins(1));
    ddd=[log10(bins)', log10(cluster_distribution)'];
    save(output_filename_text,'ddd','-ascii');
    toc;
  end
end
