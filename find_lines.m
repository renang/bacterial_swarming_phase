function [best_bacteria_pixels best_line_points] = find_lines(component_image, force_k, polygon_shape_inserter)
         
  white_indices = find(component_image == 1);
  area = length(white_indices);

  y = get_sample_points(white_indices, component_image);
  %%y = datasample(white_indices, 20, 'Replace', false)';

  %%disp(['We will sample ' num2str(length(y)) ' points.']);

  %% f = zeros(size(component_image));
  %% f(white_indices) = 0.5;
  %% f(y) = 1;
  %% figure; imshow(f); drawnow;
 
  line_length = 2*sqrt(size(component_image) * size(component_image)');
  angles = linspace(0, pi, 30);
  angles = angles(1:end-1);

  best_centers_of_mass_of_round = [];
  best_angles_of_round = [];
  best_scores_of_round = [];

  sample_point_counter = 0;
  counter = 0;
  for i = y
    sample_point_counter = sample_point_counter + 1;
    fprintf([num2str(sample_point_counter) ' \r'], sample_point_counter);
    
    [center_y, center_x] = ind2sub(size(component_image), i);

    
    best_scores_of_round(end+1,:) = 0;
    best_centers_of_mass_of_round(end+1,:) = [0 0];
    best_angles_of_round(end+1,:) = 0;
    
    for theta = angles
      %% Creating all the points of the line.
      [largest_line_x, largest_line_y, score, did_work] = best_line_segment_in_component(center_x, center_y, theta, line_length, component_image);

      if ~did_work
	continue
      end
      
      if score > best_scores_of_round(end)
	best_scores_of_round(end,:) = score;
	best_angles_of_round(end,:) = theta;
	best_centers_of_mass_of_round(end,:) = [mean(largest_line_x), mean(largest_line_y)];
      end     
    	
      %% Drawing:
      %% largest_line_indices = quick_2d_sub2ind(size(component_image), largest_line_y, largest_line_x);
      %% image_with_line = 128 * uint8(component_image);
      %% image_with_line(largest_line_indices) = 255;    
      %% figure; imshow(image_with_line); drawnow;
    end
    %% waitforbuttonpress; close all;
  end

  data_to_cluster = [best_centers_of_mass_of_round'; best_angles_of_round']';

	    
  %% TODO: we should get a better estimate of how many bacteria there are in here at all...
  %% But for now, let's use areas: the area of a bacteria is usually around 250, with small ones (or cut-off ones) at around 50. Larger ones are about 500.
  k_scores = [];
  k_rectangles = {};
  k_line_points = {};
  minimal_k = floor(area / 500);
  minimal_k = max(minimal_k, 1);
  maximal_k = ceil(area / 150) + 1;

  if exist('force_k', 'var') && force_k(1) ~= 0
    minimal_k = force_k(1);
    maximal_k = force_k(end);
  end
  
  if size(data_to_cluster,1) < 10
    %% Treat this as a failure.
    disp('Not enough data to cluster. The finding function failed.');
    best_bacteria_pixels = {};
    best_line_points = {};
    return;
  end
	    

  %%disp(['Clustering: checking from ' num2str(minimal_k) ' clusters to ' num2str(maximal_k) ' clusters']);
  
  for k = minimal_k:maximal_k  
    %%disp(['Checking for ' num2str(k) ' clusters.']);      
      
    %% [idx,centers,sumd] = kmeans(data_to_cluster, k, 'Distance', 'correlation', 'Replicates', 5);
    [idx,centers,sumd] = kmeans(data_to_cluster, k, 'Replicates', 4, 'EmptyAction', 'singleton');


    %% Draw the k clustering
    %% figure;
    %% title(['k = ' num2str(k)]);
    %% hold all;
    %% for i = 1:k
    %%   scatter3(data_to_cluster(idx==i,1),-data_to_cluster(idx==i,2),data_to_cluster(idx==i,3),'.')
    %% end
    %% hold off;

    %% Draw and calculate scores of the lines in the resultant clustering
    tempim = zeros(size(component_image));
    line_points = {};
    for i = 1:k    
      center_x = floor(mean(data_to_cluster(idx==i,1)));
      center_y = floor(mean(data_to_cluster(idx==i,2)));
      theta = mean(data_to_cluster(idx==i,3));

      [line_x, line_y, score, did_work] = best_line_segment_in_component(center_x, center_y, theta, line_length, component_image);

      if did_work
	[x1 y1 x2 y2] = extremal_points_from_line(line_x, line_y, component_image);

	rectangle = zeros(size(component_image));
	rectangle = draw_tilted_rectangle_around_line(x1,y1,x2,y2,4,rectangle,polygon_shape_inserter);
	line_points{end+1} = [quick_2d_sub2ind(size(component_image), y1,x1)  quick_2d_sub2ind(size(component_image), y2,x2)];
	tempim = tempim + rectangle;
      end      
    end
    %% figure; imshow(tempim);
    tempim = im2bw(tempim);
    covered = tempim & component_image;
    k_scores(end+1) = sum(sum(covered));
    k_line_points{end+1} = line_points;
    %% figure; imshow(covered);
  end

  if k_scores == 0
    %% Treat this as a failure.
    disp('No clusters found. The finding function failed.');
    best_bacteria_pixels = {};
    best_line_points = {};
    return;
  end

  %% Now you have given a "how-well-do-you-fit" score to each k-clustering, select the best one.
  %% Obviously, the more k clusters, the more area you'll fit. But if you found a good one, you'll cover most of it anyway. So, TODO: find a good way to compromise between area-fitting and numberof clusters. For now, let's do something very very simple.

  try  
    best_k_index = find(k_scores > 0.9 * k_scores(end),1);
    best_line_points = k_line_points{best_k_index};

    %% Now recalculate the bacteria pixels: these are the best rectangles you have found, masked with the actual component image.
    %% (we did not save them all while calculating the k scores, because for large clusters this depletes the memory.)
    best_bacteria_pixels = cell(1, size(best_line_points,2));
    for i = 1:length(best_bacteria_pixels)
      [y1 x1] = quick_2d_ind2sub(size(component_image), line_points{i}(1));
      [y2 x2] = quick_2d_ind2sub(size(component_image), line_points{i}(2));
      rectangle = draw_tilted_rectangle_around_line(x1,y1,x2,y2,4, zeros(size(component_image)), polygon_shape_inserter);
      best_bacteria_pixels{i} = rectangle & component_image;
    end  

    %%disp(['We found ' num2str(length(best_bacteria_pixels)) ' initial bacteria.']);
  catch ME
    disp(ME.identifier);
    best_bacteria_pixels = {};
    best_line_points = {};
    return;    
  end
end
