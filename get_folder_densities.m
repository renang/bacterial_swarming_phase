function [all_densities, block_deviations] = get_folder_densities(foldername, force_num_of_files)
config = load_configuration;

filenames = get_all_image_filenames_from_folder(foldername);
max_iteration = length(filenames);

if exist('force_num_of_files', 'var')
  if force_num_of_files ~= 0
    max_iteration = force_num_of_files;
  end
end

block_range = 2:config.maximum_block_num_for_spatial_variance;

all_densities = zeros(1,max_iteration);
block_deviations = zeros(max_iteration, length(block_range));

for i = 1:max_iteration
  filename = filenames{i};
  disp(['Processing file number ' num2str(i) ': ' filename]);

  [im, bw] = acquire_image_config_quick(filename);
  
  if config.density_check_method == config.INTENSITY_THRESHOLD
    density = sum(sum(bw))/prod(size(bw));
  elseif config.density_check_method == config.ENTROPY_THRESHOLD
    im_entropy = entropyfilt(im);  
    density = sum(sum(im_entropy >= config.entropy_threshold)) / prod(size(bw));
  end

  all_densities(i) = density;

  intensity_density_block_func = @(x) (sum(sum(x.data))/prod(x.blockSize));
  entropy_density_block_func = @(x) ((sum(sum(x.data >= config.entropy_threshold))) / prod(x.blockSize));

  j = 0;
  for num_of_blocks = block_range
    j = j + 1;
    blocksize = floor(size(bw,1)/num_of_blocks);   

    if config.density_check_method == config.INTENSITY_THRESHOLD
      block_intensities = blockproc(bw, [blocksize blocksize], intensity_density_block_func);
    elseif config.density_check_method == config.ENTROPY_THRESHOLD
      block_intensities = blockproc(im_entropy, [blocksize blocksize], entropy_density_block_func);
    end

    %% In case the image size did not exactly divide into num_of_blocks blocks, forgo the edges.
    block_intensities = block_intensities(1:num_of_blocks, 1:num_of_blocks);
    block_deviations(i,j) = std(block_intensities(:));
  end
  
end
