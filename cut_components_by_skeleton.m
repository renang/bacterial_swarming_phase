function new_bw = cut_components_by_skeleton(bw)
  new_bw = zeros(size(bw));
  cc = bwconncomp(bw, 4);
  regions = regionprops(cc,'basic');
  for c = 1:cc.NumObjects
    component_image = get_component_image_from_number(c,cc,regions,bw);

    if is_component_convex(bw, cc.PixelIdxList{c}, regions(c))   
      new_component_image = component_image;
    else
      new_component_image = prune_component_by_skeleton(component_image);
    end

    component_indices = find(new_component_image == 1);
    image_indices = component_coordinates_to_image_coordinates(component_indices, regions(c), new_component_image, new_bw);
    new_bw(image_indices) = 1;
    
  end

  new_bw = bwareaopen(new_bw, 50, 4);
end
