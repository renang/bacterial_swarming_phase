function change_bac_file_format(input_foldername, output_foldername)
    all_bac_files = dir([input_foldername '/bac*.txt']);
    filenames = {};
    [filenames{1:length(all_bac_files), 1}] = deal(all_bac_files.name);
    filenames = sortrows(filenames);
    for i = 1:length(filenames)
        filename = fullfile(input_foldername, filenames{i});
        content = dlmread(filename,'',1,0);
        size(content)
        new_filename = fullfile(output_foldername, [num2str(i) '.txt']);
        fid = fopen(new_filename,'w');
        fprintf(fid, '%f\t%f\t%f\n', content');
        fclose(fid);
    end        
end