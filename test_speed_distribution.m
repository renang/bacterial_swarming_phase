function test_speed_distribution(io_struct, frames_data)
  tic;
  config = load_configuration();
  test_name =  config.speed_distribution_test_name;
  display(['Now testing ' test_name]);
  create_directory_or_throw(io_struct.base_output_directory, test_name);
  create_directory_or_throw(fullfile(io_struct.base_output_directory, test_name), num2str(io_struct.bacteria_size));
  test_directory = fullfile(io_struct.base_output_directory, test_name, num2str(io_struct.bacteria_size));

  speeds = sqrt(sum(frames_data.all_relevant_velocities .* frames_data.all_relevant_velocities, 2));
  output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension '.png']);
  save_histogram_with_stats(speeds, 100, 'Velocity (pixels)', output_filename);
  
  output_filename_text = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension '.txt']);
  [counts,centers]=hist(speeds, 100);
  counts=counts/sum(counts)/(centers(2)-centers(1));
  ddd=[centers' , counts'];
  save(output_filename_text,'ddd','-ascii');
  toc;
end
