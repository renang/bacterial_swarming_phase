config = load_configuration;

[all_frame_densities, all_frame_block_deviations] = get_folder_densities(config.image_folder, config.how_many_files_to_process);

mean_density = mean(all_frame_densities);
std_density = std(all_frame_densities);

out_filename_mat = fullfile(config.output_folder, strrep([datestr(now) '_densities.mat'], ':', '_'));
save(out_filename_mat);

block_range = 2:config.maximum_block_num_for_spatial_variance;

out_filename_txt = fullfile(config.output_folder, strrep([datestr(now) '_densities.txt'], ':', '_'));
fid = fopen(out_filename_txt, 'w');

title_string = ['frame\tdensity\t' num2str(block_range,repmat('std %d blocks\t',1,length(block_range))) '\n'];
fprintf(fid, title_string);

format_string = ['%d\t%f' repmat('\t%f',1,length(block_range)) '\n'];
fprintf(fid, format_string, [1:length(all_frame_densities); all_frame_densities; all_frame_block_deviations']);
fclose(fid);
