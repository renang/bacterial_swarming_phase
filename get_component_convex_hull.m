function [x_convex, y_convex] = get_component_convex_hull(im, component_indices)
  [y,x] = ind2sub(size(im), component_indices);
  k = my_convhull(x,y);
  convex_indices = component_indices(k);
  [y_convex, x_convex] = ind2sub(size(im), convex_indices);
end
