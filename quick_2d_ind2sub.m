function [rows, cols] = quick_2d_ind2sub(s, indices)
  rows = rem(indices - 1, s(1)) + 1;
  cols = (indices-rows) / s(1) + 1;
end