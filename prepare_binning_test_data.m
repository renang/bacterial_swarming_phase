function [all_bin_bacteria_nums, ...
	  all_bin_unoriented_angle_polar_order_parameter, ...
	  all_bin_unoriented_angle_nematic_order_parameter, ...
	  all_bin_oriented_angle_polar_order_parameter, ...
	  all_bin_oriented_angle_nematic_order_parameter] = prepare_binning_test_data(frames_data)
  
  config = load_configuration();
  grid_size = config.binning_tests_grid_size;
  mn = [0 0]; mx = [1024 1024];  % mn = min possible of centers; mx = max possible of centers
  edges = linspace(mn(1), mx(1), grid_size+1);

  frame_bacteria_nums = zeros(frames_data.number_of_frames, grid_size, grid_size);

  frame_unoriented_angle_polar_order_parameter = zeros(frames_data.number_of_frames, grid_size, grid_size);
  frame_oriented_angle_polar_order_parameter = zeros(frames_data.number_of_frames, grid_size, grid_size);
  frame_unoriented_angle_nematic_order_parameter = zeros(frames_data.number_of_frames, grid_size, grid_size);
  frame_oriented_angle_nematic_order_parameter = zeros(frames_data.number_of_frames, grid_size, grid_size);
  
  for i = 1:frames_data.number_of_frames
    
    [~,subs] = histc(frames_data.frame_centers_of_mass{i}, edges, 1); 
    subs(subs==grid_size+1) = grid_size;
    for j = 1:grid_size
      for k = 1:grid_size
	indices_of_this_bin  = find(all(bsxfun(@eq, subs, [j,k]), 2));
	frame_bacteria_nums(i,j,k) = length(indices_of_this_bin);
	if frame_bacteria_nums(i,j,k) > 0	
	  angles_of_this_bin = frames_data.frame_angles{i}(indices_of_this_bin);	  
	  
	  %% Calculate the oriented angles (i.e orient the angle between -pi and pi
	  %% according to the direction of the velocity.		
	  xvel = frames_data.frame_velocities{i}(:,1);
	  yvel = frames_data.frame_velocities{i}(:,2);
	  velocities_of_this_bin = [xvel(indices_of_this_bin), yvel(indices_of_this_bin)];
	  angle_unit_vectors = [cos(angles_of_this_bin), sin(angles_of_this_bin)];
	  scalar_products = sum(angle_unit_vectors .* velocities_of_this_bin,2);

	  oriented_angle_unit_vectors = angle_unit_vectors .* sign(scalar_products);
	  oriented_angles_of_this_bin = atan2(oriented_angle_unit_vectors(:,2), oriented_angle_unit_vectors(:,1));
	  %% Remove NANs which may occur due to indefinite velocity
	  oriented_angles_of_this_bin(isnan(oriented_angles_of_this_bin)) = [];
	  
	  %% Save the parameters
	  frame_unoriented_angle_polar_order_parameter(i,j,k) = abs(mean(exp(1j*angles_of_this_bin)));
	  frame_unoriented_angle_nematic_order_parameter(i,j,k) = abs(mean(exp(2j*angles_of_this_bin)));

	  if ~isempty(oriented_angles_of_this_bin)
	    frame_oriented_angle_polar_order_parameter(i,j,k) = abs(mean(exp(1j*oriented_angles_of_this_bin)));
	    frame_oriented_angle_nematic_order_parameter(i,j,k) = abs(mean(exp(2j*oriented_angles_of_this_bin)));		
	  else
	    frame_oriented_angle_polar_order_parameter(i,j,k) = NaN;
	    frame_oriented_angle_nematic_order_parameter(i,j,k) = NaN;
	  end
	  
	else
	  frame_unoriented_angle_polar_order_parameter(i,j,k) = NaN;
	  frame_oriented_angle_polar_order_parameter(i,j,k) = NaN;
	  frame_unoriented_angle_nematic_order_parameter(i,j,k) = NaN;
	  frame_oriented_angle_nematic_order_parameter(i,j,k) = NaN;
	end
      end
    end
  end

  %% Note: there are still many NaNs here, as well as bacteria nums which are 0.
  %% The vectors are not all the same length. But future users of these data will take care of it.
  all_bin_bacteria_nums = reshape(frame_bacteria_nums, 1, []);
  all_bin_unoriented_angle_polar_order_parameter = reshape(frame_unoriented_angle_polar_order_parameter, 1, []);
  all_bin_unoriented_angle_nematic_order_parameter = reshape(frame_unoriented_angle_nematic_order_parameter, 1, []);
  all_bin_oriented_angle_polar_order_parameter = reshape(frame_oriented_angle_polar_order_parameter, 1, []);
  all_bin_oriented_angle_nematic_order_parameter = reshape(frame_oriented_angle_nematic_order_parameter, 1, []);

end
