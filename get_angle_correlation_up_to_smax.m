function ac = get_angle_correlation_up_to_smax(path, smax)
  ac = zeros(1, smax-1);
  for s = 1:(smax-1)
    current_sum = 0;
    for t = 1:(smax-s)
      current_sum = current_sum + abs(cos(path.angles(t + s) - path.angles(t)));
    end
    ac(s) = current_sum / (smax-s);
  end
end
