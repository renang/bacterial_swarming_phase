%% A usual image has 1024x1024 pixels. When the optical flow computes velocity,
%% divides the images into blocks (say, 32), so the resultant velocity field
%% is only of size 32x32. This function calculates bw image pixel sums in the
%% specified blocks sizes, and masks them according to a threshold given
%% in the configuration file. 
function mask = get_low_resolution_mask(frame_number)
  config = load_configuration;

  filenames = get_all_image_filenames_from_folder(config.image_folder);
  filename = filenames{frame_number};
  [im, bw] = acquire_image_config_quick(filename);

  %% Count pixels in blocks.
  num_of_blocks_per_row = config.optical_flow.diluteQuiverFactor;
  block_length = size(bw,1) / num_of_blocks_per_row;
  block_area = block_length * block_length;
  reshaped_bw = reshape(bw, block_length, num_of_blocks_per_row, block_length, num_of_blocks_per_row);
  pixel_counts = reshape(sum(sum(reshaped_bw,1),3), num_of_blocks_per_row, num_of_blocks_per_row);

  normalized_counts = pixel_counts / block_area;

  mask = normalized_counts >= config.optical_flow_pixel_count_threshold;

end
