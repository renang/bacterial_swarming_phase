function correlation = angle_correlation(b1, b2, angles)
  a1 = angles(b1);
  a2 = angles(b2);    
  angle_diff = min(abs(a1 - a2), pi - abs(a1 - a2));
  correlation =  cos(2*angle_diff);
end
