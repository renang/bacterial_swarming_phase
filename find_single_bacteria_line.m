function [bacteria_pixels, line_points] = find_single_bacteria_line(component_image, polygon_shape_inserter)
  
  indices = find(component_image == 1);
  [y x] = ind2sub(size(component_image), indices);
  x_cm = mean(x);
  y_cm = mean(y);
  cm = [x_cm y_cm];
  bounding_box = min_bounding_box([x y]');

  %% Now find which of the center points are the ones you want (you want the center points on the edges of the rectangle which are furthest away from you).

  [p1 p2 p3 p4] = sorted_points_of_bounding_rectangle(bounding_box);

  if norm(cm - p1) > norm(cm - p2)
    lp1 = p1;
    lp2 = p3;
  else
    lp1 = p2;
    lp2 = p4;
  end

  %% Now we might have to fix some of the line points, in case the x-y values go out of the boundaries of the image.
  for i = 1:2
    %% Fix too small x and y components
    if lp1(i) < 1
      diff = lp1 - cm;
      component_diff = diff(i);
      lp1 = lp1 - diff .* (lp1(i)-1)/component_diff;
    end

    if lp2(i) < 1
      diff = lp2 - cm;
      component_diff = diff(i);
      lp2 = lp2 - diff .* (lp2(i)-1)/component_diff;
    end


    %% Fix too large x and y components
    %% 3-i, because x and y are reversed in images, yes?
    if lp1(i) > size(component_image, 3-i)
      diff = lp1 - cm;
      component_diff = diff(i);
      lp1 = lp1 - diff .* (lp1(i)-size(component_image, 3-i))/component_diff;
    end

    if lp2(i) > size(component_image, 3-i)
      diff = lp2 - cm;
      component_diff = diff(i);
      lp2 = lp2 - diff .* (lp2(i)-size(component_image, 3-i))/component_diff;
    end    
  end

  lp1 = int32(lp1);
  lp2 = int32(lp2);  
  line_points = {[quick_2d_sub2ind(size(component_image), lp1(2), lp1(1))  quick_2d_sub2ind(size(component_image), lp2(2), lp2(1))]}; 

  bacteria_pixels = {component_image};
end


%%      rectangles = {draw_tilted_rectangle_around_line(p2(2), p2(1)),p4(2), p4(1),4,rectangle_image, polygon_shape_inserter)};

  %% yellow_circle_inserter = vision.ShapeInserter('Shape', 'Circles', 'Fill', true, 'FillColor', 'Custom','CustomFillColor',uint8([255 255 0]));

  %% blue_circle_inserter = vision.ShapeInserter('Shape', 'Circles', 'Fill', true, 'FillColor', 'Custom','CustomFillColor',uint8([0 0 255]));
  
  %% line_inserter = vision.ShapeInserter('Shape', 'Lines', 'BorderColor', 'custom', 'CustomBorderColor', uint8([0 255 0]));

  %% rect_inserter = vision.ShapeInserter('Shape', 'Polygons', 'BorderColor', 'custom', 'CustomBorderColor', uint8([255 0 0]));

  %% colored = double(zeros(size(component_image,1), size(component_image,2), 3));
  %% colored(:,:,1) = component_image;
  %% colored(:,:,2) = component_image;
  %% colored(:,:,3) = component_image;

  %% colored = step(yellow_circle_inserter, colored, int32([x_cm y_cm 3]));

  %% colored = step(blue_circle_inserter, colored, int32([rx1 ry1 2]));
  %% colored = step(blue_circle_inserter, colored, int32([rx2 ry2 2]));
  %% colored = step(blue_circle_inserter, colored, int32([rx3 ry3 2]));
  %% colored = step(blue_circle_inserter, colored, int32([rx4 ry4 2]));
  
  %% [y1 x1] = ind2sub(size(colored), line_points{1}(1));
  %% [y2 x2] = ind2sub(size(colored), line_points{1}(2));
  %% colored = step(line_inserter, colored, int32([x1 y1 x2 y2]));
  %% colored = step(rect_inserter, colored, [rx1 ry1 rx2 ry2 rx3 ry3 rx4 ry4]);
  
  %% figure; imshow(colored);
  %% waitforbuttonpress;
  %% close all
  

  
  %% close all
  %% figure; imshow(component_image);
  %% figure; imshow(rectangle_image);
  %% waitforbuttonpress;
