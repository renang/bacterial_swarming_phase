%%% Assume that there is only one connected component in component_image, given by pixel list and region.
function [r_im, line_points] = get_bacteria_rectangle_images(im, component_image, pixel_list, region, polygon_shape_inserter)

  if is_component_single_bacteria(im, pixel_list, region, component_image)
    [r_im, line_points] = find_single_bacteria_line(component_image, polygon_shape_inserter);    
  else
    force = 0;
    [initial_r_im, initial_line_points] = find_lines(component_image, force,polygon_shape_inserter);
    [r_im, line_points] = fix_overlaps_in_bacteria(initial_r_im, initial_line_points, component_image);
  end
    
end