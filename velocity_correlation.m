function correlation = velocity_correlation(b1, b2, velocities)
  v1 = velocities(b1,:);
  v2 = velocities(b2,:);      
  correlation  = v1 * v2';
  if correlation ~= 0
    correlation  = correlation / (norm(v1) * norm(v2));
  end
end
