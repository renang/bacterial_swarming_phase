function [average_correlations, average_correlations_by_neighbor] = average_correlation_in_sequence(all_bacteria_paths, frames)
  config = load_configuration;
  
  total_bacteria = {0 0 0};
  average_correlations = {0 0 0};
  average_correlations_by_neighbor = {0 0 0};
  
  for i = frames
    [bcm, ba, bv, path_lengths] = frame_data_from_paths(i, all_bacteria_paths);

    %% correlation_list = { {@angle_correlation, false, ba}, {@velocity_correlation, true, bv}, {@velocity_flow_correlation, true, bv} };
    correlation_list = { {@angle_correlation, false, ba}, {@velocity_correlation, true, bv}};
    
    %% You will inevitably ignore some velocities because they are too small / don't exist. Find out the size of the output:
    bad_indices = [find(isnan(bv(:,1))); ...
		   find(sqrt(sum(bv .* bv,2)) <= config.ignore_small_speed); ...
		   find(sqrt(sum(bv .* bv,2)) >= config.ignore_large_speed); ...
		   find(path_lengths < config.ignore_small_paths)];

    filtered_indices = setdiff(1:size(bcm,1), bad_indices);
    all_indices = 1:size(bcm,1);
    
    for counter = 1:length(correlation_list)
      correlation_struct = correlation_list{counter};
      correlation_func = correlation_struct{1};
      should_filter_indices = correlation_struct{2};
      trait = correlation_struct{3};

      if should_filter_indices
	indices = filtered_indices;
      else
	indices = all_indices;
      end

      [ac, acn] = average_correlation_in_frame(indices, bcm, trait, correlation_func);
      temp = zeros(1, config.correlation_neighbor_num);
      to_take = min(config.correlation_neighbor_num, length(acn));
      temp(1:to_take) = acn(1:to_take);
      acn = temp;

      average_correlations{counter} = average_correlations{counter} + ac * length(indices);
      average_correlations_by_neighbor{counter} = average_correlations_by_neighbor{counter} + acn * length(indices);
      total_bacteria{counter} = total_bacteria{counter} + length(indices);
    end
  end

  for i = 1:length(average_correlations)
    average_correlations{i} = average_correlations{i} / total_bacteria{i};
    average_correlations_by_neighbor{i} = average_correlations_by_neighbor{i} / total_bacteria{i};
  end

end    
