%%% A while ago, do_everything.m did not save all the correlations from the optical flow. This function takes a .mat file created by this old version of do_everything, calculates the optical flow correlations, and saves them. 

function reload_correlations_and_save_optical_flow(filename)

load(filename);
config = load_configuration;

for i = 1:(length(density_histogram)-1)
  if density_histogram(i) > 0
    frames = find(frame_indices == i);

    edges = linspace(0,1024*sqrt(2), config.correlation_bin_num + 1);
    edges = edges(1:end-1);

    temp_data = data;
    temp_data.nFrames = length(frames);
    temp_data.pu = temp_data.pu(frames,:,:);
    temp_data.pv = temp_data.pv(frames,:,:);

    %% This will let you take the correlations of optical flow velocities only for blocks
    %% for which there were actually bacteria in the image. 
    mask = zeros(size(temp_data.pu));    
    for j = 1:length(frames)
      mask(j,:,:) = get_low_resolution_mask(frames(j));
    end    
    indices_to_take = find(mask > 0);

    
    [inSpeed, inDir, inVort, inSpeedW, inDirW, inVortW, ttt] = get_data_for_space_corr(temp_data, indices_to_take);

    correlations{i}.inDir = linearly_interpolate_zeros(inDir);    
    correlations{i}.inSpeed = linearly_interpolate_zeros(inSpeed);
    correlations{i}.inVort = linearly_interpolate_zeros(inVort);    
    correlations{i}.inDirW = linearly_interpolate_zeros(inDirW);
    correlations{i}.inSpeedW = linearly_interpolate_zeros(inSpeedW);
    correlations{i}.inVortW = linearly_interpolate_zeros(inVortW);

    [time_inSpeed, time_inDir, time_inVort, time_ttt] = get_data_for_time_corr(temp_data);

    correlations{i}.time_inSpeed = linearly_interpolate_zeros(time_inSpeed);
    correlations{i}.time_inVort = linearly_interpolate_zeros(time_inVort);    
    correlations{i}.time_inDir = linearly_interpolate_zeros(time_inDir);    
  else
    correlations{i} = {};
  end  
end

save_correlations(correlations);
save_velocity_histograms_for_optical_flow(density_histogram, frame_indices, data);
out_filename = fullfile(config.output_folder, strrep([datestr(now) '.mat'], ':', '_'));
save(out_filename);

end
