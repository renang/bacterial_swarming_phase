function curvature = get_curvature_from_velocities(velocities)
  config = load_configuration();
  v = velocities;
  
  if isempty(v)
    curvature = [];
  else
    v(:,1) = smoothdata(v(:,1), 'rlowess', config.smoothing_for_curvature);
    v(:,2) = smoothdata(v(:,2), 'rlowess', config.smoothing_for_curvature);
    v_noise = v + rand * 0.000001; % To prevent division by 0
    magnitudes = sqrt(sum(v_noise .* v_noise, 2));
    T = v_noise ./ magnitudes;
    curvature = [];
    dT = [diff(T(:,1)) diff(T(:, 2))];    
    if ~isempty(dT)    
      % When dividing by the magnitudes, refer to the midpoints
      mean_magnitudes = (magnitudes(2:end) + magnitudes(1:end-1)) / 2;
      curvature = sqrt(sum(dT .* dT, 2)) ./ mean_magnitudes;
    end
  end
end
