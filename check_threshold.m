%%% This function shows the resultant pictures you get when converting from grayscale to bw using the parameter threshold_level. It is useful for when you want to find out what is the best threshold level to use for a sequence of images. 
function check_threshold(filename, threshold_level)
  [im, bw] = get_clean_images(filename, threshold_level);
  figure; imshow(im);
  figure; imshow(bw);
end