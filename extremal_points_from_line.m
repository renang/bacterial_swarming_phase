function [x1,y1,x2,y2] = extremal_points_from_line(line_x, line_y, component_image)
  line_indices = sub2ind(size(component_image), line_y, line_x);
  maxx = max(line_x);
  minx = min(line_x);
  maxy = max(line_y);
  miny = min(line_y);
  points = [];
  if find(line_indices == sub2ind(size(component_image), maxy, maxx))
    points(end+1) = sub2ind(size(component_image), maxy, maxx);
  end
  if find(line_indices == sub2ind(size(component_image), maxy, minx))
    points(end+1) = sub2ind(size(component_image), maxy, minx);
  end
  if find(line_indices == sub2ind(size(component_image), miny, maxx))
    points(end+1) = sub2ind(size(component_image), miny, maxx);
  end
  if find(line_indices == sub2ind(size(component_image), miny, minx))
    points(end+1) = sub2ind(size(component_image), miny, minx);
  end	
  points = unique(points);
  [edge_y, edge_x] = ind2sub(size(component_image), points);  
  if length(points) == 1
    %% It may be that points is just a single point!
    %% This is a degenerate case.
    x1 = edge_x(1); x2 = edge_x(1); y1 = edge_y(1); y2 = edge_y(1);
  else
    x1 = edge_x(1); y1 = edge_y(1); x2 = edge_x(2); y2 = edge_y(2);
  end
end