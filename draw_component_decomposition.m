function component_decomposition = draw_component_decomposition(r, component_image)

  %% Hardwritten, because calling the colormap function in matlab is opens a figure, which is not always desirable.
  cm = [0         0    0.5625; 0         0    0.6250; 0         0    0.6875; 0         0    0.7500; 0         0    0.8125; 0         0    0.8750;         0         0    0.9375;         0         0    1.0000;
        0    0.0625    1.0000;         0    0.1250    1.0000;         0    0.1875    1.0000;         0    0.2500    1.0000;          0    0.3125    1.0000;         0    0.3750    1.0000;         0    0.4375    1.0000;         0    0.5000    1.0000;         0    0.5625    1.0000;         0    0.6250    1.0000;         0    0.6875    1.0000;         0    0.7500    1.0000;         0    0.8125    1.0000;         0    0.8750    1.0000;         0    0.9375    1.0000;         0    1.0000    1.0000;    0.0625    1.0000    0.9375;    0.1250    1.0000    0.8750;    0.1875    1.0000    0.8125;    0.2500    1.0000    0.7500;    0.3125    1.0000    0.6875;    0.3750    1.0000    0.6250;    0.4375    1.0000    0.5625;    0.5000    1.0000    0.5000;    0.5625    1.0000    0.4375;    0.6250    1.0000    0.3750;    0.6875    1.0000    0.3125;    0.7500    1.0000    0.2500;0.8125    1.0000    0.1875;    0.8750    1.0000    0.1250;    0.9375    1.0000    0.0625;    1.0000    1.0000         0;    1.0000    0.9375         0;    1.0000    0.8750         0;    1.0000    0.8125         0;    1.0000    0.7500         0;    1.0000    0.6875         0;    1.0000    0.6250         0;    1.0000    0.5625         0;    1.0000    0.5000         0;    1.0000    0.4375         0;    1.0000    0.3750         0;    1.0000    0.3125         0;    1.0000    0.2500         0;    1.0000    0.1875         0;    1.0000    0.1250         0;    1.0000    0.0625         0;    1.0000         0         0;    0.9375         0         0;    0.8750         0         0;    0.8125         0         0;    0.7500         0         0;    0.6875         0         0;    0.6250         0         0;    0.5625         0         0;    0.5000         0         0];


  how_many_to_draw = length(r);
  num_of_times_to_copy = ceil(how_many_to_draw / size(cm,1));

  cm = repmat(cm, num_of_times_to_copy, 1);
  ix = randperm(size(cm,1));
  shuffled_colors = cm(ix,:);
		
  component_decomposition = uint8(zeros(size(component_image,1), size(component_image,2), 3));
  for i = 1:length(r)
    current_color = 1+floor(size(cm,1)*(i-1)/length(r));

    s = zeros(size(component_image));
    s(r{i}) = 1;

    %%component_decomposition = imoverlay(component_decomposition, s, shuffled_colors(i,:));    
    component_decomposition = imoverlay(component_decomposition, s, cm(current_color,:));    
  end
end