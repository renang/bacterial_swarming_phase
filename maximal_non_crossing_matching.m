%% Based on "Efficient labelling algorithms for the maximum non crossing matching problem" by Malucelli, Ottmann and Pretolani.
function [edge_matching, total_weight] = maximal_non_crossing_matching(origin, destination, adj, weights)

  node_labels = zeros(size(destination));
  edge_labels = ones(size(adj));
  edge_labels = -Inf * edge_labels;
  
  for i = 1:length(origin)
    connected_indices = find(adj(i,:));
    if ~isempty(connected_indices)
      for j = connected_indices
	if j == 1
	  edge_labels(i,j) = weights(i,j);
	else
	  edge_labels(i,j) = weights(i,j) + max(node_labels(1:j-1));
	end	
      end

      for j = connected_indices
	node_labels(j) = max(node_labels(j), edge_labels(i,j));
      end      
    end      
  end
  
  %% Done labeling, take the proper labels now.
  k = max(max(edge_labels));
  [i1, j1] = quick_2d_ind2sub(size(adj), find(edge_labels == k, 1));
  edge_matching = [i1, j1];
  edge_labels(i1,:) = -Inf;
  edge_labels(:,j1) = -Inf;
  total_weight = k;
  while true
    k = max(max(edge_labels));
    if k == -Inf
      break
    end

    [i2, j2] = quick_2d_ind2sub(size(adj), find(edge_labels == k, 1));
    %% TODO: you can make this past a tiny bit faster by removing the check, and using the "find" function in parts. It's not that interesting for now, though.
    if do_edges_not_intersect(i1, j1, i2, j2)
      i1 = i2; j1 = j2;
      edge_matching(end+1,:) = [i1, j1];
      edge_labels(i1,:) = -Inf;
      edge_labels(:,j1) = -Inf;
      total_weight = total_weight + k;
    else
      edge_labels(i2,j2) = -Inf;
    end    

  end
  
end

%% Tells if the edge (i1,j1) intersects the edge (i2, j2).
function a = do_edges_not_intersect(i1, j1, i2, j2)
  a = (i1 < i2 && j1 < j2) || (i1 > i2 && j1 > j2);
end