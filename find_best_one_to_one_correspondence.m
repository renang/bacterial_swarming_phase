%%% p1 = [x1 y1] are the centers of mass of bacteria in image1.
%%% p2 = [x2 y2] are the centers of mass of bacteria in image2.
%%% p1 and p2 do not have to be the same size.
%%% The function finds the best correspondence between them, so that the distance is minimized. It returns two lists of indexes: (x1,y1)[i] corresponds to (x2,y2)[i].
%%% is_original_in_origin_or_destination: tells you wether the original bacteria (you give its index) is in p1, the origin, or p2, the destination. 1 if p1, 2 if p2.
function [i1 i2] = find_best_one_to_one_correspondence(p1, p2, original_bacteria_index, is_original_in_origin_or_destination , max_allowed_distance)

  if size(p2,1) >= size(p1,1)

    if isempty(p1)
      i1 = []; i2 = []; return;
    end
    
    n = size(p2,1);
    k = size(p1,1);
    
    all_choices = nchoosek(1:n, k);

    least_distances = Inf;
    best_p = [];
    
    for i = 1:size(all_choices,1)
      choice = all_choices(i,:);
      permutations = perms(choice);
      for j = 1:size(permutations,1)
	p = permutations(j,:);		
	distances = sum((p1 - p2(p,:)) .^2, 2);
	%% TODO: even better, especially for long bacteria - allow longer jumps if they are along the axis of the bacteria! Zomg! (say, allow a jump of 8 + 0.5*bacteria_length * cos(angle_between_bacteria_and_jump). This could actually work.

	if is_original_in_origin_or_destination == 1
	  d = distances(1);
	elseif is_original_in_origin_or_destination == 2
	  idx = find(p == 1,1);
	  if ~isempty(idx)
	    d = distances(1);
	  else
	    d = 0;
	  end
	    
	else
	  %% We don't check the distance of one particular bacteria.
	  d = 0;
	end	  	  
	
	if d <= max_allowed_distance ^ 2
	%%if all(distances <= max_allowed_distance ^ 2)	  
	  if sum(distances) < least_distances	    
	    least_distances = sum(distances);
	    best_p = p;
	  end

	end
	%% %% DEBUG DEBUG DEBUG
	%% disp(total_distances);
	%% qwe = zeros(size(im,1),size(im,2),3);
	%% for t = 1:size(p,2)
	%%   x1 = c1(bac1(t), 1); y1 = c1(bac1(t), 2);
	%%   x2 = c2(p(t), 1); y2 = c2(p(t), 2);
	%%   qwe = step(line_inserter, qwe, int32([x1 y1 x2 y2]));
	%% end
	%% qwe = imoverlay(qwe, im2bw(piv_image_1), [.3 .3 .1]);
	%% qwe = imoverlay(qwe, im2bw(piv_image_2), [1 .3 .3]);

	%% qwe = qwe(low_y:high_y, low_x:high_x,:);
	
	%% figure; imshow(qwe);
	%% waitforbuttonpress;	
      end

    end

    %% It might be that you never found any coresspondence points (all too far away).
    if ~isempty(best_p)
      i1 = 1:k;
      i2 = best_p;
    else
      i1 = [];
      i2 = [];
    end
        
  else
    [i2, i1] = find_best_one_to_one_correspondence(p2, p1, original_bacteria_index, 3-is_original_in_origin_or_destination, max_allowed_distance);

  end

end