function plot_cluster_summary_graphs(all_clustering_fits, base_output_directory, bacteria_size)
  config = load_configuration();
  for i = 1:length(config.clustering_test_names)
    test_name = config.clustering_test_names{i};
    test_indices = find(arrayfun(@(RIDX) strcmp(all_clustering_fits{RIDX}.name, test_name), 1:length(all_clustering_fits)));
    fits_of_this_test = all_clustering_fits(test_indices);


    slope_absolute_values = zeros(1, length(config.density_bins)-1);
    relative_portion = zeros(1, length(config.density_bins)-1);

    
    for i = 1:length(config.density_bins)-1
      density = config.density_bins(i);
      
      density_indices = find(arrayfun(@(RIDX) fits_of_this_test{RIDX}.density == density, 1:length(fits_of_this_test)));
      if ~isempty(density_indices)
	fits_of_this_density = fits_of_this_test(density_indices);
	b_param = extractfield([fits_of_this_density{:}], 'b');
	slope_absolute_values(i) = mean(abs(b_param));

	c_param = extractfield([fits_of_this_density{:}], 'c');
	relative_portion(i) = mean(c_param);
      else
	slope_absolute_values(i) = 0;
	relative_portion(i) = 0;
      end
    end

    
    figure;
    plot(config.density_bins(1:end-1), slope_absolute_values);
    title({'Slope absolute values for ', test_name}, 'Interpreter', 'None');
    ylabel('Absolute value of slope of power law fit');
    xlabel('Density');
    output_filename = fullfile(base_output_directory, test_name, [num2str(bacteria_size) '_summary_absolute_slope.png']);
    print(output_filename, '-dpng');
    close();
    
    output_filename_text = fullfile(base_output_directory, test_name, [num2str(bacteria_size) '_summary_absolute_slope.txt']);
    if size(config.density_bins,1)>size(config.density_bins,2)
        ddd=[config.density_bins(1:end-1), slope_absolute_values];
    else
        ddd=[config.density_bins(1:end-1)', slope_absolute_values'];
    end
    save(output_filename_text,'ddd','-ascii');
    
    figure;
    plot(config.density_bins(1:end-1), relative_portion);
    title({'Relative portion of largest cluster for ', test_name}, 'Interpreter', 'None');
    ylabel('Relative portion');
    xlabel('Density');
    output_filename = fullfile(base_output_directory, test_name, [num2str(bacteria_size) '_summary_relative_portion.png']);
    print(output_filename, '-dpng');
    close();
    output_filename_text = fullfile(base_output_directory, test_name, [num2str(bacteria_size) '_summary_relative_portion.txt']);
    if size(config.density_bins,1)>size(config.density_bins,2)
        ddd=[config.density_bins(1:end-1), relative_portion];
    else
        ddd=[config.density_bins(1:end-1)', relative_portion'];
    end
    save(output_filename_text,'ddd','-ascii');
  end

end
