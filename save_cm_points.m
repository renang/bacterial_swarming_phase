%%% This function saves all the positions of all the bacteria found in a sequene of images. The output is one file per bacteria, with the name of the file being the index of the bacteria. The contents are the time (frame number) and the position at that time.
%%% foldername - the folder in which the data files will be saved. 
%%% all_bacteria_paths - a cell array of bacteria path structures (as returned by process_folder).
function save_cm_points(foldername, all_bacteria_paths)
  for i = 1:length(all_bacteria_paths)
    t = all_bacteria_paths{i}.first_frame:all_bacteria_paths{i}.last_frame;
    t = t';
    filename = fullfile(foldername, sprintf('bac%06d.txt',i));
    fid = fopen(filename,'w');    
    fprintf(fid, 'time\tx_pos\ty_pos\n');
    fprintf(fid, '%d\t%f\t%f\n', [t all_bacteria_paths{i}.cm_points]');
    fclose(fid);	    
  end
end