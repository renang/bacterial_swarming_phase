function bw_bacteria = create_bw_bacteria_image(all_bacteria_pixels, im)
  bw_bacteria = zeros(size(im));
  for i = 1:length(all_bacteria_pixels)
    bw_bacteria(all_bacteria_pixels{i}) = 1;
  end


end
    