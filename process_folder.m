%% First, get all the files in the directory given by raw_image_directory.
filenames = get_all_image_filenames_from_folder(raw_image_foldername);
  
%% For debugging purposes, sometimes you do not want to reload all the data all the time. In this case, put "false" in "should_acquire_data". Otherwise - meaning, during normal operation of the script - it should be set to "true".
should_acquire_data = true;
if should_acquire_data
  %% For debugging / partial checking, you can choose how many files you want to process by setting this parameter, "force_num_of_files". If this parameter exists and is not 0 (for example, because it was given in "call_process_folder", or if you added it yourself; it should be an integer in any case), then we will only process that many files.
  %% (an alternative way to do this is to put only a limited number of images in the folder you are processing, but this is a bit cumbersome).
  max_iteration = length(filenames);
  if exist('force_num_of_files', 'var')
    if force_num_of_files ~= 0
      max_iteration = force_num_of_files;
    end
  end

  all_frame_centers = cell(1,max_iteration);
  all_frame_angles = cell(1,max_iteration);
  all_frame_line_points = cell(1,max_iteration);
  all_frame_bacteria_pixels = cell(1,max_iteration);
  all_frame_densities = zeros(1,max_iteration);
  
  parfor i = 1:max_iteration
    disp(['Processing file number ' num2str(i) ': ' filenames{i}]);
    [c, a, blp, bp, density] = call_process_file(filenames{i});
    all_frame_centers{i} = c;
    all_frame_angles{i} = a;
    all_frame_line_points{i} = blp;
    all_frame_bacteria_pixels{i} = bp;
    all_frame_densities(i) = density;
  end
    
end

%% Now that you have all the data, create matchings by tracking every two consecutive frames.
all_matchings = {};
for i = 2:length(all_frame_centers)
  disp(['Processing interval number ' num2str(i-1) ':']);
  c1 = all_frame_centers{i-1};
  c2 = all_frame_centers{i};
  matchings = track_bacteria_between_two_frames(c1,c2);
  all_matchings{end+1} = matchings;
end    


%% Finally, create paths from the matchings you have created. Inside this script we also add corrections to bacteria which were "lost" and reappear a short number of frames and a close distance away.
bacteria_paths_from_matching;
