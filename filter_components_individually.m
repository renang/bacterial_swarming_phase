function new_bw = filter_components_individually(im, bw, iteration_num)
  bw_temp = bw;
  for i = 1:iteration_num
    new_bw = zeros(size(bw_temp));
  
    cc = bwconncomp(bw, 4);
    regions = regionprops(cc,'basic');
    for c = 1:cc.NumObjects
      gs = get_grayscale_component_image_from_number(c,cc,regions,im);
      
      if is_component_convex(im,  cc.PixelIdxList{c}, regions(c)) || size(gs,1) < 8 || size(gs,2) < 8
	new_bw(cc.PixelIdxList{c}) = 1;
      else      
	gs = adapthisteq(gs, 'NBins', 1024, 'Distribution', 'exponential');
	gs = imadjust(gs);
	clean_gs = medfilt2(gs,[3 3]);
	%% TODO: you need to somehow decide on the proper level of thresholding. This is KEY! In general, you might want to reconsider your conversion to black-white methods. Maybe you should look at the Hessian method at some point.
	threshold_level = 0.53;
	%%threshold_level = graythresh(clean_gs);
	clean_component = im2bw(clean_gs, threshold_level);
	clean_component = bwareaopen(clean_component, 10, 4);
	[x1 y1 x2 y2] = get_bounded_box_coordinates_from_region(regions(c));
	new_bw(y1:y2, x1:x2) = new_bw(y1:y2, x1:x2) + clean_component;
      end    
    end
  
    bw_temp = new_bw;
  end
end  

