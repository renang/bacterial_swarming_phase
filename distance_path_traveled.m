function d = distance_path_traveled(path)
  d = sum(sqrt(sum(diff(path.cm_points).^2, 2)));
end
