warning('off', 'Images:initSize:adjustingMag');
warning('off', 'stats:kmeans:EmptyCluster');

polygon_shape_inserter =  vision.ShapeInserter('Shape', 'Polygons', 'Fill', true, 'FillColor', 'White');

tic;

config = load_configuration;

[im, bw] = acquire_image_config(filename);

if config.density_check_method == config.INTENSITY_THRESHOLD
  density = sum(sum(bw))/prod(size(bw));
elseif config.density_check_method == config.ENTROPY_THRESHOLD
  j = entropyfilt(im);
  density = sum(sum(j >= config.entropy_threshold)) / prod(size(bw));
end

%% Note: it is important that at this stage there are no more very small components (say, of size 5). However, if you have done everything correctly, at least a bwareaopen(bw, 50,4) should have been run, so there are no blobs smaller than 50.

cc = bwconncomp(bw, 4);
regions = regionprops(cc,'basic');

all_bacteria_pixels = {};
all_bacteria_line_points = {};


ratios = [];

for c = 1:cc.NumObjects
  fprintf(['\rProcessing cluster number ' num2str(c) '/' num2str(cc.NumObjects)]);
  component_image = get_component_image_from_number(c,cc,regions,im);
  gs = get_grayscale_component_image_from_number(c,cc,regions,im);
    
  [r_im, line_points] = get_bacteria_rectangle_images(im, component_image, cc.PixelIdxList{c}, regions(c), polygon_shape_inserter);

  conversion_func1 = @(x) component_coordinates_to_image_coordinates(find(x{1}), regions(c), component_image, im);
  conversion_func2 = @(x) component_coordinates_to_image_coordinates(x{1}, regions(c), component_image, im);
  
  bacteria_pixels = arrayfun(conversion_func1, r_im, 'UniformOutput', false);
  bacteria_line_points = arrayfun(conversion_func2, line_points, 'UniformOutput', false);
  all_bacteria_pixels = [all_bacteria_pixels bacteria_pixels];
  all_bacteria_line_points = [all_bacteria_line_points bacteria_line_points];
end

centers_of_mass = [];
angles = [];
areas = [];
to_remove = [];

for i = 1:length(all_bacteria_line_points)
  p = all_bacteria_line_points{i};
  p1 = p(1); p2 = p(2);
  [y1 x1] = ind2sub(size(im), p1);
  [y2 x2] = ind2sub(size(im), p2);
  mid_x = 0.5 * (x1 + x2);
  mid_y = 0.5 * (y1 + y2);
  centers_of_mass(end+1,:) = [mid_x mid_y];
  angles(end+1) = atan(double(y2-y1)/double((x2-x1)));
  areas(end+1) = size(all_bacteria_pixels{i},1);

  %% Remove bacteria which are "too short", as these are probably artifacts.
  l = sqrt(double((y2-y1)*(y2-y1) + (x2-x1)*(x2-x1)));
  if l < 7
    to_remove(end+1) = i;
  end  

  %% TODO: remove bacteria which are too small by area. 
end


%% TODO: consider also removing some bacteria whose centers of mass are too close to each other. It seems like 4 is an ok distance to remove from.
%% Until then, for now at least, remove bacteria which have exactly the same center of mass. Remove the smallest of them.
cm_indices = quick_2d_sub2ind(size(im), int32(centers_of_mass(:,1)), int32(centers_of_mass(:,2)));
unique_indices = unique(cm_indices);
n = hist(cm_indices, double(unique_indices));
non_unique_indices = unique_indices(find(n>1));
if ~isempty(non_unique_indices)
  for i = non_unique_indices'
    where_it_happens = find(cm_indices == i)';
    largest_area = -Inf;
    to_keep_index = 0;
    for j = where_it_happens
      if areas(j) > largest_area
	to_keep_index = j;
	largest_area = areas(j);
      end
    end
    where_it_happens(where_it_happens == j) = [];
    to_remove = [to_remove where_it_happens];
  end    
end


angles(to_remove) = [];
areas(to_remove) = [];
centers_of_mass(to_remove,:) = [];
all_bacteria_line_points(to_remove) = [];
all_bacteria_pixels(to_remove) = [];
 

%% Now draw everything you have! (if you want)
should_draw = false;
black_image = zeros(size(im));
if should_draw  
  colored_bacteria = create_colored_bacteria_image(centers_of_mass, all_bacteria_line_points, all_bacteria_pixels, im);
  just_lines = create_colored_bacteria_image(centers_of_mass, all_bacteria_line_points, all_bacteria_pixels, black_image, true, true, false);
  bw_bacteria = create_bw_bacteria_image(all_bacteria_pixels, im);

  figure; imshow(colored_bacteria);
  figure; imshow(just_lines);
  figure; imshow(bw_bacteria); drawnow;
end

toc;
