function plot_diffusivity_summary_graphs(diffusivity_summary, all_diffusivity_fits, base_output_directory, bacteria_size)
  config = load_configuration();

  %% Plotting mean squared distance.
  msds = zeros(1, length(config.density_bins)-1);
  for i = 1:length(config.density_bins)-1
    if ~isempty(diffusivity_summary{i})
      msds(i) = mean(diffusivity_summary{i});    
    end    
  end
  
  figure;
  plot(config.density_bins(1:end-1), msds, '.-');
  title(['Mean square displacement for s = ' num2str(config.diffusivity_summary_path_length)]);
  ylabel('msd');
  xlabel('Density');
  output_filename = fullfile(base_output_directory, config.diffusivity_test_name, [num2str(bacteria_size) '_summary_msd.png']);
  print(output_filename, '-dpng');
  close();
  
  %% Plotting exponents
  exponents = zeros(1, length(config.density_bins)-1);  
  for i = 1:length(config.density_bins)-1
    density = config.density_bins(i);
      
    density_indices = find(arrayfun(@(RIDX) all_diffusivity_fits{RIDX}.density == density, 1:length(all_diffusivity_fits)));
    if ~isempty(density_indices)
      fits_of_this_density = all_diffusivity_fits(density_indices);
      b_param = extractfield([fits_of_this_density{:}], 'alpha');
      exponents(i) = mean(b_param);	
    else
      exponents(i) = 0;
    end
  end

  figure;
  plot(config.density_bins(1:end-1), exponents, '.-');
  title(['Exponents for ' config.diffusivity_test_name], 'Interpreter', 'None');
  ylabel('Exponent of power law fit');
  xlabel('Density');
  output_filename = fullfile(base_output_directory, config.diffusivity_test_name, [num2str(bacteria_size) '_summary_exponent.png']);
  print(output_filename, '-dpng');
  close();
  
  %save to file
  text_output_filename = fullfile(base_output_directory, config.diffusivity_test_name, [num2str(bacteria_size) '_summary_exponent.txt']);
  if size(config.density_bins,1)>size(config.density_bins,2)
    ddd=[config.density_bins(1:end-1),  exponents];
  else
    ddd=[config.density_bins(1:end-1)',  exponents'];
  end
  save(text_output_filename,'ddd','-ascii');
  
  
end
