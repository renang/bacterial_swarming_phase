%%% This function gets a filename of an image and a threshold_level (a number from 0 to 1). It opens the image and cleans it up a bit, and converts it to black and white according to the threshold level given. It returns both the enhanced and the bw image.
function [im, bw] = get_clean_images(filename, threshold_level)  
  im = imcomplement(imread(filename));

  %% im = adapthisteq(im, 'NBins', 1024, 'Distribution', 'exponential');
  %% im = imadjust(im);

  bw = im2bw(im, 1 - threshold_level);
  bw = bwareaopen(bw, 50, 4);

  %% There may be some noise. You can denoise by comparing with the canny edge detection, which is quite noise-resistant.
  cc = bwconncomp(bw, 4);    
  edges = edge(im, 'canny');
  edge_pixels = find(edges == 1);
  
  for c = 1:cc.NumObjects
    if isempty(intersect(cc.PixelIdxList{c}, edge_pixels))
      bw(cc.PixelIdxList{c}) = 0;
    end     
  end
  
end
