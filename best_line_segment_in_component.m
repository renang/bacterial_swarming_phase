%%% This function tries to fit a line *inside* a connected component image. You give it a starting position and angle, and it returns the generated line points in the image, making sure that the points returned constitute one connected segment which is all inside the component image.
%%% Output:
%%% line_x, line_y - the x,y coordinates of the line.
%%% score - a score on how good this line is.
%%% did_work - This function might fail (for example, if the center point is not even in the component image to begin with), and returns whether or not it did in the parameter using this parameter.
function [line_x, line_y, score, did_work] = best_line_segment_in_component(center_x, center_y, theta, line_length, component_image)

  line_x = zeros(1,int32(line_length));
  line_y = zeros(1,int32(line_length));
  costheta = cos(theta);
  sintheta = sin(theta);
  
  for i=1:line_length
    x = floor(center_x + costheta * i);
    y = floor(center_y + sintheta * i);
    if (x < 1) || (x > size(component_image, 2)) || (y<1) || (y > size(component_image, 1)) || component_image(y,x) == 0
      break
    else
      line_x(i) = x;
      line_y(i) = y;
    end

  end

  last = i-1;
  for i=1:line_length
    x = floor(center_x - costheta * i);
    y = floor(center_y - sintheta * i);
    if (x < 1) || (x > size(component_image, 2)) || (y<1) || (y > size(component_image, 1)) || component_image(y,x) == 0
      break
    else
      line_x(last+i) = x;
      line_y(last+i) = y;
    end
  end

  index_of_first_bad_one = find(line_x == 0, 1);

  if (index_of_first_bad_one == 1)
    line_x = 0;
    line_y = 0;
    did_work = false;
    score = 0;
    return;
  end
  
  line_x = line_x(1:index_of_first_bad_one-1);
  line_y = line_y(1:index_of_first_bad_one-1);
  line_x(end+1) = center_x;
  line_y(end+1) = center_y;


  did_work = true;
  score = length(line_x);

end