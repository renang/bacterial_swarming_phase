function msd = get_mean_square_displacement_up_to_smax(path, smax)
  msd = zeros(1, smax-1);
  for s = 1:(smax-1)
    current_sum = 0;
    for t = 1:(smax-s)
      current_sum = current_sum + sum((path.cm_points(t+s)-path.cm_points(t))^2);
    end
    msd(s) = current_sum / (smax-s);
  end
end
