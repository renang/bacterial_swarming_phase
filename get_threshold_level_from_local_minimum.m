%% The function assumes that most of the background has high intensity, and most of the bacteria have low intensity. The histogram should then have two peaks; we return the intensity where there is a minimum between them.
function threshold = get_threshold_level_from_local_minimum(filename)

  im = imread(filename);
  
  edges = 0.5 + -1:256;
  intensities = double(im(:));
  spectrum = histc(intensities,edges);

  %% Clip edges (artifacts might occur, like saturation).  
  spectrum(1:20) = 0;
  spectrum(end-20:end) = 0;
  
  smoothed_spectrum = smooth(spectrum, 'sgolay');
  [m, ind] = max(smoothed_spectrum);
  smoothed_spectrum = smoothed_spectrum / m;

  %% Method 1 (less reliable): largest second derivative.
  differences = diff(smoothed_spectrum);
  smoothed_differences = smooth(differences,'sgolay');
  second_derivative = diff(smoothed_differences);
  [asd,second_derivative_index] = max(second_derivative);


  %% Method 2: search for places where we switch from decreasing to increasing.
  %% We find points when the graph starts increasing.
 
  potential_indices = [];
  while true
    last_negative_index = find(differences<=0, 1, 'last');
    if isempty(last_negative_index)
      break;
    end
    
    potential_indices(end+1) = last_negative_index;
    differences = differences(1:last_negative_index);
    last_positive_index = find(differences>0, 1, 'last');
    if isempty(last_positive_index)
      break;
    end
    differences = differences(1:last_positive_index);
  end

  values = smoothed_spectrum(potential_indices);
  if any( (values < 0.9) .* (values>0.1))
    %% Take the closest one to the peak?
    potential_indices(smoothed_spectrum(potential_indices) > 0.9) = [];
    potential_indices(smoothed_spectrum(potential_indices) < 0.1) = [];
    [minimal_distance, mindist_index] = min(abs(potential_indices - ind));
    derivative_index = potential_indices(mindist_index);

    %% We used to take the largest value, but this of course caused trouble sometimes. Keep here for reference for a small while.
    %% values(values > 0.9) = 0;
    %% values(values < 0.1) = 0;
    %% [maximum_value, maximum_ind] = max(values);
    %% derivative_index = potential_indices(maximum_ind);
  elseif any(values > 0.9)
    values(values < 0.1) = 1;
    [minimum_value, minimum_ind] = min(values);
    derivative_index = potential_indices(minimum_ind);
  else
    %% All values are lower than 0.1. Take the maximum again.
    [maximum_value, maximum_ind] = max(values);
    derivative_index = potential_indices(maximum_ind);
  end

  %% Generally, the difference method is prefferd. However, sometimes it gives wack results (for example, if there is no minimum at all, which can sometimes happen). In this case, the largest second derivative method might give something better.
  if derivative_index < 30 || derivative_index > 170
    best_index = second_derivative_index;
  else
    best_index = derivative_index;
  end

  threshold = best_index / 256;

  %% figure; imshow(im);
  %% figure; hold on;
  %% plot(edges,smoothed_spectrum, 'r','linewidth',1.5);
  %% plot(derivative_index-1, smoothed_spectrum(derivative_index), 'bo', 'linewidth', 2);
  %% plot(second_derivative_index-1, smoothed_spectrum(second_derivative_index), 'go', 'linewidth', 2);
end
