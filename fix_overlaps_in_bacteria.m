function [fixed_r, fixed_line_points] = fix_overlaps_in_bacteria(r, line_points, component_image)
  %% Note: it seems you want to take the *larger* one of the overlaps, yes?
  %% Or, perhaps: look if one of them is almost contained in the union of the rest of them. If so, maybe you can drop it. Depends on the almost, I guess.
  should_continue = true;
  threshold = 0.8;
  fixed_r = r;
  fixed_line_points = line_points;
  current_indices = 1:length(r);
  should_continue = true;
  while should_continue
    should_continue = false;
    l = length(fixed_r);
    smallest_area = size(component_image,1) * size(component_image,2);

    for i = 1:l
      rest = [1:(i-1) (i+1):l];
      composite = false(size(component_image));
      for j = rest
	composite = composite + fixed_r{j};     
      end
      %%composite = im2bw(composite);
      composite = composite & component_image;
      %% figure; imshow(composite);
      
      bw = fixed_r{i} & component_image;
      this_bacteria_area = sum(sum(bw));
      overlap_area = sum(sum(bw & composite));
      overlap_ratio = overlap_area ./ this_bacteria_area;
      if (overlap_ratio > threshold) && (this_bacteria_area < smallest_area)
	should_continue = true;
	to_remove = i;
	smallest_area = this_bacteria_area;
      end      
    end
    if should_continue
      fixed_r(to_remove) = [];
      fixed_line_points(to_remove) = [];
    end
  end

end    