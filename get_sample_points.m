function sample_indices = get_sample_points(white_indices, component_image)
  reducing_filter = ones(3);
  reduced_image = imcomplement(imfilter(double(imcomplement(component_image)), reducing_filter));  
  %%figure; imshow(reduced_image);

  reduced_white_indices = find(reduced_image == 1);
%%  reduced_white_indices = white_indices;
  
  %% The jump in the middle can be replaced to get any fine granulation we want (up to rounding errors).
  indices_of_indices = unique(floor(1:6:length(reduced_white_indices)));
  sample_indices = reduced_white_indices(indices_of_indices)'; 
  end
