input_filename = 'YOUR MAT FILENAME HERE';

load(input_filename, 'all_frame_centers', 'all_bacteria_paths');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Curvaturing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
total_curvatures = 0;
total_counts = 0;

for i = 1:length(all_bacteria_paths)
  path = all_bacteria_paths{i};  
  curvatures = get_curvature_from_velocities(path.velocities);
  curvatures = min(curvatures, 100); % Remove anomalies. You decide what counts as an anomaly.
  total_curvatures = total_curvatures + sum(curvatures);
  total_counts = total_counts + length(curvatures);  
end

curvature = total_curvatures / total_counts;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Structure func-ing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Concatenate all center of masses together, since we want the average 
% of the entire video.

frame_centers = [];
for i = all_frame_centers
    frame_centers = cat(1, frame_centers, i{1});
end

min_magnitude = 0.001;
max_magnitude = 0.1;
magnitude_ticks = 100;
angle_ticks = 20;

magnitude_range = linspace(min_magnitude, max_magnitude, magnitude_ticks);
angle_range = linspace(0, 2*pi, angle_ticks+1);
angle_range = angle_range(1:end-1);

mag_structure_func = zeros(1, length(magnitude_range));

for i = 1:length(magnitude_range)
  magnitude = magnitude_range(i);
  mag_structure_func(i) = structure_func_for_magnitude(magnitude, frame_centers, angle_range);  
end

output_filename = ['curvature_and_structure_' datestr(now) '.mat'];
save(output_filename, 'curvature', 'mag_structure_func');
