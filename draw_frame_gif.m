%%% This function draws an animated gif which tracks all of the bacteria in a given frame.
%%% Parameters:
%%% frame_index - which frame you want to track. The gif will start at this frame, and track all the bacteria in it, until they all go out of bounds / are lost.
%%% For the rest of the parameters / function description, see "draw_path_gif".
function draw_frame_gif(frame_index,gif_filename, all_bacteria_paths, input_folder, all_frame_centers, all_frame_line_points, all_frame_bacteria_pixels, should_draw_rest)

  bacteria_indices = [];
  
  for i = 1:length(all_bacteria_paths)    
    if frame_index >= all_bacteria_paths{i}.first_frame && frame_index <= all_bacteria_paths{i}.last_frame
      bacteria_indices(end+1) = i;
    end
  end

  draw_path_gif(bacteria_indices, gif_filename, all_bacteria_paths, input_folder, all_frame_centers, all_frame_line_points, all_frame_bacteria_pixels, should_draw_rest);  

end