function [im,bw] = acquire_image_config(filename)

  config = load_configuration;

  if config.should_calculate_threshold_with_local_minimum
    image_threshold = get_threshold_level_from_local_minimum(filename);
  else
    image_threshold = config.intensity_threshold;
  end

  [im, bw] = acquire_image(filename, image_threshold);

end
