function matchings = track_bacteria_between_two_frames2(c1, c2)

  %% Let's say that the germs move about 10 pixels max. So we expect (with errors) the center of mass not to move more than 16...
  small_window = 16;
  large_window = 32;  
  c2_bak = c2(:,:);
  matchings = zeros(size(c1, 1), 1);

  dt1 = DelaunayTri(c1(:,1), c1(:,2));
  dt2 = DelaunayTri(c2(:,1), c2(:,2));

  adj1 = shared_vertex_to_adjacency_matrix(c1, dt1.Triangulation);
  adj2 = shared_vertex_to_adjacency_matrix(c2, dt2.Triangulation);

  all_scores = [];
  all_possible_pairings = [];
  
  for bacteria = 1:size(c1, 1)
    fprintf(['\r' num2str(bacteria) '/' num2str(size(c1,1))]); 
    x = c1(bacteria,1);
    y = c1(bacteria,2);

    neighbors = find(adj1(bacteria,:));        
    sorted_neighbors = sort_neighbors_by_angle(bacteria, neighbors, c1);  
    possible_candidates_in_next_frame = all_bacteria_in_window(x, y, small_window, c2);

    %% TODO: possibly try to take into account angles / shapes / areas, not only distance when matching the current bacteria with a candidate in the next frame.
      
    %% If you found any other bacteria to match against.
    if size(possible_candidates_in_next_frame,1) ~=0
      %% Go over each candidate and try to match:
      for candidate_index = 1:size(possible_candidates_in_next_frame,1)
	candidate = possible_candidates_in_next_frame(candidate_index);
	candidate_neighbors = find(adj2(candidate,:));
	sorted_candidate_neighbors = sort_neighbors_by_angle(candidate, candidate_neighbors, c2);

	%% Now cycle through every pair (so that you don't have trouble with the non-crossing algorithm at the edges).

	p1 = c1(sorted_neighbors,:);
	p2 = c2(sorted_candidate_neighbors,:);
	original_distances = pdist2(p1,p2);
	
	best_score = -Inf;
	for i = 1:length(sorted_neighbors)
	  for j = 1:length(sorted_candidate_neighbors)

	    rotated_i = [i:length(sorted_neighbors) 1:i-1];
	    rotated_j = [j:length(sorted_candidate_neighbors) 1:j-1];
	    
	    origin = [sorted_neighbors(i:end) sorted_neighbors(1:i-1)];
	    destination = [sorted_candidate_neighbors(j:end) sorted_candidate_neighbors(1:j-1)];
	    distances = original_distances(rotated_i, rotated_j);

	    %% TODO: the paper you are based on uses the Modified Hausdorff Distance *between the shapes*, NOT the Euclidean distance. See if this is worthwhile (because Modified Hausdorff takes much longer to compute).
	    bipartite_adj = distances < 32;
	    bipartite_weights = distances;	    
	    bipartite_weights = 1 - bipartite_weights ./ 32;
	    bipartite_weights(distances >= 32) = 0;

	    [m, total_weight] = maximal_non_crossing_matching(origin, destination, bipartite_adj, bipartite_weights);
	    best_score = max(best_score, total_weight);
	  end	    
	end    

	best_score = 1 + norm(c1(bacteria,:) - c2(candidate,:))/32;
	%% Ok, we now have the best score for this pair. Add it to a list.
	all_scores(end+1) = best_score;
	all_possible_pairings(end+1,:) = [bacteria candidate];	
      end
	
    end
  end


  %% At this point, you have gone over all bacteria and ranked the best candidates they can go to in the next frame. It's time to select the best scores.
  [sorted_scores, idx] = sort(all_scores, 'descend');
  sorted_pairings = all_possible_pairings(idx,:);
  while ~isempty(sorted_pairings)
    origin = sorted_pairings(1,1);
    destination = sorted_pairings(1,2);

    matchings(origin) = destination;

    %% These two bacteria are taken, remove them as possible options.
    idx = find(sorted_pairings(:,1) == origin);
    sorted_pairings(idx,:) = [];
    idx = find(sorted_pairings(:,2) == destination);
    sorted_pairings(idx,:) = [];    
  end
  
end


function sorted_neighbors = sort_neighbors_by_angle(current_bacteria,  neighbor_indices, cm)
  neighbor_pos = cm(neighbor_indices,:);

  diffs = neighbor_pos - repmat(cm(current_bacteria,:), length(neighbor_indices), 1);
  angles = atan(diffs(:,2) ./ diffs(:,1));
  [dummy, sorted_indices] = sort(angles); 
  sorted_neighbors = neighbor_indices(sorted_indices);
end


function b = all_bacteria_in_window(x, y, window_size, c)
  low_x = x - window_size; high_x = x + window_size;
  low_y = y - window_size; high_y = y + window_size;

  f1 = find(c(:,1) >= low_x);
  f2 = find(c(:,1) <= high_x);
  f3 = find(c(:,2) >= low_y);
  f4 = find(c(:,2) <= high_y);
    
  f = quick_intersect(f1, f2);
  f = quick_intersect(f, f3);
  b = quick_intersect(f, f4);
end