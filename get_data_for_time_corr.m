function [inSpeed, inDir, inVort, ttt] = get_data_for_time_corr(data)

  data.skipFrames = 1;
  speeds=sqrt(data.pu.^2+data.pv.^2);
  nu=data.pu./speeds;
  nv=data.pv./speeds;
  nfr=data.nFrames;
  maxTime=floor(200/data.skipFrames);
  if maxTime>=nfr
    maxTime=floor(nfr/2/data.skipFrames);
  end

  vort=zeros(nfr,data.vfsize(1),data.vfsize(2));
  for f=1:nfr
    vort(f,:,:)=curl( reshape(data.pu(f,:,:),data.vfsize), reshape(data.pv(f,:,:),data.vfsize) )/data.dx;
  end
   
  inSpeed=zeros(maxTime,1);
  inDir=zeros(maxTime,1);
  inVort=zeros(maxTime,1);
  avSpeed=mean(mean(speeds,3),2);
  avU=mean(mean(nu,3),2);
  avV=mean(mean(nv,3),2);
  avC=mean(mean(vort,3),2);

  
  bases=1:10:nfr/data.skipFrames-maxTime;
  for b=bases
    for fr=1:maxTime
      inSpeed(fr)=inSpeed(fr)+mean(mean(speeds(b,:,:).*speeds(b+fr,:,:),3),2)-avSpeed(b)*avSpeed(b+fr);
      inDir(fr)=inDir(fr)+mean(mean(nu(b,:,:).*nu(b+fr,:,:)+nv(b,:,:).*nv(b+fr,:,:),3),2)-avU(b)*avU(b+fr)-avV(b)*avV(b+fr);
      inVort(fr)=inVort(fr)+mean(mean(vort(b,:,:).*vort(b+fr,:,:),3),2)-avC(b)*avC(b+fr);
    end
  end
  
  inSpeed=inSpeed/length(bases);
  inSpeed=inSpeed/inSpeed(1);
  inDir=inDir/inDir(1);
  inVort=inVort/inVort(1);
  ttt=((0:maxTime-1)/data.fps*data.skipFrames)';
end
