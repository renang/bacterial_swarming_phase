%%% It may be that our recognition algorithm failed for just a small number of frames. This causes us to lose our tracks. Can we connect two paths if the center of masses and angles are all very close, and there is just a small number (say, 1 or 2) of frames in between them?
function new_bacteria_paths = correct_matching_jumps(all_bacteria_paths, frame_difference)
  config = load_configuration;
  
  if ~exist('frame_difference', 'var')
    frame_difference = 2;
  end

  all_last_frames = [];
  for i = 1:length(all_bacteria_paths)
    all_last_frames(end+1) = all_bacteria_paths{i}.last_frame;
  end
  upper_frame_limit = max(all_last_frames);
  
  new_bacteria_paths = all_bacteria_paths;

  %% An array of size nx2. The pairs are [first second], so that the second path starts up to frame_difference
  counter = 0;
  did_make_change = true;
  while did_make_change
    fprintf(['\r' num2str(counter) ' ' num2str(length(new_bacteria_paths))]);
    counter = counter + 1;
    
    did_make_change = false;
    pairs_to_fix = [];
    distances = [];

    bacteria_by_starting_frame = cell(1, upper_frame_limit);;
    bacteria_by_ending_frame = cell(1, upper_frame_limit);;
    for i = 1:length(new_bacteria_paths)
      bacteria_by_starting_frame{new_bacteria_paths{i}.first_frame}(end+1) = i;
      bacteria_by_ending_frame{new_bacteria_paths{i}.last_frame}(end+1) = i;
    end

    for k = 1:(length(bacteria_by_ending_frame) - frame_difference)
      for i = 1:length(bacteria_by_ending_frame{k})
	for j = 1:length(bacteria_by_starting_frame{k+frame_difference})
	  b1_index = bacteria_by_ending_frame{k}(i);
	  b2_index = bacteria_by_starting_frame{k+frame_difference}(j);
    	  b1 = new_bacteria_paths{b1_index};
    	  b2 = new_bacteria_paths{b2_index};

	  d = norm(b1.cm_points(end,:) - b2.cm_points(1,:));
    	  if d < 10*frame_difference
    	    pairs_to_fix(end+1,:) = [b1_index b2_index];
	    distances(end+1,:) = d;
    	  end

	end

      end
	
    end

    if isempty(pairs_to_fix)
      did_make_change = false;
    else
    
      [sorted_distances, idx] = sort(distances);
      sorted_pairs_to_fix = pairs_to_fix(idx,:);
      already_seen_this_round = [];
    
      new_paths = {};
      while ~isempty(sorted_pairs_to_fix)
	b1 = new_bacteria_paths{sorted_pairs_to_fix(1,1)};
	b2 = new_bacteria_paths{sorted_pairs_to_fix(1,2)};

	new_x = linspace(b1.cm_points(end,1), b2.cm_points(1,1), frame_difference+1);
	new_y = linspace(b1.cm_points(end,2), b2.cm_points(1,2), frame_difference+1);
	new_angles = linspace(b1.angles(end), b2.angles(1), frame_difference+1);
	new_x = new_x(2:end-1);
	new_y = new_y(2:end-1);
	new_angle = new_angles(2:end-1);
	new_center_of_mass = [new_x' new_y'];			    
	
	%% new_center_of_mass = 0.5 * (b1.cm_points(end,:) + b2.cm_points(1,:));
	%% new_angle = 0.5 * (b1.angles(end) + b2.angles(1));
	new_start_frame = b1.first_frame;
	new_end_frame = b2.last_frame;

	new_path = struct ('cm_points', [b1.cm_points; new_center_of_mass; b2.cm_points], 'angles', [b1.angles new_angle b2.angles], 'first_frame', new_start_frame, 'last_frame', new_end_frame);
	new_path.velocities = [diff(new_path.cm_points(:,1)) diff(new_path.cm_points(:,2))];
	%% The "smooth_span" parameter comes from loading the configuration file.
	if config.smooth_span ~= 0
	  if config.smooth_span < size(new_path.velocities, 1)
	    new_path.velocities(:,1)= malowess(1:size(new_path.velocities, 1), new_path.velocities(:,1), 'Span', config.smooth_span);
	    new_path.velocities(:,2)= malowess(1:size(new_path.velocities, 1), new_path.velocities(:,2), 'Span', config.smooth_span);
	  else
	    new_path.velocities(:,1) = smooth(new_path.velocities(:,1), config.smooth_span);
	    new_path.velocities(:,2) = smooth(new_path.velocities(:,2), config.smooth_span);
	  end	  
	end

	new_paths{end+1} = new_path;

	i = sorted_pairs_to_fix(1,1);
	j = sorted_pairs_to_fix(1,2);
	already_seen_this_round(end+1) = i;
	already_seen_this_round(end+1) = j;

	idx = find(sorted_pairs_to_fix(:,1) == i);
	sorted_pairs_to_fix(idx,:) = [];
	idx = find(sorted_pairs_to_fix(:,1) == j);
	sorted_pairs_to_fix(idx,:) = [];
	idx = find(sorted_pairs_to_fix(:,2) == i);
	sorted_pairs_to_fix(idx,:) = [];
	idx = find(sorted_pairs_to_fix(:,2) == j);
	sorted_pairs_to_fix(idx,:) = [];
      end

      new_bacteria_paths(already_seen_this_round) = [];
      new_bacteria_paths = [new_bacteria_paths new_paths];
    end
  end
end
