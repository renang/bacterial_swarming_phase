%%% Bounding rectangle is an array of size 2*4 of points - each one is a point of the rectangle. But they may be unordered. This function returns them points in an ordered way (not know if clockwise or counter-clockwise, though).
function [p1 p2 p3 p4] = sorted_points_of_bounding_rectangle(bounding_rectangle)
  %% Copy-paste from the comment from draw_tilted_line_around_rectangle:
  %% Now we have all the points of a rectangle, but we don't know their correct order. Here is one way to find it:
  %% Sort the points by x value, from left to right. The leftmost point must be connected to the second leftmost. The rightmost point must be connected to the second rightmost. However, the seconds cannot be connected to each other, otherwise we have a chain, which is impossible.
  all_xs = bounding_rectangle(1,:);
  all_ys = bounding_rectangle(2,:);

  rx1 = all_xs(1); rx2 = all_xs(2);   rx3 = all_xs(3);   rx4 = all_xs(4);
  ry1 = all_ys(1); ry2 = all_ys(2);   ry3 = all_ys(3);   ry4 = all_ys(4);
  
  [sorted_xs, ind] = sort(all_xs);
  sorted_ys = all_ys(ind);
  rx1 = sorted_xs(1); rx2 = sorted_xs(2); rx3 = sorted_xs(4); rx4 = sorted_xs(3);
  ry1 = sorted_ys(1); ry2 = sorted_ys(2); ry3 = sorted_ys(4); ry4 = sorted_ys(3);

  p1 = [0.5*(rx1+rx2) 0.5*(ry1+ry2)];
  p2 = [0.5*(rx2+rx3) 0.5*(ry2+ry3)];
  p3 = [0.5*(rx3+rx4) 0.5*(ry3+ry4)];
  p4 = [0.5*(rx4+rx1) 0.5*(ry4+ry1)];

end