function new_im = draw_tilted_rectangle_around_line(x1, y1, x2, y2, width, im, polygon_shape_inserter)
  theta = atan2(y2-y1, x2-x1) + pi/2;
  rx1 = x1 + width * cos(theta);
  ry1 = y1 + width * sin(theta);
  rx2 = x1 - width * cos(theta);
  ry2 = y1 - width * sin(theta);

  rx3 = x2 + width * cos(theta);
  ry3 = y2 + width * sin(theta);
  rx4 = x2 - width * cos(theta);
  ry4 = y2 - width * sin(theta);

  %% TODO: use the function sorted_points_of_bounding_rectangle. It won't make anything faster or better, just a bit cleaner code.
  %% Now we have all the points of a rectangle, but we don't know their correct order. Here is one way to find it:
  %% Sort the points by x value, from left to right. The leftmost point must be connected to the second leftmost. The rightmost point must be connected to the second rightmost. However, the seconds cannot be connected to each other, otherwise we have a chain, which is impossible.
  all_xs = [rx1 rx2 rx3 rx4];
  all_ys = [ry1 ry2 ry3 ry4];
  [sorted_xs, ind] = sort(all_xs);
  sorted_ys = all_ys(ind);

  rx1 = sorted_xs(1); rx2 = sorted_xs(2); rx3 = sorted_xs(4); rx4 = sorted_xs(3);
  ry1 = sorted_ys(1); ry2 = sorted_ys(2); ry3 = sorted_ys(4); ry4 = sorted_ys(3);
  new_im = step(polygon_shape_inserter, im, int32([rx1 ry1 rx2 ry2 rx3 ry3 rx4 ry4]));
end
  