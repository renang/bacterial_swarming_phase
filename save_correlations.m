function save_correlations(correlations)
  config = load_configuration;
  
  for i = correlations
    %% Get the actual struct.
    c = i{1}; 
    if ~isempty(c)
      edges = c.edges;
      s = ['density_' num2str(c.density, '%0.4f') '_'];

      angle_filename = fullfile(config.output_folder, [s config.angle_correlation_filename]);
      fid = fopen(angle_filename, 'w');
      fprintf(fid, 'dist\tcorrelation\n');
      fprintf(fid, '%f\t%f\n', [edges; c.aac]);
      fclose(fid);

      velocity_filename = fullfile(config.output_folder, [s config.velocity_correlation_filename]);
      fid = fopen(velocity_filename, 'w');
      fprintf(fid, 'dist\tcorrelation\n');
      fprintf(fid, '%f\t%f\n', [edges; c.avc]);
      fclose(fid);

      angle_by_neighbor_filename = fullfile(config.output_folder, [s config.angle_correlation_by_neighbor_filename]);
      fid = fopen(angle_by_neighbor_filename, 'w');
      fprintf(fid, 'neighbor_num\tcorrelation\n');
      fprintf(fid, '%f\t%f\n', [1:length(c.aacn); c.aacn]);
      fclose(fid);

      velocity_by_neighbor_filename = fullfile(config.output_folder, [s config.velocity_correlation_by_neighbor_filename]);
      fid = fopen(velocity_by_neighbor_filename, 'w');
      fprintf(fid, 'neighbor_num\tcorrelation\n');
      fprintf(fid, '%f\t%f\n', [1:length(c.avcn); c.avcn]);
      fclose(fid);

      velocity_optical_flow_filename = fullfile(config.output_folder, [s config.velocity_optical_flow_correlation_filename]);
      fid = fopen(velocity_optical_flow_filename, 'w');
      fprintf(fid, 'dist\tcorrelation\n');
      fprintf(fid, '%f\t%f\n', [edges; c.inDir']);
      fclose(fid);

      speed_optical_flow_filename = fullfile(config.output_folder, [s config.speed_optical_flow_correlation_filename]);
      fid = fopen(speed_optical_flow_filename, 'w');
      fprintf(fid, 'dist\tcorrelation\n');
      fprintf(fid, '%f\t%f\n', [edges; c.inSpeed']);
      fclose(fid);

      vorticity_optical_flow_filename = fullfile(config.output_folder, [s config.vorticity_optical_flow_correlation_filename]);
      fid = fopen(vorticity_optical_flow_filename, 'w');
      fprintf(fid, 'dist\tcorrelation\n');
      fprintf(fid, '%f\t%f\n', [edges; c.inVort']);
      fclose(fid);

      time_velocity_optical_flow_filename = fullfile(config.output_folder, [s config.time_velocity_optical_flow_correlation_filename]);
      fid = fopen(time_velocity_optical_flow_filename, 'w');
      fprintf(fid, 'dist\tcorrelation\n');
      fprintf(fid, '%f\t%f\n', [1:length(c.time_inDir); c.time_inDir']);
      fclose(fid);

      time_speed_optical_flow_filename = fullfile(config.output_folder, [s config.time_speed_optical_flow_correlation_filename]);
      fid = fopen(time_speed_optical_flow_filename, 'w');
      fprintf(fid, 'dist\tcorrelation\n');
      fprintf(fid, '%f\t%f\n', [1:length(c.time_inSpeed); c.time_inSpeed']);
      fclose(fid);

      time_vorticity_optical_flow_filename = fullfile(config.output_folder, [s config.time_vorticity_optical_flow_correlation_filename]);
      fid = fopen(time_vorticity_optical_flow_filename, 'w');
      fprintf(fid, 'dist\tcorrelation\n');
      fprintf(fid, '%f\t%f\n', [1:length(c.time_inVort); c.time_inVort']);
      fclose(fid);
      
    end
  end
end
