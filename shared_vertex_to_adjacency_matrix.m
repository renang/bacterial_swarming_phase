% Returns an adjacency matrix of all the vertices, given a shared vertex representation of a mesh.
function a = shared_vertex_to_adjacency_matrix(vertices, faces)
  vnum = size(vertices, 1);
  fnum = size(faces, 1);

  a = sparse(vnum, vnum);

  i1 = sub2ind(size(a), faces(:,1), faces(:,2));
  i2 = sub2ind(size(a), faces(:,2), faces(:,3));
  i3 = sub2ind(size(a), faces(:,3), faces(:,1));
  
  a(i1) = 1;
  a(i2) = 1;
  a(i3) = 1;
	       
  % Make symmetric:
  % Note! Here a vertex is not connected to itself.
  a = max(a, a');

end
