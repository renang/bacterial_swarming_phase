function draw_histogram_gif(directory)
  gif_filename = 'asd.gif';
  
  edges = 0.5 + -1:256;
  filenames = get_all_image_filenames_from_folder(directory);
  num_of_filenames = length(filenames);
  figure(1);

  for i = 1:num_of_filenames
    disp(['Currently on frame number ' num2str(i) ' out of ' num2str(num_of_filenames) ]);
    filename = filenames{i};
    im = imread(filename);
    intensities = double(im(:));
    spectrum = histc(intensities,edges);
    smoothed_spectrum = smooth(spectrum, 'sgolay');    
    [m, ind] = max(smoothed_spectrum);
    smoothed_spectrum = smoothed_spectrum / m;

    plot(smoothed_spectrum);
    ylim([0 1]);
    drawnow;
    gif_frame = getframe(1);
    imframe = frame2im(gif_frame);  
    [imind,cm] = rgb2ind(imframe,256);
    if i == 1
      imwrite(imind,cm, gif_filename','gif', 'DelayTime', 0.1, 'Loopcount',inf);
    else
      imwrite(imind,cm, gif_filename,'WriteMode','append', 'DelayTime', 0.1);
    end
  end
end
