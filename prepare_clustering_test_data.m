function [distance_clusters_sizes, ...
	  combined_clusters_sizes, ...
	  distance_largest_cluster, ...
	  combined_largest_cluster] = prepare_clustering_test_data(io_struct, frames_data)
  
  config = load_configuration();
  speeds = sqrt(sum(frames_data.all_relevant_velocities .* frames_data.all_relevant_velocities, 2));

  %% When the adj matrix is built only with distance proximity, use this threshold:
  distance_threshold = 1*io_struct.bacteria_size * config.pixels_per_micrometer;

  %% When the adj matrix is built out of both distance and velocity proximity, use these thresholds:
  combined_velocity_threshold = mean(speeds) / 5 ;
  combined_distance_threshold = 1*io_struct.bacteria_size * config.pixels_per_micrometer;

  %% TODO: ^^^ The analysis is quite sensitive to the thresholds. Perhaps you want to
  %% reconsider how you treat it.
  
  distance_clusters_sizes = [];
  combined_clusters_sizes = [];

  distance_largest_cluster = zeros(1, frames_data.number_of_frames);
  combined_largest_cluster = zeros(1, frames_data.number_of_frames);
  
  for i = 1:frames_data.number_of_frames
    fprintf(['\r' num2str(i) '/' num2str(frames_data.number_of_frames)]);
    this_frame_centers = frames_data.frame_centers_of_mass{i};
    this_frame_velocities = frames_data.frame_velocities{i};

    pairwise_distances = squareform(pdist(this_frame_centers));
    distance_adjacency_matrix = (pairwise_distances < distance_threshold) - eye(size(this_frame_centers, 1));
    G = graph(distance_adjacency_matrix);
    distance_components_of_vertices = conncomp(G);
    distance_component_sizes = hist(distance_components_of_vertices, max(distance_components_of_vertices));

    pairwise_relative_velocities = squareform(pdist(this_frame_velocities));
    pairwise_relative_velocities(isnan(pairwise_relative_velocities)) = 1000000; %% Some absurdly high number, so it will be zerofied in the threshold.
    combined_velocity_adj = (pairwise_relative_velocities < combined_velocity_threshold) - eye(size(this_frame_centers, 1));
    combined_distance_adj = (pairwise_distances < combined_distance_threshold) - eye(size(this_frame_centers, 1));;
    
    G = graph(combined_velocity_adj .* combined_distance_adj);
    combined_components_of_vertices = conncomp(G);
    combined_component_sizes = hist(combined_components_of_vertices, max(combined_components_of_vertices));	  
    
    distance_clusters_sizes = cat(2, distance_clusters_sizes, distance_component_sizes);
    combined_clusters_sizes = cat(2, combined_clusters_sizes, combined_component_sizes);
    distance_largest_cluster(i) = max(distance_component_sizes);
    combined_largest_cluster(i) = max(combined_component_sizes);
  end
  display(' ');
end
