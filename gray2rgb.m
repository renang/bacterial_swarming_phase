function rgb = gray2rgb(gray_im)
  rgb = zeros(size(gray_im,1), size(gray_im,2), 3);
  rgb(:,:,1) = gray_im;
  rgb(:,:,2) = gray_im;
  rgb(:,:,3) = gray_im;
  rgb = double(rgb) ./ double(max(max(max(rgb))));
end