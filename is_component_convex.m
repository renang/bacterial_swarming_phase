function a = is_component_convex(im, component_indices, region)
  if length(component_indices) < 4
    %% Arbitrary.
    a = true;
    return;
  end
  
  [x_convex, y_convex] = get_component_convex_hull(im, component_indices);
  hull_area = polyarea(x_convex, y_convex);
  component_area = region.Area;

  % Emperically (checked about 75 bacteria components (not junk), most
  % convex components are over 0.90. (all but 1 was so).
  % So let's choose this number as an indicator.
  % I think we would rather have a false negative (meaning, we think that it
  % isn't convex, while it actually is) than a false positive, since maybe
  % the (later) separation algorithms will realize that it's single.
  % But I'm not sure. Should check.
  if hull_area > 0
    a = (component_area / hull_area) > 0.80;
  else
    a = true;
  end
end
