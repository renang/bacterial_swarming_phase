filename = '../20-Mar-2017 19_11_46_densities.txt';

fid = fopen(filename,'r');
fgetl(fid);
data = textscan(fid, '%d%f', 'Delimiter', '\t');

density = data{2};

%% Shift the density so that it has 0 mean; this will remove large 0-frequency Fourier coefficient.
zeroed_density = density - mean(density);

z = fftshift(fft(zeroed_density));
mag = abs(z);
frequencies = ((1:length(density)) - (length(density)/2 + 1)) / (length(density)/2 );

figure;
plot(frequencies, mag);
title('Magnitude of fourier coeffs');

%% Now you can do analysis on the fft. For example, see what are the 20 strongest frequencies.
k = 20;

[sorted, ind] = sort(mag, 'descend');

strongest_frequencies = (ind(1:k)- (length(density)/2 + 1)) / (length(density)/2 );

disp 'Strongest frequencies are:'
disp(strongest_frequencies);

disp 'The magnitude of the fourier coeffs of these frequencies are'
disp(sorted(1:k));


%% For example, for fun, you can smooth the signal by taking only the strongest frequencies.
strongest = zeros(size(z));
strongest(ind(1:20)) = z(ind(1:20));
strongest = fftshift(bla);

smoothed = ifft(strongest);

figure;
hold on
plot(abs(smoothed),'b', 'linewidth', 2);
plot(abs(zeroed_density),'r');
title('Absolute value of smoothed and actual density');
hold off;
