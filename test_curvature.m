function test_curvature(io_struct, paths_of_this_density)
  tic;
  test_name = 'curvature';
  display(['Now testing ' test_name]);
  create_directory_or_throw(io_struct.base_output_directory, test_name);
  create_directory_or_throw(fullfile(io_struct.base_output_directory, test_name), num2str(io_struct.bacteria_size));
  test_directory = fullfile(io_struct.base_output_directory, test_name, num2str(io_struct.bacteria_size));
  
  total_curvatures = 0;
  total_counts = 0;
  for i = 1:length(paths_of_this_density)
    path = paths_of_this_density{i};  
    curvatures = get_curvature_from_velocities(path.velocities);
    curvatures = min(curvatures, 100); % Remove anomalies. You decide what counts as an anomaly.
    total_curvatures = total_curvatures + sum(curvatures);
    total_counts = total_counts + length(curvatures);  
  end
  curvature = total_curvatures / total_counts;
  output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension '.txt']);
  fileID = fopen(output_filename,'w');
  fprintf(fileID, '%f\n', curvature);
  fclose(fileID);
  toc;
end
