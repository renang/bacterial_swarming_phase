base_input_directory  = 'YOUR INPUT DIRECTORY HERE';
base_output_directory = 'YOUR OUTPUT DIRECOTRY HERE';

start_time_of_all_files = tic();

warning('off', 'MATLAB:MKDIR:DirectoryExists');
warning('off', 'curvefit:fit:noStartPoint');

config = load_configuration();

%bacteria_sizes = [5 7 13 19];
bacteria_sizes = [5 7];

should_test_diffusivity = false;
should_test_autocorrelation = false;
should_test_angle_correlation = false;
should_test_direction_correlation = false;
should_test_curvature = false;
should_test_speed_distribution = false;
should_test_structure_factor = false;
should_test_bins = false;
should_test_clustering = false;
should_test_large_fluctuations_with_bins = true;

for bacteria_size = bacteria_sizes
  dir_query = fullfile(base_input_directory, num2str(bacteria_size), '*.mat');
  directory_listing = dir(dir_query);
  density_counts = zeros(1,length(config.density_bins)-1);    

  all_mean_speeds = {};
  
  all_clustering_fits = {};
  all_structure_factor_fits = {};
  all_diffusivity_fits = {};
  all_autocorrelation_fits = {};
  all_direction_correlation_fits = {};
  all_angle_correlation_fits = {};
  all_fluctuation_fits = {};
  
  binning_summary = cell(1, length(config.density_bins)-1);
  for i = 1:length(config.density_bins)-1
    binning_summary{i}.unoriented_angle_polar_order = [];
    binning_summary{i}.unoriented_angle_nematic_order = [];
    binning_summary{i}.oriented_angle_polar_order = [];
    binning_summary{i}.oriented_angle_nematic_order = [];
    binning_summary{i}.bacteria_nums = [];
  end

  diffusivity_summary = cell(1, length(config.density_bins)-1);
  for i = 1:length(config.density_bins)-1
    diffusivity_summary{i} = [];
  end
 
  for file_index = 1:length(directory_listing)
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Preliminary data loading and preparation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    file_found = directory_listing(file_index);
    full_filename = fullfile(file_found.folder, file_found.name);
    display(['Processing file ' full_filename]);
    file_start_time = tic();

    file_data = load(full_filename);
    display('File loaded');
    [dummy, filename_without_extension] = fileparts(file_found.name);

    io_struct.filename_without_extension = filename_without_extension;
    io_struct.bacteria_size = bacteria_size;
    io_struct.base_output_directory = base_output_directory;
             
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Tests which depend on paths with density
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    %% We only want to work with not-too-short-paths.
    path_lengths = extractfield([file_data.all_bacteria_paths{:}], 'last_frame') - extractfield([file_data.all_bacteria_paths{:}], 'first_frame');
    long_paths_indices = find(path_lengths >= config.ignore_small_paths);
    long_paths = file_data.all_bacteria_paths(long_paths_indices);
    long_paths_lengths = path_lengths(long_paths_indices);

    %% We also only want to work with paths which traveled at least a certain distance.
    distances_traveled = cellfun(@distance_path_traveled, long_paths);
    large_distance_indices = find(distances_traveled >= config.diffusivity_path_minimal_distance); 
    long_paths = long_paths(large_distance_indices);
    long_paths_lengths = long_paths_lengths(large_distance_indices);
    
    %% First, attach to each path an average density.
    path_densities = zeros(length(long_paths),1);
    for i = 1:length(long_paths)
      path = long_paths{i};
      path_densities(i) = mean(file_data.all_frame_densities(path.first_frame:path.last_frame));
    end

    %% Now go over each individual density and perform the tests.
    [path_density_histogram, path_indices] = histc(path_densities, config.density_bins);
    
    for density_index = 1:(length(path_density_histogram)-1)
      if path_density_histogram(density_index) > 0
	density = config.density_bins(density_index);
	io_struct.density = density;
	display(['Processing paths of density ' num2str(density)]);
    	paths_indices_of_this_density = find(path_indices == density_index);
	paths_of_this_density = long_paths(paths_indices_of_this_density);
	path_lengths_of_this_density = long_paths_lengths(paths_indices_of_this_density);

	if should_test_diffusivity
	  power_law_fit = test_property_on_paths(io_struct, paths_of_this_density, @get_mean_square_displacement_up_to_smax, config.diffusivity_test_name, false, true);
	  power_law_fit.density = density;
	  all_diffusivity_fits{end+1} = power_law_fit;
	  
	  diffusivity_summary{density_index} = [diffusivity_summary{density_index} get_mean_square_displacement_from_paths_for_single_s(paths_of_this_density, path_lengths_of_this_density, config.diffusivity_summary_path_length)];
	end

	if should_test_autocorrelation
	  power_law_fit = test_property_on_paths_for_correlation(io_struct, paths_of_this_density, @get_autocorrelation_up_to_smax, config.autocorrelation_test_name, true, false);
	  power_law_fit.density = density;
	  all_autocorrelation_fits{end+1} = power_law_fit;
	end

	if should_test_direction_correlation
	  power_law_fit = test_property_on_paths_for_correlation(io_struct, paths_of_this_density, @get_direction_correlation_up_to_smax, config.direction_correlation_test_name, true, false);
	  power_law_fit.density = density;
	  all_direction_correlation_fits{end+1} = power_law_fit;
	end

	if should_test_angle_correlation
	  power_law_fit = test_property_on_paths_for_correlation(io_struct, paths_of_this_density, @get_angle_correlation_up_to_smax, config.angle_correlation_test_name, true, false);
	  power_law_fit.density = density;
	  all_angle_correlation_fits{end+1} = power_law_fit;
	end
	
	if should_test_curvature
	  test_curvature(io_struct, paths_of_this_density);	
	end
	
      end
    end    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Tests which depend on frames
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    [density_histogram, frame_indices] = histc(file_data.all_frame_densities(1:end-1), config.density_bins);

    for density_index = 1:(length(density_histogram)-1)
      if density_histogram(density_index) > 0

	density_counts(density_index) = density_counts(density_index) + 1;
	
	density = config.density_bins(density_index);
	io_struct.density = density;
	display(['Processing frames of density ' num2str(density)]);	
	frames = find(frame_indices == density_index);
	
	frames_data = prepare_frames_test_data(frames, frame_indices, density_index, file_data);	

	if should_test_speed_distribution
	  test_speed_distribution(io_struct, frames_data);	  	  
	end


	speed_count.density = density;
	speed_count.mean_speed = mean(sqrt(sum(frames_data.all_relevant_velocities .* frames_data.all_relevant_velocities, 2)));
	speed_count.bacteria_num = size(frames_data.all_relevant_velocities,1);	
	all_mean_speeds{end+1} = speed_count;
	
	if should_test_structure_factor
	  power_law_fit = test_structure_factor(io_struct, frames_data);
	  power_law_fit.density = density;
	  all_structure_factor_fits{end+1} = power_law_fit;
	end

	if should_test_bins
	  display('Gathering bin data');
	  tic;
	  [all_bin_bacteria_nums, ...
	   all_bin_unoriented_angle_polar_order_parameter, ...
	   all_bin_unoriented_angle_nematic_order_parameter, ...
	   all_bin_oriented_angle_polar_order_parameter, ...
	   all_bin_oriented_angle_nematic_order_parameter] = prepare_binning_test_data(frames_data);
	  toc;

	  binning_summary{density_index}.unoriented_angle_polar_order = [binning_summary{density_index}.unoriented_angle_polar_order remove_nans(all_bin_unoriented_angle_polar_order_parameter)];
	  binning_summary{density_index}.unoriented_angle_nematic_order = [binning_summary{density_index}.unoriented_angle_nematic_order remove_nans(all_bin_unoriented_angle_nematic_order_parameter)];
	  binning_summary{density_index}.oriented_angle_polar_order = [binning_summary{density_index}.oriented_angle_polar_order remove_nans(all_bin_oriented_angle_polar_order_parameter)];
	  binning_summary{density_index}.oriented_angle_nematic_order = [binning_summary{density_index}.oriented_angle_nematic_order remove_nans(all_bin_oriented_angle_nematic_order_parameter)];
	  binning_summary{density_index}.bacteria_nums = [binning_summary{density_index}.bacteria_nums all_bin_bacteria_nums(all_bin_bacteria_nums > 0)];
	  
	  test_bin_statistics(io_struct, all_bin_bacteria_nums, all_bin_unoriented_angle_polar_order_parameter, all_bin_unoriented_angle_nematic_order_parameter, all_bin_oriented_angle_polar_order_parameter, all_bin_oriented_angle_nematic_order_parameter);
	end


	if should_test_large_fluctuations_with_bins
	  display('Gathering fluctuation data');
	  bacteria_num_variances = prepare_large_fluctuations_test_data(frames_data);	  
	  fit = test_large_fluctuations_statistics(io_struct, bacteria_num_variances);
	  f.fit = fit; 
	  f.density = density; 
	  all_fluctuation_fits{end+1} = f;
	end
	

	if should_test_clustering
	  average_num_of_bacteria_in_frames = size(frames_data.all_relevant_velocities, 1) / frames_data.number_of_frames;
	  display('Gathering clustering data')
	  tic;
	  [distance_clusters_sizes, ...
	  combined_clusters_sizes, ...
	  distance_largest_cluster, ...
	  combined_largest_cluster] = prepare_clustering_test_data(io_struct, frames_data);
	  toc;
	  
	  fits = test_clustering(io_struct, frames_data, average_num_of_bacteria_in_frames, ...
			distance_clusters_sizes, ...
			combined_clusters_sizes, ...
			distance_largest_cluster, ...
			combined_largest_cluster);

	  for i = 1:length(fits)
	    f = fits{i};
	    f.density = density;
	    all_clustering_fits{end+1} = f;
	  end
	
	end
      end
    end
    display(['Finished processing file ' full_filename])
    toc(file_start_time);
  end

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %% Summary plots, after all files of a certain aspect ratio were processed
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if should_test_large_fluctuations_with_bins
    plot_large_fluctuations_summary_graphs(all_fluctuation_fits, base_output_directory, bacteria_size);
  end
  
  if should_test_speed_distribution
    plot_mean_speed_summary_graphs(all_mean_speeds, base_output_directory, bacteria_size);
  end
  
  if should_test_clustering
    plot_cluster_summary_graphs(all_clustering_fits, base_output_directory, bacteria_size);
  end

  if should_test_structure_factor
    plot_structure_factor_summary_graphs(all_structure_factor_fits, base_output_directory, bacteria_size);
  end

  if should_test_bins
    plot_bins_summary_graphs(binning_summary, base_output_directory, bacteria_size);
  end

  if should_test_diffusivity
    plot_diffusivity_summary_graphs(diffusivity_summary, all_diffusivity_fits, base_output_directory, bacteria_size);
  end

  if should_test_autocorrelation
    plot_correlation_summary_graphs(all_autocorrelation_fits, base_output_directory, config.autocorrelation_test_name, bacteria_size);
  end

  if should_test_direction_correlation
    plot_correlation_summary_graphs(all_direction_correlation_fits, base_output_directory, config.direction_correlation_test_name, bacteria_size);
  end
  
  if should_test_angle_correlation
    plot_correlation_summary_graphs(all_angle_correlation_fits, base_output_directory, config.angle_correlation_test_name, bacteria_size);
  end
  
end

display('All files done!');
toc(start_time_of_all_files);
