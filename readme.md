# Introduction
This repository contains the majority of the code used in the following paper:
 "A phase diagram for bacterial swarming",
published in Communication physics, available at https://www.nature.com/articles/s42005-020-0327-1 .

Its main goal is to track individual bacteria across multiple frames and analyze properties of motion of the collective motion of all bacteria in a colony. Please refer to the above paper and the following blogpost for more information on the research.
https://sarcasticresonance.wordpress.com/2020/04/06/phase_diagram_for_bacterial_swarming .


The majority of this code, apart from optical flow and selected files, was written by Renan Gross, who may be reached at renan.gross $you-know-the-sign$ gmail.com . 

# How to cite
If you use this code or its ideas in your research, please cite us as
Be’er, A., Ilkanaiv, B., Gross, R. et al. A phase diagram for bacterial swarming. Commun Phys 3, 66 (2020). https://doi.org/10.1038/s42005-020-0327-1 . See also https://www.nature.com/articles/s42005-020-0327-1#citeas .

# How to use this program
The code should work (at least) with matlab 2017a. 

The code for this program is spread across several files, containing both scripts and functions. However, if you wish to simply track, record, and save the bacteria data, most of these files are of no interest to you.

In the simplest case, you will only be messing around with several files:

- do_everything
- load_configutation
- threshold_selector
- draw_frame_gif
- draw_path_gif

###### do_everything and load_configuration

The file do_everything.m, as its name suggests, does everything for you. It automatically loads pictures from a given directory, finds the bacteria in each frame, tracks them across several frames, and saves the data to text file. If you want to understand the code, this is the place to start, top-down.
How does it know from where to take the pictures, how many pictures to process, and other runtime parameters? These are all given in the file load_configuration.m. In it are the parameters which might regularly change from one experimental setup to another.
Thus, your interaction with the program might go like this: after the physical, wet experiment has been performed and the thousands of images are generated, they are all put into a single folder. You write this folder in the configuration file. You choose how much you want to smooth the velocity, the thresholding of the images (more on this follows), and run do_everything:

Example:
> do_everything

After waiting for the program to finish, all the variables are loaded, the text files are saved, and you are ready to start performing your analysis.

What data will you have after running this script?

In the matlab workspace:

- The variable all_bacteria_paths, which provides a reference by id to all the paths taken by bacteria - centers of mass, angles, velocities.
- The variable path_lengths - which tells you what are the path lengths of all the paths in all_bacteira_paths. It's just a shortcut.

The results will generally be saved according to densities of individual frames in your sequence. For each density, we will have:

- A file containing all the angles in all the frames analyzed.
- A file containing all the velociites in all the frames analyzed.
- Files containing the angle correlation as a function of distance (averaged over all frames analyzed)
- Files containing the velocity correlation as a function of distance (averaged over all frames analyzed)

Also, we'll have one separate file with the centers of mass for each bacteria path found in all the frames analyzed (there may be up to several thousands of files).

The filenames (and folder) of these files are given in the configuration file, load_configuration.m. You control them using the config.outpuf_folder param, as well as the filenames which follow it.


###### threshold_selector
The algorithm transforms the grayscale input images into black and white images. For this it must use a threshold - anything less bright than some threshold will be converted to black, and anything brighter will be converted to white. Unfortunately, I do not currently have a good way to automatically choose a threshold value given a certain image. A value that is too low will result in background noise in the black and white image. A value that is too high will result in large clumps of bacteria disappearing, having distorted shapes, or cutting in half in the black and white image.
Thus, you are going to have to find out an "optimal" threshold value yourself. I say "optimal", because we judge according to our naked eye, and not with some well defined metric.
The file threshold_selector helps you do just that. You can load files, and compare and inspect two different types of thresholds: whitepixel density and entropy.

###### draw_frame_gif and draw_path_gif
You may sometimes want to create movies which show the tracked bacteria. These two files help you do it, and create animated gifs.

The function draw_frame_gif tracks all the bacteria in a given frame.

Example:
> draw_frame_gif(1, 'bacteria_image.gif', all_bacteria_paths, config.image_folder, all_frame_centers, all_frame_line_points, all_frame_bacteria_pixels, true)

The function draw_path_gif tracks all the bacteria according the indices you give it. It is more general than draw_frame_gif.

Example:
> draw_path_gif([1 2 10 20 156], 'bacteria_image.gif', all_bacteria_paths, config.image_folder, all_frame_centers, all_frame_line_points, all_frame_bacteria_pixels, true)

When I say "track", it means that the specified bacteria will be colored in a different color than the rest, and their trail (the history of their entire movement) will also be displayed. 

You can read more about using these in the function files themselves. However, generally, you will only be interested about changing the first parameter (which tells which bacteria to draw), the second parameter (the filename of the output gif) and the last parameter (which says if you should draw all the other bacteria, those you didn't specify in the first parameter). The rest of the parameters are just the results from running the code in do_everything.

# A short technical explanation 
The program is given as input a directory containing all the images of the bacteria - one grayscale image for each frame. 
The first part of the program is to process every frame individually. The goal is to turn the grayscale image into a black and white image, where each bacteria is separated from its neighbors. This is done the following way:
First, we increase the contrast of the grayscale image, then turn it to black and white using a predefined threshold. This already separates some bacteria, but not all, and some remain in connected components.
Second, each component is then locally contrast-enhanced, and again thresholded. This separates components that were not separated before.
Third, we check which components look like single bacteria, and which one look like multiple connected-together bacteria. For the latter, we use an outer-medial pruning method, as described in "Detecting and Tracking Motion of Myxococcus xanthus Bacteria in Swarms". In this method, touching bacteria are cut so that they are no longer touching.
Finally, the program goes over all the components in the black and white image. If a component is of a single bacteria, it finds its center of mass and orientation. If it still contains multiple bacteria after the previous steps, it uses a clustering algorithm to try and see in what ways multiple bacteria can fit in the connected component.

The final result of this part, is that for each frame we have a list of centers of masses and angles, of all the bacteria we found in the frame.

The second part of the program is the tracking of bacteria across multiple frames. This is done by looking at every pair of two subsequent frames, and trying to match each bacteria found in the first frame to a bacteria found in the second frame. This is done by looking at the bacteria's surroundings in both frames, and trying to create a one-to-one matching between it and its neighbors in such a way that will minimize the total distance that all bacteria have to go through.
After this matching process, bacteria are tracked along the frames, and the following data about their path is saved:

- the center of mass points at every frame
- the angle at every frame
- the velocity between every two frames
- the starting and ending frames.

A possible problem with the above approach is that if a bacteria is momentarily "lost" by the algorithm, meaning that for some reason it was not identified in a single frame, then the program will think that it is two separate bacteria. The algorithm tries to correct this by connecting together disconnected paths that are very close to each other in both space and time.

The final result is a large array (usually named all_bacteria_paths in my scripts). This array contains data (as noted above - cm points, velocity, etc) for ALL the bacteria across ALL the frames. This data can then be plotted, analysed, manipulated, etc.