cc = bwconncomp(bw, 4);
regions = regionprops(cc,'basic');

ccim = zeros(size(im));

for c = 1:cc.NumObjects
  
  component_image = get_component_image_from_number(c,cc,regions,im);
  tempzero = zeros(size(im));
  tempzero(cc.PixelIdxList{c}) = 1;

  s = bwmorph(tempzero, 'skel', Inf);
  
  if is_component_single_bacteria(im, cc.PixelIdxList{c}, regions(c), component_image)
    ccim = imoverlay(ccim, tempzero, [1 .3 .3]);
  else
    ccim = imoverlay(ccim, tempzero, [.3 .3 1]);
  end
  ccim = imoverlay(ccim, s, [.3 1 .3]);

end

figure; imshow(ccim);