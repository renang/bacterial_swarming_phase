function indices = quick_2d_sub2ind(s, rows, cols)
  indices = rows + (cols-1)*s(1);
end
