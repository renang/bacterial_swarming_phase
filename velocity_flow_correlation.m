function correlation = velocity_flow_correlation(b1, b2, velocities)
  angle = acos(velocity_correlation(b1,b2,velocities));
  angle_diff = min(abs(angle), pi - abs(angle));
  correlation =  cos(2*angle_diff);
end
