function [x1 y1 x2 y2] = get_bounded_box_coordinates_from_region(region)
  x1 = region.BoundingBox(1);
  y1 = region.BoundingBox(2);
  x2 = region.BoundingBox(1) + region.BoundingBox(3);
  y2 = region.BoundingBox(2) + region.BoundingBox(4);
  x1 = int32(floor(x1)); y1 = int32(floor(y1)); x2 = int32(floor(x2)); y2 = int32(floor(y2));
  if x1 == 0
    x1 = 1;
  end
  if y1 == 0
    y1 = 1;
  end    
end