function indices = component_coordinates_to_image_coordinates(component_indices, region, component_image, im)
  %% Note: because we may get negative indices (this is by design), we want t ouse the non-checking method here. We fix everything according to the borders anyway.
  %%[y,x] = ind2sub(size(component_image), component_indices);
  [y,x] = quick_2d_ind2sub(size(component_image), component_indices);
  x = int32(floor(x + region.BoundingBox(1)));
  y = int32(floor(y + region.BoundingBox(2)));
  x(x==0) = 1;
  x(x>=size(im,2)) = size(im,2);
  y(y==0) = 1;
  y(y>=size(im,1)) = size(im,1);
  indices = quick_2d_sub2ind(size(im), y,x);
 end
  