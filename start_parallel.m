%%% Just a helper function that closes the parallel session before opening a new one (otherwise there is an error). Also configures to work with the given number of workers.
function start_parallel(worker_num)

  current_worker_num = matlabpool('size');
  if current_worker_num ~=0
    matlabpool('close');
  end

  p = parcluster('local');
  p.NumWorkers = worker_num;
  saveProfile(p);
  matlabpool(p);
  
end
