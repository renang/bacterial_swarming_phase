function varargout = preview_mask(varargin)
% PREVIEW_MASK MATLAB code for preview_mask.fig
%      PREVIEW_MASK, by itself, creates a new PREVIEW_MASK or raises the existing
%      singleton*.
%
%      H = PREVIEW_MASK returns the handle to a new PREVIEW_MASK or the handle to
%      the existing singleton*.
%
%      PREVIEW_MASK('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PREVIEW_MASK.M with the given input arguments.
%
%      PREVIEW_MASK('Property','Value',...) creates a new PREVIEW_MASK or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before preview_mask_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to preview_mask_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help preview_mask

% Last Modified by GUIDE v2.5 15-Jan-2017 10:53:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @preview_mask_OpeningFcn, ...
                   'gui_OutputFcn',  @preview_mask_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


function im = get_thresholded_image(handles)
    config = load_configuration;
    num_of_blocks_per_row = config.optical_flow.diluteQuiverFactor;
    block_length = size(handles.bw,1) / num_of_blocks_per_row;
    block_area = block_length * block_length;
    reshaped_bw = reshape(handles.bw, block_length, num_of_blocks_per_row, block_length, num_of_blocks_per_row);
    pixel_counts = reshape(sum(sum(reshaped_bw,1),3), num_of_blocks_per_row, num_of_blocks_per_row);

    normalized_counts = pixel_counts / block_area;
    mask = normalized_counts >= get(handles.threshold_slider, 'Value');
    large_mask = imresize(mask, [1024 1024], 'box');
    im = imoverlay(handles.im, imcomplement(large_mask), [0.0 0.0 0.0]);


function draw_modified_image(handles)  
  if handles.loaded_image
    axes(handles.axes_modified_image);
    im2 = get_thresholded_image(handles);      
    imshow(im2);
    axis off;
    axis image;
  end


function update_texts(handles)
  if handles.loaded_image    
    s = ['Threshold: ' num2str(get(handles.threshold_slider,'Value'))];    
    set(handles.text_threshold_value, 'String', s);   
  end


function draw_initial_images(handles)
  if handles.loaded_image
    axes(handles.original_image_axes);
    imshow(handles.im);
    axis off;
    axis image;

    draw_modified_image(handles);   
  end
  

function handles = load_image(hObject, handles, filename)
  [im, bw] = acquire_image_config_quick(filename);
  handles.im = im;   
  handles.bw = bw;
  handles.loaded_image = true;
  
  
% --- Executes just before preview_mask is made visible.
function preview_mask_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to preview_mask (see VARARGIN)

  handles.loaded_image = false; 
  draw_initial_images(handles);

  
% Choose default command line output for preview_mask
  handles.output = hObject;

% Update handles structure
  guidata(hObject, handles);

% UIWAIT makes preview_mask wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = preview_mask_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function threshold_slider_Callback(hObject, eventdata, handles)
% hObject    handle to threshold_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
  draw_modified_image(handles);
  update_texts(handles)
  
% --- Executes during object creation, after setting all properties.
function threshold_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to threshold_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in button_load_image.
function button_load_image_Callback(hObject, eventdata, handles)
% hObject    handle to button_load_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
  [filename, pathname] = uigetfile('*.tif');
  if ~isequal(filename,0)
    handles = load_image(hObject, handles, [pathname filename]);
    draw_initial_images(handles);
    guidata(hObject, handles);
  end

% --- Executes on button press in bw_checkbox.
function bw_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to bw_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of bw_checkbox
  draw_modified_image(handles);

  
% --- Executes when selected object is changed in uipanel2.
function uipanel2_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel2 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)  
  guidata(hObject, handles);
  draw_modified_image(handles);
  update_texts(handles);
