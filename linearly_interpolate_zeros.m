%% The function takes a data vector which is assumed to be a smooth curve, but for lack of data points has some zeros in between values. It linearly interpolates the zeros until there are none left.
%% We assume that regular zeros don't exist. This is true enough for x that is measured from the real world; we can hardly expect a perfect zero.
function y = linearly_interpolate_zeros(x)
  y = x;
  i = 1;
  while i < length(x)
    %% We found a zero!
    if x(i+1) == 0
      j = i+1;
      while x(j) == 0
	j = j + 1;
      end
      
      y(i:j) = linspace(x(i),x(j),j-i+1);
      i = j;
    else
      i = i +1;
    end
  end
end
