function plot_structure_factor_summary_graphs(all_structure_factor_fits, base_output_directory, bacteria_size)
  config = load_configuration();
  slope_absolute_values = zeros(1, length(config.density_bins)-1);

  for i = 1:length(config.density_bins)-1
      density = config.density_bins(i);
      
      density_indices = find(arrayfun(@(RIDX) all_structure_factor_fits{RIDX}.density == density, 1:length(all_structure_factor_fits)));
      if ~isempty(density_indices)
	fits_of_this_density = all_structure_factor_fits(density_indices);
	b_param = extractfield([fits_of_this_density{:}], 'b');
	slope_absolute_values(i) = mean(abs(b_param));	
      else
	slope_absolute_values(i) = 0;
      end
  end

  figure;
  plot(config.density_bins(1:end-1), slope_absolute_values, '.-');
  title(['Slope absolute values for ' config.structure_factor_test_name], 'Interpreter', 'None');
  ylabel('Absolute value of slope of power law fit');
  xlabel('Density');
  output_filename = fullfile(base_output_directory, config.structure_factor_test_name, [num2str(bacteria_size) '_summary_absolute_slope.png']);
  print(output_filename, '-dpng');
  close();
  
  %save to file
  text_output_filename = fullfile(base_output_directory, config.structure_factor_test_name, [num2str(bacteria_size) '_summary_absolute_slope.txt']);
  if size(config.density_bins,1)>size(config.density_bins,2)
    ddd=[config.density_bins(1:end-1),  slope_absolute_values];
  else
    ddd=[config.density_bins(1:end-1)',  slope_absolute_values'];
  end
  save(text_output_filename,'ddd','-ascii');
  
end
