function no_nans = remove_nans(x)
  no_nans = x;
  no_nans(isnan(no_nans)) = [];
end
