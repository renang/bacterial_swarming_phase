function [fit] = test_large_fluctuations_statistics(io_struct, bacteria_num_variances)
  config = load_configuration();
  test_name = config.large_fluctuations_test_name;
  display(['Now testing ' test_name]);
  create_directory_or_throw(io_struct.base_output_directory, test_name);
  create_directory_or_throw(fullfile(io_struct.base_output_directory, test_name), num2str(io_struct.bacteria_size));
  test_directory = fullfile(io_struct.base_output_directory, test_name, num2str(io_struct.bacteria_size));

%   %linear
%   figure;
%   plot(config.large_fluctuations_bin_sizes, bacteria_num_variances, '.-');
%   xlabel('Number of bins per size');
%   ylabel('Average variance in bacteria num');
%   title('Fluctuations in bacteria num in bins');
%   output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' large_fluctuations.png']);
%   print(output_filename, '-dpng');
%   close();
  
  avN=config.large_fluctuations_bin_sizes.^(-2)*io_struct.density;

%   %semi-log
%   figure;
%   plot(avN, log10(bacteria_num_variances), '.-');
%   xlabel('<n>');
%   ylabel('log_{10} (Average variance in bacteria num)');
%   title('Fluctuations in bacteria num in bins');
%   output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' large_fluctuations.png']);
%   print(output_filename, '-dpng');
%   close();
  
  %log-log
  xx=log10(avN);
  yy=log10(bacteria_num_variances);
  mask=avN>0 & bacteria_num_variances>0;
  xx=xx(mask);
  yy=yy(mask);
  figure;
  plot(xx, yy, '.-');
  xlabel('log_{10} (<n>)');
  ylabel('log_{10} (Average variance in bacteria num)');
  title('Fluctuations in bacteria num in bins');
  
  %linear fit
  pp=polyfit(xx(1:end),yy(1:end),1);
  hold on
  plot(xx(1:end),xx(1:end)*pp(1)+pp(2),'-r','linewidth',1);
  legend('data',['slope=' num2str(pp(1))],'location','northwest');
  set(gca,'fontsize',16);
  
  output_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' large_fluctuations.png']);
  print(output_filename, '-dpng');
  close();
  
  %save to file
  text_filename = fullfile(test_directory, ['density_' num2str(io_struct.density, '%.3f') '_' io_struct.filename_without_extension ' large_fluctuations.txt']);
  MMM=[xx ; yy ; xx*pp(1)+pp(2)]';
  save(text_filename,'MMM','-ascii');

  fit = pp(1);
end
