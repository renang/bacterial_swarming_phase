ttlHead=[data.experimentName ' - start at ' num2str(data.startFrame) '.'];

load(['OFdata/data_' data.experimentName '_' num2str(data.startFrame) '.mat'],'data');

%crop
data.ppf=data.ppf/numberOfSubsection;
data.windowSize=data.windowSize/numberOfSubsection;
startx=floor(data.vfsize(1)*((subSection(1)-1)/numberOfSubsection))+1;
endx=floor(data.vfsize(1)*((subSection(1))/numberOfSubsection));
if endx>data.vfsize(1); endx=data.vfsize(1); end;
starty=floor(data.vfsize(2)*((subSection(2)-1)/numberOfSubsection))+1;
endy=floor(data.vfsize(2)*((subSection(2))/numberOfSubsection));
if endy>data.vfsize(2); endy=data.vfsize(2); end;

data.pu=data.pu(:,startx:endx,starty:endy);
data.pv=data.pv(:,startx:endx,starty:endy);
data.iii=data.iii(1:endx-startx+1,1:endy-starty+1);
data.jjj=data.jjj(1:endx-startx+1,1:endy-starty+1);
data.vfsize=[size(data.pu,2) size(data.pu,3)];

speeds=sqrt(data.pu.^2+data.pv.^2);
speedsArray=reshape(speeds,size(speeds,1)*data.vfsize(1)*data.vfsize(2),1);
data.bins=0:0.5:data.maxSpeed;
data.h=hist(speedsArray,data.bins);

nu=data.pu./speeds;
nv=data.pv./speeds;
nfr=data.nFrames;
vort=zeros(nfr,data.vfsize(1),data.vfsize(2));
for f=1:nfr
    vort(f,:,:)=curl( reshape(data.pu(f,:,:),data.vfsize) ...
        ,reshape(data.pv(f,:,:),data.vfsize) )/data.dx;
end
vortArray=reshape(vort,size(vort,1)*size(vort,2)*size(vort,3),1); 


%histograms
run plotHistograms;

%correlations in time
run plotTimeCorrs;

%correlation in space
run plotSpaceCorrs;
