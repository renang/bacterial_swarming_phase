clear all
close all; clc;
data.fps=100; %100;
data.ppf=1024; %pixles per frame
data.windowSize=100; %window size in microns
data.diluteQuiverFactor=32; %16;  %even
data.dx=data.diluteQuiverFactor*data.windowSize/data.ppf;
distanceForVortiticySlope=5;
distanceForDirectionalSlope=5;
timeForCorrInTimeSlopes=0.3;
data.crop=0; %[1 800 201 1000];
numberOfSubsection=1; %power of 2
subSection=[1,1];

data.mainDirectory = 'YOUR IMAGE INPUT DIRECTORY HERE'; % outer folder
numberOfFrames=5;



tic

%data.subFolderName='32 32.5 33 33.5'; % inner folder and it runs all in parallel
data.subFolderName='.';
data.experimentName='Camera 1'; % file name name of tif file
data.nDigits=4;% number of "0" digits before number like 2 for 00500
data.startFrame=472; % name of first tif
data.skipFrames=1;

run main_calc_OF; 
%% run doPlots;

ttt=toc;
disp(['running time = ' num2str(ttt)]);
