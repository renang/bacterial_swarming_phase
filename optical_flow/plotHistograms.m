
speedsArray=reshape(speeds,nfr*data.vfsize(1)*data.vfsize(2),1);
vortArray=reshape(vort,size(vort,1)*size(vort,2)*size(vort,3),1);


%speed(all)
%%%%%%%%%%%%%%%%%

fg=figure(1);
bins=data.bins; %0:0.01:data.maxSpeed;
nbins=length(bins)-1;
cbins=(bins(1:nbins)+bins(2:nbins+1))/2;
hhh=data.h(1:nbins)/sum(data.h(1:nbins))/(bins(2)-bins(1));
bar(cbins,hhh);
ttl=[' speed histogram (all)'];
title(ttl,'fontsize',16);
axis([0 data.maxSpeed 0 max(hhh)]);

av=sum(cbins.*data.h(1:nbins))/sum(data.h);
stdev=sqrt(sum(cbins.^2.*data.h(1:nbins))/sum(data.h)-av^2);
[ccc,iii]=max(data.h(1:nbins));
md=cbins(iii);
xlabel(['[\mu m/sec]  ' 'average=' num2str(av) ' stdev=' num2str(stdev) ' mode=' num2str(md)],'fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[cbins;data.h(1:nbins)]';
save(fn,'xxx','-ascii');

%speed w fits
%%%%%%%%%%%%%%%%%
%
% vx=reshape(data.pu,size(speeds,1)*data.vfsize(1)*data.vfsize(2),1);
% vx=(vx-mean(vx))/std(vx);
% vy=reshape(data.pv,size(speeds,1)*data.vfsize(1)*data.vfsize(2),1);
% vy=(vy-mean(vy))/std(vy);
% centeredS=sqrt(vx.^2+vy.^2);
% lowCutoff=prctile(centeredS,50);
% highCutoff=prctile(centeredS,99.9);
% sTail=centeredS(centeredS>=lowCutoff & centeredS<=highCutoff);
% dbTail=(highCutoff-lowCutoff)/nbins;
% binsTail=lowCutoff:dbTail:highCutoff+0.5*dbTail;
% cbinsTail=(binsTail(1:nbins)+binsTail(2:nbins+1))/2;
% hTail=histc(sTail,binsTail);
% hhhTail=hTail(1:nbins)/sum(hTail)/(binsTail(2)-binsTail(1));
%
% logH=log(hhhTail);
% %logH=logH-logH(1);
% %cbinsTail=cbinsTail-cbinsTail(1);
%
% fg=figure(2);
% hold off
% plot(cbinsTail,logH,'b.','markersize',15);
% ttl=[' speed histogram (tail)'];
% title(ttl,'fontsize',16);
% axis([0 cbinsTail(end) min(logH) 0]);
%
% %quadratic fit
% p2=polyfit(cbinsTail,logH',2);
% yyy=p2(1)*cbinsTail.^2+p2(2)*cbinsTail+p2(3);
% hold on
% plot(cbinsTail,yyy,'-r','linewidth',2);
% text(double(cbinsTail(80)),double(yyy(20)),[num2str(p2(1),2) ' x^2 + ' num2str(p2(2),2) ' x + ' num2str(p2(3),2)],'color','r','fontsize',16);
%
% %streached fit
% fun=@(a,x) a(1)*x.^a(2)+a(3)*x+a(4);
% a0=[p2(1),2,p2(2),p2(3)];
% ax = lsqcurvefit(fun,double(a0),double(cbinsTail),double(logH'));
% yyy=fun(ax,cbinsTail);
% plot(cbinsTail,yyy,'--g','linewidth',2);
% text(double(cbinsTail(80)),double(yyy(40)),[num2str(ax(1),2) ' x^{' num2str(ax(2),2) '} + ' num2str(ax(3),2) ' x + ' num2str(ax(4),2)],'color','g','fontsize',16);
%
%
% fn=['results/' ttlHead ttl '.tif'];
% print(fg, '-r80', '-dtiff', fn);
% close(fg);

% fn=['results/' ttlHead ttl '.txt'];
% xxx=[cbinsTail';logH]';
% save(fn,'xxx','-ascii');


%speed(parts)
%%%%%%%%%%%%%%
ava=abs(vortArray);
ps=prctile(ava,[33 66]);
if min(ps)>0
    h1=hist(speedsArray(ava<=ps(1)),bins);
    h2=hist(speedsArray(ava>ps(1) & ava<ps(2)),bins);
    h3=hist(speedsArray(ava>=ps(2)),bins);
    hhh1=h1(1:nbins)/sum(h1(1:nbins))/(bins(2)-bins(1));
    hhh2=h2(1:nbins)/sum(h2(1:nbins))/(bins(2)-bins(1));
    hhh3=h3(1:nbins)/sum(h3(1:nbins))/(bins(2)-bins(1));
    clear ava;
    
    fg=figure(2);
    bar(cbins,hhh1);
    ttl=[' speed histogram (small vorticity)'];
    title(ttl,'fontsize',16);
    axis([0 data.maxSpeed 0 max(hhh1)]);
    av=sum(cbins.*h1(1:nbins))/sum(h1);
    [ccc,iii]=max(h1(1:nbins));
    md=cbins(iii);
    xlabel(['[\mu m/sec]  ' 'average=' num2str(av) ' mode=' num2str(md)],'fontsize',16);
    fn=['results/' ttlHead ttl '.tif'];
    print(fg, '-r80', '-dtiff', fn);
    close(fg);
    
    fn=['results/' ttlHead ttl '.txt'];
    xxx=[cbins;hhh1]';
    save(fn,'xxx','-ascii');
    
    fg=figure(3);
    bar(cbins,hhh2);
    ttl=[' speed histogram (intermediate vorticity)'];
    title(ttl,'fontsize',16);
    axis([0 data.maxSpeed 0 max(hhh2)]);
    av=sum(cbins.*h2(1:nbins))/sum(h2);
    [ccc,iii]=max(h2(1:nbins));
    md=cbins(iii);
    xlabel(['[\mu m/sec]  ' 'average=' num2str(av) ' mode=' num2str(md)],'fontsize',16);
    fn=['results/' ttlHead ttl '.tif'];
    print(fg, '-r80', '-dtiff', fn);
    close(fg);
    
    fn=['results/' ttlHead ttl '.txt'];
    xxx=[cbins;hhh2]';
    save(fn,'xxx','-ascii');
    
    fg=figure(4);
    bar(cbins,hhh3);
    ttl=[' speed histogram (large vorticity)'];
    title(ttl,'fontsize',16);
    axis([0 data.maxSpeed 0 max(hhh3)]);
    av=sum(cbins.*h3(1:nbins))/sum(h3);
    [ccc,iii]=max(h3(1:nbins));
    md=cbins(iii);
    xlabel(['[\mu m/sec]  ' 'average=' num2str(av) ' mode=' num2str(md)],'fontsize',16);
    fn=['results/' ttlHead ttl '.tif'];
    print(fg, '-r80', '-dtiff', fn);
    close(fg);
    
    fn=['results/' ttlHead ttl '.txt'];
    xxx=[cbins;hhh3]';
    save(fn,'xxx','-ascii');
    
end %if min(ps)>0


% vx and vy
%%%%%%%%%%%%%%%%%

vxArray=reshape(data.pu,nfr*data.vfsize(1)*data.vfsize(2),1);
vyArray=reshape(data.pv,nfr*data.vfsize(1)*data.vfsize(2),1);

avx=mean(vxArray);
avy=mean(vyArray);
stdx=std(vxArray);
stdy=std(vyArray);
nx=(vxArray-avx)/stdx;
ny=(vyArray-avy)/stdy;

maxSpeed=double(max(abs([nx;ny]))*0.9);
bins1D=-maxSpeed:double(2*maxSpeed/nbins):maxSpeed;
nbins1D=length(bins1D)-1;
cbins1D=(bins1D(1:nbins1D)+bins1D(2:nbins1D+1))/2;
h1Dx=hist(nx,bins1D);
h1Dy=hist(ny,bins1D);
hhh1Dx=h1Dx(1:nbins1D)/sum(h1Dx(1:nbins1D))/(bins1D(2)-bins1D(1));
hhh1Dy=h1Dy(1:nbins1D)/sum(h1Dy(1:nbins1D))/(bins1D(2)-bins1D(1));

[mux,sigx]=normfit(nx);
[muy,sigy]=normfit(ny);
fitxGauss=normpdf(cbins1D,mux,sigx);
fityGauss=normpdf(cbins1D,muy,sigy);

muhatx=expfit(abs(nx));
muhaty=expfit(abs(ny));
fitxExp=exppdf(abs(cbins1D),muhatx)/2;
fityExp=exppdf(abs(cbins1D),muhaty)/2;

m4x=mean(nx.^4);
m4y=mean(ny.^4);

%streached exp
fff=floor(nbins1D/5);
xdata=cbins1D(fff:nbins1D-fff+1);
ydata=-log(hhh1Dx(fff:nbins1D-fff+1));
xdata=xdata(ydata>0 & ydata<1e9);
ydata=ydata(ydata>0 & ydata<1e9);
fun=@(a,x) a(1)*x.^a(2)+a(3);
a0=[double(max(ydata)/max(xdata)),1,double(min(ydata))];
ax = lsqcurvefit(fun,a0,double(abs(xdata)),double(ydata));
xxx=min(xdata):0.1:max(xdata);
sexpx=exp(-ax(1)*abs(xxx).^ax(2)-ax(3));

xdata=cbins1D(fff:nbins1D-fff+1);
ydata=-log(hhh1Dy(fff:nbins1D-fff+1));
xdata=xdata(ydata>0 & ydata<1e9);
ydata=ydata(ydata>0 & ydata<1e9);
fun=@(a,x) a(1)*x.^a(2)+a(3);
a0=[max(ydata)/max(xdata),1,min(ydata)];
ay = lsqcurvefit(fun,a0,abs(xdata),ydata);
sexpy=exp(-ay(1)*abs(xxx).^ay(2)-ay(3));

%stable fit
stblX = stblfit(double(nx),'percentile');
stblY = stblfit(double(ny),'percentile');
fitStableX=stblpdf(cbins1D,stblX(1),stblX(2),stblX(3),stblX(4));
fitStableY=stblpdf(cbins1D,stblY(1),stblY(2),stblY(3),stblY(4));

fg=figure(2);
hold off
bar(cbins1D,hhh1Dx,'b');
hold on
ttl=[' vx with fits Gauss (red) exp (black) streached exp (purple)'];
title(ttl,'fontsize',16);
mmm=max(hhh1Dx);
axis([-maxSpeed maxSpeed 0 max(hhh1Dx)*1.1]);
xlabel(['[\mu m/sec]  '],'fontsize',16);
plot(xxx,sexpx,'-m','linewidth',2);
plot(cbins1D,fitxGauss,'-r','linewidth',2);
plot(cbins1D,fitxExp,'-k','linewidth',2);
plot(cbins1D,fitStableX,'-','color',[1 0.6 0],'linewidth',2);
text(-0.95*maxSpeed,mmm*1.0,['av(x)=' num2str(avx) ' std(vx)=' num2str(stdx)],'fontsize',14);
text(-0.95*maxSpeed,mmm*0.9,['exp rate=' num2str(muhatx)],'fontsize',14);
text(-0.95*maxSpeed,mmm*0.8,['scaled 4th moment=' num2str(m4x)],'fontsize',14);
text(-0.95*maxSpeed,mmm*0.7,['streached exp power=' num2str(ax(2))],'fontsize',14);
text(-0.95*maxSpeed,mmm*0.6,['stable exp power=' num2str(stblX(1))],'fontsize',14);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fg=figure(2);
hold off
bar(cbins1D,hhh1Dy,'b');
hold on
ttl=[' vy with fits Gauss (red) exp (black) streached exp (purple)'];
title(ttl,'fontsize',16);
mmm=max(hhh1Dy);
axis([-maxSpeed maxSpeed 0 max(hhh1Dy)*1.1]);
xlabel(['[\mu m/sec]  '],'fontsize',16);
plot(xxx,sexpx,'-m','linewidth',2);
plot(cbins1D,fityGauss,'-r','linewidth',2);
plot(cbins1D,fityExp,'-k','linewidth',2);
plot(cbins1D,fitStableY,'-','color',[1 0.6 0],'linewidth',2);
text(-0.95*maxSpeed,mmm*1.0,['av(y)=' num2str(avy) ' std(vy)=' num2str(stdy)],'fontsize',14);
text(-0.95*maxSpeed,mmm*0.9,['exp rate=' num2str(muhaty)],'fontsize',14);
text(-0.95*maxSpeed,mmm*0.8,['scaled 4th moment=' num2str(m4y)],'fontsize',14);
text(-0.95*maxSpeed,mmm*0.7,['streached exp power=' num2str(ay(2))],'fontsize',14);
text(-0.95*maxSpeed,mmm*0.6,['stable exp power=' num2str(stblY(1))],'fontsize',14);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);



fn=['results/' ttlHead ttl '.txt'];
xxx=double([cbins1D;hhh1Dx(1:nbins1D);hhh1Dy(1:nbins1D);fitxGauss(1:nbins1D);fityGauss(1:nbins1D);fitxExp(1:nbins1D);fityExp(1:nbins1D);fitStableX(1:nbins1D);fitStableY(1:nbins1D)]');
save(fn,'xxx','-ascii');

fg=figure(2);
plot(vxArray(1:10:end),vyArray(1:10:end),'k.','markersize',3);
cor=corrcoef(vxArray,vyArray);
ttl=[' corr(vx,vy)'];
title(ttl,'fontsize',16);
axis([-maxSpeed maxSpeed -maxSpeed maxSpeed]);
xlabel(['vx [\mu m/sec]  correlation=' num2str(cor(1,2))],'fontsize',16);
ylabel(['vy [\mu m/sec]'],'fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);



%Rayleigh
%%%%%%%%%%%%%%%%%%%%

vxArray=vxArray-mean(vxArray);
vyArray=vyArray-mean(vyArray);
vxArray=vxArray/std(vxArray);
vyArray=vyArray/std(vyArray);
s=sqrt(vxArray.^2+vyArray.^2);

range=5;
bins2=0:range/100:range;
xxx2=(bins2(1:end-1)+bins2(2:end))/2;
hsa=hist(s,bins2);
hhsa=hsa(1:end-1);
esa=hhsa/trapz(xxx2,hhsa);

fg=figure(3);
ttl=[' Rayleigh'];
hold off
plot(xxx2,esa,'linewidth',2);
fff=raylpdf(xxx2,1);
hold on
plot(xxx2,fff,'r','linewidth',2);
title(ttl,'fontsize',16);
mmm=max(esa);
axis([0 range 0 mmm*1.1]);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=double([xxx2;esa]');
save(fn,'xxx','-ascii');



%vorticity(all)
%%%%%%%%%%%%%%%%%%%%

maxVort=max(max(max(abs(vort))))*0.9;
nbinsV=200;
dbV=2*maxVort/nbinsV;
binsV=-maxVort:dbV:maxVort+dbV/10;
cbinsV=(binsV(1:nbinsV)+binsV(2:nbinsV+1))/2;
histV=hist(vortArray,binsV);
histV=histV/sum(histV)/(binsV(2)-binsV(1));

fg=figure(5);
bar(cbinsV,histV(1:nbinsV))
ttl=' vorticity histogram (all)';
title(ttl,'fontsize',16);
axis([-maxVort maxVort 0 max(histV)]);

av=sum(cbinsV.*histV(1:nbinsV))/sum(histV);
stdr=sqrt(sum(cbinsV.^2.*histV(1:nbinsV))/sum(histV)-av^2);
[ccc,iii]=max(histV(1:nbinsV));
md=cbinsV(iii);
xlabel(['[rad/sec]  ' 'average=' num2str(av) ' std=' num2str(stdr) ' mode=' num2str(md)],'fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[cbinsV;histV(1:nbinsV)]';
save(fn,'xxx','-ascii');


%vorticity(master)
%%%%%%%%%%%%%%%%%%%%


%sigma=std(speedsArray)*1.525;
sigma=std(vortArray);
masterV=(vortArray-mean(vortArray))/sigma;

maxVortM=max(max(max(abs(masterV))))*0.8;
nbinsVM=200;
dbVM=2*maxVortM/nbinsVM;
binsVM=-maxVortM:dbVM:maxVortM+dbVM/10;
cbinsVM=(binsVM(1:nbinsVM)+binsVM(2:nbinsVM+1))/2;
histVM=hist(masterV,binsVM);
histVM=histVM/sum(histVM)/(binsVM(2)-binsVM(1));

fg=figure(5);
bar(cbinsVM,histVM(1:nbinsVM),'b')
ttl=' vorticity histogram (master) with fits Gauss (red) exp (black) streached exp (purple)';
title(ttl,'fontsize',16);
axis([-maxVortM maxVortM 0 max(histVM)*1.1]);

avM=sum(cbinsVM.*histVM(1:nbinsVM))/sum(histVM);
stdrM=std(masterV); %sqrt(sum(cbinsV.^2.*histV(1:nbinsV))/sum(histV)-av^2);
[ccc,iii]=max(histVM(1:nbinsVM));
xlabel(['[rad/sec] std=' num2str(stdrM)],'fontsize',16);

hold on

nrmv=masterV/std(masterV);
m4v=mean(nrmv.^4);
text(double(-0.9*maxVortM),double(0.9*max(histVM)),['scaled 4th moment=' num2str(m4v)],'fontsize',14);

%streached exp
fff=floor(nbinsVM/5);
xdata=double(cbinsVM(fff:nbinsVM-fff+1));
ydata=double(-log(histVM(fff:nbinsVM-fff+1)));
xdata=xdata(ydata>0 & ydata<1e9);
ydata=ydata(ydata>0 & ydata<1e9);
fun=@(a,x) a(1)*x.^a(2)+a(3);
a0=[max(ydata)/max(xdata),2,min(ydata)];
av = lsqcurvefit(fun,a0,abs(xdata),ydata);
xxx=min(xdata):0.01:max(xdata);
sexpv=exp(-av(1)*abs(xxx).^av(2)-av(3));
plot(xxx,sexpv,'-m','linewidth',2);
text(double(-0.9*maxVortM),double(0.8*max(histVM)),['streached exp power=' num2str(av(2))],'fontsize',14);

yyy=normpdf(cbinsVM,0,stdrM);
plot(cbinsVM,yyy,'r','linewidth',2);

muhatV=expfit(abs(masterV));
fitxExp=exppdf(abs(cbinsVM),muhatV)/2;
plot(cbinsVM,fitxExp,'-k','linewidth',2);

%stable fit
stblV = stblfit(double(masterV),'percentile');
fitStableV=stblpdf(cbins1D,stblX(1),stblX(2),stblX(3),stblX(4));
plot(cbinsVM,fitStableV,'-','color',[1 0.6 0],'linewidth',2);
text(double(-0.9*maxVortM),double(0.7*max(histVM)),['stable exp power=' num2str(stblV(1))],'fontsize',14);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=double([cbinsVM;histVM(1:nbinsVM);yyy]');
save(fn,'xxx','-ascii');


%vorticity(parts)
%%%%%%%%%%%%%%%%%%%%%

ps=prctile(speedsArray,[33 66]);
if min(ps)>0
    histV1=hist(vortArray(speeds<=ps(1)),binsV);
    histV2=hist(vortArray(speeds>ps(1) & speeds<ps(2)),binsV);
    histV3=hist(vortArray(speeds>=ps(2)),binsV);
    histV1=histV1/sum(histV1)/(binsV(2)-binsV(1));
    histV2=histV2/sum(histV2)/(binsV(2)-binsV(1));
    histV3=histV3/sum(histV3)/(binsV(2)-binsV(1));
    
    fg=figure(6);
    bar(cbinsV,histV1(1:nbinsV))
    ttl=' vorticity histogram (small speed)';
    title(ttl,'fontsize',16);
    axis([-maxVort maxVort 0 max(histV1)]);
    av=sum(cbinsV.*histV1(1:nbinsV))/sum(histV1);
    stdr=sqrt(sum(cbinsV.^2.*histV1(1:nbinsV))/sum(histV1)-av^2);
    [ccc,iii]=max(histV1(1:nbinsV));
    md=cbinsV(iii);
    xlabel(['[rad/sec]  ' 'average=' num2str(av) ' std=' num2str(stdr) ' mode=' num2str(md)],'fontsize',16);
    fn=['results/' ttlHead ttl '.tif'];
    print(fg, '-r80', '-dtiff', fn);
    close(fg);
    
    fg=figure(7);
    bar(cbinsV,histV2(1:nbinsV))
    ttl=' vorticity histogram (intermediate speed)';
    title(ttl,'fontsize',16);
    axis([-maxVort maxVort 0 max(histV2)]);
    av=sum(cbinsV.*histV2(1:nbinsV))/sum(histV2);
    stdr=sqrt(sum(cbinsV.^2.*histV2(1:nbinsV))/sum(histV2)-av^2);
    [ccc,iii]=max(histV1(1:nbinsV));
    md=cbinsV(iii);
    xlabel(['[rad/sec]  ' 'average=' num2str(av) ' std=' num2str(stdr) ' mode=' num2str(md)],'fontsize',16);
    fn=['results/' ttlHead ttl '.tif'];
    print(fg, '-r80', '-dtiff', fn);
    close(fg);
    
    fg=figure(8);
    bar(cbinsV,histV3(1:nbinsV))
    ttl=' vorticity histogram (large speed)';
    title(ttl,'fontsize',16);
    axis([-maxVort maxVort 0 max(histV3)]);
    av=sum(cbinsV.*histV3(1:nbinsV))/sum(histV3);
    stdr=sqrt(sum(cbinsV.^2.*histV3(1:nbinsV))/sum(histV3)-av^2);
    [ccc,iii]=max(histV3(1:nbinsV));
    md=cbinsV(iii);
    xlabel(['[rad/sec]  ' 'average=' num2str(av) ' std=' num2str(stdr) ' mode=' num2str(md)],'fontsize',16);
    fn=['results/' ttlHead ttl '.tif'];
    print(fg, '-r80', '-dtiff', fn);
    close(fg);
    
end %if min(ps)>0

%abs vorticity(all)
%%%%%%%%%%%%%%%%%%%%%%

maxVort=max(max(max(abs(vort))));
nbinsV=200;
dbV=maxVort/nbinsV;
binsV=0:dbV:maxVort+dbV/10;
cbinsV=(binsV(1:nbinsV)+binsV(2:nbinsV+1))/2;
histV=hist(abs(vortArray),binsV);
histV=histV(1:nbinsV)/sum(histV(1:nbinsV))/(binsV(2)-binsV(1));

fg=figure(9);
bar(cbinsV,histV(1:nbinsV))
ttl=' abs(vorticity) histogram (all)';
title(ttl,'fontsize',16);
axis([0 maxVort 0 max(histV)]);

av=sum(cbinsV.*histV(1:nbinsV))/sum(histV);
stdr=sqrt(sum(cbinsV.^2.*histV(1:nbinsV))/sum(histV)-av^2);
[ccc,iii]=max(histV(1:nbinsV));
md=cbinsV(iii);
xlabel(['[rad/sec]  ' 'average=' num2str(av) ' std=' num2str(stdr) ' mode=' num2str(md)],'fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[cbinsV;histV]';
save(fn,'xxx','-ascii');

%speed vs. vorticity
%%%%%%%%%%%%%%%%%%%%%%

%%nSamples=50000;
nSamples = 500;
ns=length(speedsArray);
sp=1:floor(ns/nSamples):ns;
datx=speedsArray(sp);
daty=abs(vortArray(sp));
cr=corr([datx daty]);

fg=figure(10);
plot(datx,daty,'.','markersize',1);
xlabel(['speed   correlation=' num2str(cr(2,1))],'fontsize',12);
ylabel('|vorticity|','fontsize',12);

ttl=' corr(speed,abs(vorticity))';
title(ttl,'fontsize',16);
axis([0 max(datx) 0 max(daty)]);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=double([datx daty]);
save(fn,'xxx','-ascii');


