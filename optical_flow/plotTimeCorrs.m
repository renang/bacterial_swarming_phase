
maxTime=floor(200/data.skipFrames);
if maxTime>nfr
    maxTime=floor(nfr/2/data.skipFrames);
end
inSpeed=zeros(maxTime,1);
inDir=zeros(maxTime,1);
inVort=zeros(maxTime,1);
avSpeed=mean(mean(speeds,3),2);
avU=mean(mean(nu,3),2);
avV=mean(mean(nv,3),2);
avC=mean(mean(vort,3),2);

bases=1:10:nfr/data.skipFrames-maxTime;
for b=bases
    for fr=1:maxTime
        inSpeed(fr)=inSpeed(fr)+mean(mean(speeds(b,:,:).*speeds(b+fr,:,:),3),2)-avSpeed(b)*avSpeed(b+fr);
        inDir(fr)=inDir(fr)+mean(mean(nu(b,:,:).*nu(b+fr,:,:)+nv(b,:,:).*nv(b+fr,:,:),3),2)-avU(b)*avU(b+fr)-avV(b)*avV(b+fr);
        inVort(fr)=inVort(fr)+mean(mean(vort(b,:,:).*vort(b+fr,:,:),3),2)-avC(b)*avC(b+fr);
    end
end
inSpeed=inSpeed/length(bases);
inSpeed=inSpeed/inSpeed(1);
inDir=inDir/inDir(1);
inVort=inVort/inVort(1);

ttt=((0:maxTime-1)/data.fps*data.skipFrames)';
pointForSlope=find(ttt<=timeForCorrInTimeSlopes,1,'last');


%speed
%%%%%%%%%%%%

fg=figure(12);
hold off
plot(ttt,inSpeed,'linewidth',2);
hold on
plot([ttt(1) ttt(end)],[0 0],':k','linewidth',2);
ttl=' speed corr in time';
title(ttl,'fontsize',16);

at02=find(inSpeed<0.2,1,'first');
timeAt02=(ttt(at02-1)+ttt(at02))/2;
at01=find(inSpeed<0.1,1,'first');
timeAt01=(ttt(at01-1)+ttt(at01))/2;

xlabel(['t [sec]',10,'time@0.2=' num2str(timeAt02,2) ' time@0.1=' num2str(timeAt01,2)],'fontsize',16);
ylabel('correlation','fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[ttt,inSpeed];
save(fn,'xxx','-ascii');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


fg=figure(13);
neg=floor(find(inSpeed<0.2,1,'first')/5)-1;
neg0=floor(find(inSpeed<0,1,'first'))-1;
if isempty(neg)==true || neg<10
    neg=length(ttt)-10; %floor(length(inDir)/2);
    if neg>neg0
        neg=neg0;
    end
end
hold off
plot(((ttt(2:pointForSlope))),log(inSpeed(2:pointForSlope)),'.b','markersize',20);
hold on
ttl=' speed corr in time (semilog)';
title(ttl,'fontsize',16);
ylabel('log(correlation)','fontsize',16);

p=polyfit((ttt(2:pointForSlope)),log((inSpeed(2:pointForSlope))),1);
fun=@(x) p(1)*x+p(2);
plot([(ttt(2)) (ttt(pointForSlope))],[fun((ttt(2))) fun((ttt(pointForSlope)))],'--r','linewidth',2);
%xlabel(['t [sec]    fit: ' num2str(p(1),3) 't+' num2str(p(2),3)],'fontsize',16);

xdata=ttt(1:pointForSlope);
ydata=log(inSpeed(1:pointForSlope));
funt=@(a,x) a(1)*x.^a(2)+a(3);
x0=[-1,1,0];
a = lsqcurvefit(funt,x0,xdata,ydata);
fun2=@(x) a(1)*x.^a(2)+a(3);
plot(ttt(1:pointForSlope),fun2(ttt(1:pointForSlope)),'--k','linewidth',2);

p3=polyfit(log(ttt(2:pointForSlope)),log(inSpeed(2:pointForSlope)),1);
fun3=@(x) p3(1)*log(x)+p3(2);
plot(ttt(2:pointForSlope),fun3(ttt(2:pointForSlope)),'--m','linewidth',2);

xlabel('t [sec]','fontsize',16);
text(0.05,-0.1,['linear fit: ' num2str(p(1),2) ' t + ' num2str(p(2),2)],'color','r','fontsize',16);
text(0.05,-0.25,['curve: ' num2str(a(1),2) ' t^{' num2str(a(2),2) '} + ' num2str(a(3),2)],'color','k','fontsize',16);
text(0.05,-0.4,['loglog: ' num2str(p3(1),2) ' log(t) + ' num2str(p3(2),2)],'color','m','fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[xdata,ydata,fun(xdata),fun2(xdata),fun3(xdata)];
save(fn,'xxx','-ascii');


%direction
%%%%%%%%%%%%%%%

fg=figure(14);
hold off
plot(ttt,inDir,'linewidth',2);
hold on
plot([ttt(1) ttt(end)],[0 0],':k','linewidth',2);
ttl=' dir corr in time';
title(ttl,'fontsize',16);

at02=find(inDir<0.2,1,'first');
timeAt02=(ttt(at02-1)+ttt(at02))/2;
at01=find(inDir<0.1,1,'first');
timeAt01=(ttt(at01-1)+ttt(at01))/2;

xlabel(['t [sec]',10,'time@0.2=' num2str(timeAt02,2) ' time@0.1=' num2str(timeAt01,2)],'fontsize',16);
ylabel('correlation','fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[ttt,inDir];
save(fn,'xxx','-ascii');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


fg=figure(15);
neg=find(inDir<0.2,1,'first')-20;
if isempty(neg)==true || neg<10
    neg=length(ttt)-10; %neg=floor(length(inDir)/2);
end
hold off
plot(ttt(1:pointForSlope),log(inDir(1:pointForSlope)),'.b','markersize',20);
hold on
ttl=' dir corr in time (semilog)';
title(ttl,'fontsize',16);
ylabel('log(correlation)','fontsize',16);

p=polyfit(ttt(1:pointForSlope),log(inDir(1:pointForSlope)),1);
fun=@(x) p(1)*x+p(2);
plot([ttt(1) ttt(pointForSlope)],[fun(ttt(1)) fun(ttt(pointForSlope))],'--r','linewidth',2);

xdata=ttt(1:pointForSlope);
ydata=log(inDir(1:pointForSlope));
funt=@(a,x) a(1)*x.^a(2)+a(3);
x0=[-1,1,0];
a = lsqcurvefit(funt,x0,xdata,ydata);
fun2=@(x) a(1)*x.^a(2)+a(3);
plot(ttt(1:pointForSlope),fun2(ttt(1:pointForSlope)),'--k','linewidth',2);

p3=polyfit(log(ttt(2:pointForSlope)),log(inDir(2:pointForSlope)),1);
fun3=@(x) p3(1)*log(x)+p3(2);
plot(ttt(2:pointForSlope),fun3(ttt(2:pointForSlope)),'--m','linewidth',2);

%two exponentials
fun4=@(a,x) log(a(1)*exp(-a(2)*x)+(1-a(1))*exp(-a(3)*x));
x0=[1,1,1];
a2 = lsqcurvefit(fun4,x0,xdata,ydata);
plot(double(ttt(1:pointForSlope)),double(real(fun4(a2,ttt(1:pointForSlope)))),'--','color',[0 0.7 0],'linewidth',2);

xlabel('t [sec]','fontsize',16);
text(0.01,-0.0,['linear fit: ' num2str(p(1),2) ' t + ' num2str(p(2),2)],'color','r','fontsize',16);
text(0.01,-0.2,['curve: ' num2str(a(1),2) ' t^{' num2str(a(2),2) '} + ' num2str(a(3),2)],'color','k','fontsize',16);
text(0.01,-0.4,['loglog: ' num2str(p3(1),2) ' log(t) + ' num2str(p3(2),2)],'color','m','fontsize',16);
text(0.01,-0.6,['2 exps: C=' num2str(a2(1),2) ' exp(-' num2str(a2(2),2) 't) + ' num2str(1-a2(1),2) ' exp(-' num2str(a2(3),2) 't)'],'color',[0 0.7 0],'fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[xdata,ydata,fun(xdata),fun2(xdata),fun3(xdata),fun4(a2,xdata)];
save(fn,'xxx','-ascii');


%vorticity
%%%%%%%%%%%%%%%

fg=figure(16);
hold off
plot(ttt,inVort,'linewidth',2);
hold on
plot([ttt(1) ttt(end)],[0 0],':k','linewidth',2);
ttl=' vorticity corr in time';
title(ttl,'fontsize',16);

at02=find(inVort<0.2,1,'first');
timeAt02=(ttt(at02-1)+ttt(at02))/2;
at01=find(inVort<0.1,1,'first');
timeAt01=(ttt(at01-1)+ttt(at01))/2;

xlabel(['t [sec]',10,'time@0.2=' num2str(timeAt02,2) ' time@0.1=' num2str(timeAt01,2)],'fontsize',16);
ylabel('correlation','fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[ttt,inVort];
save(fn,'xxx','-ascii');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fg=figure(17);
neg=floor(find(inVort<0.2,1,'first')/2);
if isempty(neg)==true || neg<10
    neg=length(ttt)-10; %floor(length(inDir)/2);
end
hold off
plot(ttt(1:pointForSlope),log(inVort(1:pointForSlope)),'.b','markersize',20);
hold on
ttl=' vorticity corr in time (semilog)';
title(ttl,'fontsize',16);
ylabel('log(correlation)','fontsize',16);

p=polyfit(ttt(1:pointForSlope),log(inVort(1:pointForSlope)),1);
fun=@(x) p(1)*x+p(2);
plot([ttt(1) ttt(pointForSlope)],[fun(ttt(1)) fun(ttt(pointForSlope))],'--r','linewidth',2);
%xlabel(['t [sec]   fit: ' num2str(p(1),3) 't+' num2str(p(2),3)],'fontsize',16);

xdata=ttt(1:pointForSlope);
ydata=log(inVort(1:pointForSlope));
funt=@(a,x) a(1)*x.^a(2)+a(3);
x0=[-1,1,0];
a = lsqcurvefit(funt,x0,xdata,ydata);
fun2=@(x) a(1)*x.^a(2)+a(3);
plot(ttt(1:pointForSlope),fun2(ttt(1:pointForSlope)),'--k','linewidth',2);

p3=polyfit(log(ttt(2:pointForSlope)),log(inVort(2:pointForSlope)),1);
fun3=@(x) p3(1)*log(x)+p3(2);
plot(ttt(2:pointForSlope),fun3(ttt(2:pointForSlope)),'--m','linewidth',2);

xlabel('t [sec]','fontsize',16);
text(0.05,-0.1,['linear fit: ' num2str(p(1),2) ' t + ' num2str(p(2),2)],'color','r','fontsize',16);
text(0.05,-0.25,['curve: ' num2str(a(1),2) ' t^{' num2str(a(2),2) '} + ' num2str(a(3),2)],'color','k','fontsize',16);
text(0.05,-0.4,['loglog: ' num2str(p3(1),2) ' log(t) + ' num2str(p3(2),2)],'color','m','fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[xdata,ydata,fun(xdata),fun2(xdata),fun3(xdata)];
save(fn,'xxx','-ascii');



