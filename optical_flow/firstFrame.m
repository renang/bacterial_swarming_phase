clear all

im1=imread('../frames/frame1.jpg');
im2=imread('../frames/frame10.jpg');
[u,v]=HS(im1,im2,1,100,zeros(size(im1(:,:,1))),zeros(size(im2(:,:,1))),0);

diluteQuiverFactor=30;
is=diluteQuiverFactor/2:diluteQuiverFactor:size(im1,1);
js=diluteQuiverFactor/2:diluteQuiverFactor:size(im1,2);
[iii,jjj]=meshgrid(js,is);
pu=u(is,js);
pv=v(is,js);

whiteThreshold=220;
cim1=im1(is,js);
idx=find(cim1<whiteThreshold);

imshow(im1);
hold on
%quiver(iii,jjj,pu,pv,1.5,'r','linewidth',2);
quiver(iii(idx),jjj(idx),pu(idx),pv(idx),1.0,'r','linewidth',2);

