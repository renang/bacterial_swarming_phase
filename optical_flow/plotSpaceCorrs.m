

maxdist=max(data.vfsize);
nbins=100/numberOfSubsection;
pntsPerBin=100000;
db=maxdist/nbins;
bins=0:db:maxdist+db/10;
nbins=length(bins)-1;
cbins=(bins(1:nbins)+bins(2:nbins+1))/2;

xs=reshape(data.iii/data.iii(1,1)/2,data.vfsize(1)*data.vfsize(2),1);
ys=reshape(data.jjj/data.jjj(1,1)/2,data.vfsize(1)*data.vfsize(2),1);

pos=[xs,ys];
pdt=pdist(pos);
pd=squareform(pdt);

inSpeed=zeros(nbins,1);
inDir=zeros(nbins,1);
inVort=zeros(nbins,1);
inSpeedW=zeros(nbins,1);
inDirW=zeros(nbins,1);
inVortW=zeros(nbins,1);

for b=1:nbins
    %disp(b);
    [r,c]=find(pd>=bins(b) & pd<bins(b+1));
    r2=r(r<=c);
    c2=c(r<=c);
    
    k=length(r2);
    [rr,rc]=ind2sub(data.vfsize,r2);
    [cr,cc]=ind2sub(data.vfsize,c2);
    
    if k>0
        ls=randi(k,pntsPerBin,1);
        ts=randi(nfr,pntsPerBin,1);
        indr=sub2ind(size(speeds),ts,rr(ls),rc(ls));
        indc=sub2ind(size(speeds),ts,cr(ls),cc(ls));
        
        dx=[rr(ls)-cr(ls) rc(ls)-cc(ls)];
        dxn=sqrt(dx(:,1).^2 + dx(:,2).^2);
        dx(:,1)=dx(:,1)./dxn;
        dx(:,2)=dx(:,2)./dxn;
        dx(isnan(dx))=0;
        weights=1-abs(dx(:,1).*nu(indr)+dx(:,2).*nv(indr));
        sumW=sum(weights);
        
        inSpeed(b)=mean(speeds(indr).*speeds(indc))-mean(speeds(indr))*mean(speeds(indc));
        inDir(b)=mean(nu(indr).*nu(indc)+nv(indr).*nv(indc))-mean(nu(indr))*mean(nu(indc))-mean(nv(indr))*mean(nv(indc));
        inVort(b)=mean(vort(indr).*vort(indc))-mean(vort(indr))*mean(vort(indc));
        
        inSpeedW(b)=sum(speeds(indr).*speeds(indc).*weights)/sumW-sum(speeds(indr).*weights)*sum(speeds(indc).*weights)/sumW/sumW;
        inDirW(b)=sum((nu(indr).*nu(indc)+nv(indr).*nv(indc)).*weights)/sumW-sum(nu(indr).*weights)*sum(nu(indc).*weights)/sumW/sumW-sum(nv(indr).*weights)*sum(nv(indc).*weights)/sumW/sumW;
        inVortW(b)=sum(vort(indr).*vort(indc).*weights)/sumW-sum(vort(indr).*weights)*sum(vort(indc).*weights)/sumW/sumW;
    else
        inSpeedW(b)=0;
        inDirW(b)=0;
        inVortW(b)=0;
    end
    
end

inSpeed=inSpeed/inSpeed(1);
inDir=inDir/inDir(1);
inVort=inVort/inVort(1);
inSpeedW=inSpeedW/inSpeedW(1);
inDirW=inDirW/inDirW(1);
inVortW=inVortW/inVortW(1);
ttt=cbins'*data.windowSize/max(data.vfsize);

dist3micros=find(ttt<=distanceForVortiticySlope,1,'last')+1;
dist15micros=find(ttt<=distanceForDirectionalSlope,1,'last')+1;

%speed
fg=figure(22);
hold off
plot(ttt,inSpeed,'linewidth',2);
hold on
plot([ttt(1) ttt(end)],[0 0],':k','linewidth',2);
ttl=' speed corr in dist';
title(ttl,'fontsize',16);
xlabel('r [um]','fontsize',16);
ylabel('correlation','fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[ttt;inSpeed]';
save(fn,'xxx','-ascii');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fg=figure(23);
neg0=floor(find(inSpeed<0,1,'first'));
hold off
xdata=ttt(1:dist15micros);
ydata=log(inSpeed(1:dist15micros));
plot(xdata,ydata,'.b','markersize',20);
hold on
ttl=' speed corr in dist (semilog)';
title(ttl,'fontsize',16);
ylabel('log(correlation)','fontsize',16);

p=polyfit(ttt(1:dist15micros),log(inSpeed(1:dist15micros)),1);
fun=@(x) p(1)*x+p(2);
plot([ttt(1) ttt(dist15micros)],[fun(ttt(1)) fun(ttt(dist15micros))],'--r','linewidth',2);
txt=[num2str(p(1),3) 'r+' num2str(p(2),3)];
xxx=ttt(floor(dist15micros/3));
yyy=1.5*fun(ttt(floor(dist15micros/3)));
xlabel(['r [um]   fit=' txt],'fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[xdata;ydata;fun(xdata)]';
save(fn,'xxx','-ascii');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%direction
fg=figure(24);
hold off
plot(ttt,inDir,'linewidth',2);
hold on
plot([ttt(1) ttt(end)],[0 0],':k','linewidth',2);
ttl=' dir corr in dist';
title(ttl,'fontsize',16);
neg0=floor(find(inDir<=0,1,'first'));
xlabel(['r [um]  first zero @' num2str(ttt(neg0))],'fontsize',16);
ylabel('correlation','fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[ttt;inDir]';
save(fn,'xxx','-ascii');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


fg=figure(25);
hold off
xdata=ttt(1:dist15micros);
ydata=log(inDir(1:dist15micros));
plot(xdata,ydata,'.b','markersize',20);
hold on
ttl=' dir corr in dist (semilog)';
title(ttl,'fontsize',16);
ylabel('log(correlation)','fontsize',16);

p=polyfit(ttt(1:dist15micros),log(inDir(1:dist15micros)),1);
fun=@(x) p(1)*x+p(2);
plot([ttt(1) ttt(dist15micros)],[fun(ttt(1)) fun(ttt(dist15micros))],'--r','linewidth',2);
txt=[num2str(p(1),3) 'r+' num2str(p(2),3)];
xxx=ttt(floor(dist15micros/3));
yyy=0.5*fun(ttt(floor(dist3micros/3)));
xlabel(['r [um]  fit:' txt],'fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[xdata;ydata;fun(xdata)]';
save(fn,'xxx','-ascii');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%vorticity
fg=figure(26);
hold off
plot(ttt,inVort,'linewidth',2);
hold on
plot([ttt(1) ttt(end)],[0 0],':k','linewidth',2);
ttl=' vorticity corr in dist';
title(ttl,'fontsize',16);
neg0=floor(find(inVort<=0,1,'first'));
xlabel(['r [um]  first zero @' num2str(ttt(neg0))],'fontsize',16);
ylabel('correlation','fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[ttt;inVort]';
save(fn,'xxx','-ascii');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


fg=figure(27);
hold off
xdata=ttt(1:dist15micros);
ydata=log(inVort(1:dist15micros));
plot(xdata,ydata,'.b','markersize',20);
hold on
ttl=' vorticity corr in dist (semilog)';
title(ttl,'fontsize',16);
ylabel('log(correlation)','fontsize',16);

p=polyfit(ttt(1:dist3micros),log(inVort(1:dist3micros)),1);
fun=@(x) p(1)*x+p(2);
plot([ttt(1) ttt(dist3micros)],[fun(ttt(1)) fun(ttt(dist3micros))],'--r','linewidth',2);
txt=[num2str(p(1),3) 'r+' num2str(p(2),3)];
xxx=ttt(floor(dist3micros/3));
yyy=1.5*fun(ttt(floor(dist3micros/3)));
xlabel(['r [um]  fit:' txt],'fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[xdata;ydata;fun(xdata)]';
save(fn,'xxx','-ascii');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%weighted speed
fg=figure(32);
hold off
plot(ttt,inSpeedW,'linewidth',2);
hold on
plot([ttt(1) ttt(end)],[0 0],':k','linewidth',2);
ttl=' speed corr in dist weighted';
title(ttl,'fontsize',16);
xlabel('r [um]','fontsize',16);
ylabel('correlation','fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[ttt;inSpeedW]';
save(fn,'xxx','-ascii');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


fg=figure(33);
neg0=floor(find(inSpeedW<0,1,'first'));
hold off
xdata=ttt(1:dist15micros);
ydata=log(inSpeedW(1:dist15micros));
plot(xdata,ydata,'.b','markersize',20);
hold on
ttl=' speed corr in dist  weighted (semilog)';
title(ttl,'fontsize',16);
ylabel('log(correlation)','fontsize',16);

p=polyfit(ttt(1:dist15micros),log(inSpeedW(1:dist15micros)),1);
fun=@(x) p(1)*x+p(2);
plot([ttt(1) ttt(dist15micros)],[fun(ttt(1)) fun(ttt(dist15micros))],'--r','linewidth',2);
txt=[num2str(p(1),3) 'r+' num2str(p(2),3)];
xxx=ttt(floor(dist15micros/3));
yyy=1.5*fun(ttt(floor(dist15micros/3)));
xlabel(['r [um]   fit=' txt],'fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[xdata;ydata;fun(xdata)]';
save(fn,'xxx','-ascii');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%weighted direction
fg=figure(34);
hold off
plot(ttt,inDirW,'linewidth',2);
hold on
plot([ttt(1) ttt(end)],[0 0],':k','linewidth',2);
ttl=' dir corr in dist weighted';
title(ttl,'fontsize',16);
neg0=floor(find(inDirW<=0,1,'first'));
xlabel(['r [um]  first zero @' num2str(ttt(neg0))],'fontsize',16);
ylabel('correlation','fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[ttt;inDirW]';
save(fn,'xxx','-ascii');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


fg=figure(35);
hold off
xdata=ttt(1:dist15micros);
ydata=log(inDirW(1:dist15micros));
plot(xdata,ydata,'.b','markersize',20);
hold on
ttl=' dir corr in dist weighted (semilog)';
title(ttl,'fontsize',16);
ylabel('log(correlation)','fontsize',16);

p=polyfit(ttt(1:dist15micros),log(inDirW(1:dist15micros)),1);
fun=@(x) p(1)*x+p(2);
plot([ttt(1) ttt(dist15micros)],[fun(ttt(1)) fun(ttt(dist15micros))],'--r','linewidth',2);
txt=[num2str(p(1),3) 'r+' num2str(p(2),3)];
xxx=ttt(floor(dist15micros/3));
yyy=0.5*fun(ttt(floor(dist3micros/3)));
xlabel(['r [um]  fit:' txt],'fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[xdata;ydata;fun(xdata)]';
save(fn,'xxx','-ascii');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%weighted vorticity
fg=figure(36);
hold off
plot(ttt,inVortW,'linewidth',2);
hold on
plot([ttt(1) ttt(end)],[0 0],':k','linewidth',2);
ttl=' vorticity corr in dist weighted';
title(ttl,'fontsize',16);
neg0=floor(find(inVortW<=0,1,'first'));
xlabel(['r [um]  first zero @' num2str(ttt(neg0))],'fontsize',16);
ylabel('correlation','fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[ttt;inVortW]';
save(fn,'xxx','-ascii');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


fg=figure(37);
hold off
xdata=ttt(1:dist15micros);
ydata=log(inVortW(1:dist15micros));
plot(xdata,ydata,'.b','markersize',20);
hold on
ttl=' vorticity corr in dist weighted (semilog)';
title(ttl,'fontsize',16);
ylabel('log(correlation)','fontsize',16);

p=polyfit(ttt(1:dist3micros),log(inVortW(1:dist3micros)),1);
fun=@(x) p(1)*x+p(2);
plot([ttt(1) ttt(dist3micros)],[fun(ttt(1)) fun(ttt(dist3micros))],'--r','linewidth',2);
txt=[num2str(p(1),3) 'r+' num2str(p(2),3)];
xxx=ttt(floor(dist3micros/3));
yyy=1.5*fun(ttt(floor(dist3micros/3)));
xlabel(['r [um]  fit:' txt],'fontsize',16);

fn=['results/' ttlHead ttl '.tif'];
print(fg, '-r80', '-dtiff', fn);
close(fg);

fn=['results/' ttlHead ttl '.txt'];
xxx=[xdata;ydata;fun(xdata)]';
save(fn,'xxx','-ascii');



