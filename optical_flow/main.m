clear all

load('movie_path_sim500');

numberOfFrames=500;

cutoff=50; %Amit
%cutoff=100; %Sivan

%fileNameHeader='G:\gil\data\projects\bacteria\memi\Gil\Gil-Sivan\50-50 WT IM 0801\50-50 WT IM 0801-';
%fileNameHeader='G:\gil\data\projects\bacteria\memi\Gil\Gil-Sivan\50-50 WT IM 0801 no2\50-50 WT IM 0801 no2-';
fileNameHeader='G:\gil\data\projects\bacteria\memi\Gil\Gil-Amit\tif\(num 2) wt-274 18 hr with shake-00001\(num 2) wt-274 18 hr with shake-';

nDigits=5; %Sivan
%nDigits=5; %Amit

createMovie=true;


frwrd=1;
recalcEvery=1;
whiteThreshold=320;
diluteQuiverFactor=16;  %even
memory=0.7;
centeralWeight=0.5;
maxNorm2=cutoff*cutoff;
nnn=[0 1];

bins=0:0.1:cutoff;
h=zeros(1,length(bins));

h_path_movie = figure(1);

%open one test image for size
if nDigits==4
    im=imread([fileNameHeader num2str(fr,'%0.4i') '.tif']);
else
    im=imread([fileNameHeader num2str(fr,'%0.5i') '.tif']);
end
data.pu=zeros(numberOfFrames-frwrd,size(im,1),size(im,2),'float');
data.pv=zeros(numberOfFrames-frwrd,size(im,1),size(im,2),'float');

%calculate flow maps
%------------------------
for fr=1:1:numberOfFrames-frwrd
    if mod(fr,recalcEvery)==0
        if nDigits==4
            im1=imread([fileNameHeader num2str(fr,'%0.4i') '.tif']);
            im2=imread([fileNameHeader num2str(fr+frwrd,'%0.4i') '.tif']);
        else
            im1=imread([fileNameHeader num2str(fr,'%0.5i') '.tif']);
            im2=imread([fileNameHeader num2str(fr+frwrd,'%0.5i') '.tif']);
        end
        [u,v]=HS(im1,im2,1,100,zeros(size(im1(:,:,1))),zeros(size(im2(:,:,1))),0);
                
        is=diluteQuiverFactor/2:diluteQuiverFactor:size(im1,1);
        js=diluteQuiverFactor/2:diluteQuiverFactor:size(im1,2);
        [iii,jjj]=meshgrid(js,is);
        pu=centeralWeight*u(is,js) + (1-centeralWeight)*( u(is+1,js) + u(is-1,js) + u(is,js+1) + u(is,js-1) + u(is+1,js+1) + u(is-1,js-1) + u(is-1,js+1) + u(is+1,js-1) );
        pv=centeralWeight*v(is,js) + (1-centeralWeight)*( v(is+1,js) + v(is-1,js) + v(is,js+1) + v(is,js-1) + v(is+1,js+1) + v(is-1,js-1) + v(is-1,js+1) + v(is+1,js-1) );
        
        pu=centeralWeight*pu + (1-centeralWeight)*( circshift(pu,[0 1]) + circshift(pu,[0 -1]) + circshift(pu,[1 0]) + circshift(pu,[-1 0]) );
        pv=centeralWeight*pv + (1-centeralWeight)*( circshift(pv,[0 1]) + circshift(pv,[0 -1]) + circshift(pv,[1 0]) + circshift(pv,[-1 0]) );
        pu=pu/frwrd;
        pv=pv/frwrd;
        
        norm2=pu.^2+pv.^2;        
        pu(norm2>maxNorm2)=pu(norm2>maxNorm2)./sqrt(norm2(norm2>maxNorm2)/maxNorm2);
        pv(norm2>maxNorm2)=pv(norm2>maxNorm2)./sqrt(norm2(norm2>maxNorm2)/maxNorm2);

        if fr>1
            pv=(1-memory)*pv+memory*pastPv;
            pu=(1-memory)*pu+memory*pastPu;
        end
%         pu(13,20)=pu(13,20)*0.5;
%         pv(13,20)=pv(13,20)*0.5;
        
        cim1=im1(is,js);
        idx=find(cim1<whiteThreshold);
        %idxr=find(cim1<whiteThreshold & pu*nnn(1)+pv*nnn(2)>=0);
        %idxl=find(cim1<whiteThreshold & pu*nnn(1)+pv*nnn(2)<0);
        norms=sqrt(pu.^2+pv.^2);
        idxr=find(cim1<whiteThreshold & norms>=0.4*cutoff);
        idxb=find(cim1<whiteThreshold & norms<0.4*cutoff & norms>=0.15*cutoff);
        idxg=find(cim1<whiteThreshold & norms<0.15*cutoff);
        
        pastPu=pu;
        pastPv=pv;
        
        h=h+hist(reshape(norms,1,size(norms,1)*size(norms,2)),bins);

        
        disp(fr);
    end
    
    if nDigits==4
        im=imread([fileNameHeader num2str(fr,'%0.4i') '.tif']);
    else
        im=imread([fileNameHeader num2str(fr,'%0.5i') '.tif']);
    end
    
    imshow(im);
    hold on
    
    %quiver(iii(idx),jjj(idx),pu(idx),pv(idx),1.0,'r','linewidth',2);
    quiver(iii(idxr),jjj(idxr),pu(idxr),pv(idxr),0.0,'r','linewidth',2);
    quiver(iii(idxb),jjj(idxb),pu(idxb),pv(idxb),0.0,'y','linewidth',2);
    quiver(iii(idxg),jjj(idxg),pu(idxg),pv(idxg),0.0,'g','linewidth',2);
    
    movie_path_sim(i) = getframe(h_path_movie); %#ok
    i=i+1;    
end %for fr=1:238-frwrd
save('data');



movie2avi(movie_path_sim,'movieOutRGB.avi','compression','none');
close(h_path_movie);

figure(3)
bar(bins,h)
axis([0 cutoff 0 max(h)])

