%calculate OF.
%save it to data.mat
%clear all

%load('movie_path_sim500');



data.maxSpeed=100; %Amit
frwrd=1;
memory=0.7;
centeralWeight=0.6;


data.nFrames=numberOfFrames-frwrd;
bins=0:0.1:data.maxSpeed;
data.bins=bins;
%data.h=zeros(1,length(bins));
accuracyString=['%0.' num2str(data.nDigits) 'i'];

%open one test image for size
fr=1;
data.fileNameHeader=[data.mainDirectory '/' data.subFolderName];
if data.nDigits>0
    fn=[data.fileNameHeader '/' data.experimentName '-' num2str(data.startFrame,accuracyString) '.tif'];
else
    fn=[data.fileNameHeader '/' data.experimentName num2str(data.startFrame) '.tif'];
end
im1=imread(fn);
if data.crop(1)>0
    im1=im1(data.crop(1):data.crop(2),data.crop(3):data.crop(4));
end

data.frameSize=size(im1);
is=data.diluteQuiverFactor/2:data.diluteQuiverFactor:size(im1,1);
js=data.diluteQuiverFactor/2:data.diluteQuiverFactor:size(im1,2);
[data.iii,data.jjj]=meshgrid(js,is);
data.vfsize=[size(data.iii,1) size(data.iii,2)];
data.pu=zeros(data.nFrames,data.vfsize(1),data.vfsize(2),'single');
data.pv=zeros(data.nFrames,data.vfsize(1),data.vfsize(2),'single');


%calculate flow maps
%------------------------

fr=data.startFrame;
for frNum=1:data.nFrames/data.skipFrames
    
    %fn=[data.fileNameHeader '/' data.experimentName '/' data.experimentName '-' num2str(fr+data.startFrame-1,accuracyString) '.tif'];
    if data.nDigits>0
        fn=[data.fileNameHeader '/' data.experimentName '-' num2str(fr,accuracyString) '.tif'];
    else
        fn=[data.fileNameHeader '/' data.experimentName num2str(fr) '.tif'];
    end
    im1=imread(fn);
    if data.crop(1)>0
        im1=im1(data.crop(1):data.crop(2),data.crop(3):data.crop(4));
    end
    if data.nDigits>0
        fn=[data.fileNameHeader '/' data.experimentName '-' num2str(fr+data.skipFrames,accuracyString) '.tif'];
    else
        fn=[data.fileNameHeader '/' data.experimentName num2str(fr+data.skipFrames) '.tif'];
    end
    im2=imread(fn);
    if data.crop(1)>0
        im2=im2(data.crop(1):data.crop(2),data.crop(3):data.crop(4));
    end
    
    [u,v]=HS(im1,im2,1,100,zeros(size(im1(:,:,1))),zeros(size(im2(:,:,1))),0);
    %     u=ones(size(im1));
    %     v=ones(size(im1));
    
    u=(u+circshift(u,[0 1]) + circshift(u,[0 -1]) + circshift(u,[1 0]) + circshift(u,[-1 0]) )/5;
    v=(v+circshift(v,[0 1]) + circshift(v,[0 -1]) + circshift(v,[1 0]) + circshift(v,[-1 0]) )/5;
    u=(u+circshift(u,[0 1]) + circshift(u,[0 -1]) + circshift(u,[1 0]) + circshift(u,[-1 0]) )/5;
    v=(v+circshift(v,[0 1]) + circshift(v,[0 -1]) + circshift(v,[1 0]) + circshift(v,[-1 0]) )/5;
    u=(u+circshift(u,[0 1]) + circshift(u,[0 -1]) + circshift(u,[1 0]) + circshift(u,[-1 0]) )/5;
    v=(v+circshift(v,[0 1]) + circshift(v,[0 -1]) + circshift(v,[1 0]) + circshift(v,[-1 0]) )/5;
    
    pu=u(is,js);
    pv=v(is,js);
    
    %pu=centeralWeight*u(is,js) + (1-centeralWeight)*( u(is+1,js) + u(is-1,js) + u(is,js+1) + u(is,js-1) + u(is+1,js+1) + u(is-1,js-1) + u(is-1,js+1) + u(is+1,js-1) )/8;
    %pv=centeralWeight*v(is,js) + (1-centeralWeight)*( v(is+1,js) + v(is-1,js) + v(is,js+1) + v(is,js-1) + v(is+1,js+1) + v(is-1,js-1) + v(is-1,js+1) + v(is+1,js-1) )/8;
    
    pu=centeralWeight*pu + (1-centeralWeight)*( circshift(pu,[0 1]) + circshift(pu,[0 -1]) + circshift(pu,[1 0]) + circshift(pu,[-1 0]) )/4;
    pv=centeralWeight*pv + (1-centeralWeight)*( circshift(pv,[0 1]) + circshift(pv,[0 -1]) + circshift(pv,[1 0]) + circshift(pv,[-1 0]) )/4;
    %pu=pu*(100/data.fps/frwrd/1.6);
    %pv=pv*(100/data.fps/frwrd/1.6);
    %pu=pu*(100/data.fps/frwrd/data.diluteQuiverFactor/data.ppf*data.windowSize);
    %pv=pv*(100/data.fps/frwrd/data.diluteQuiverFactor/data.ppf*data.windowSize);
    pu=pu*(data.windowSize*data.fps/frwrd/data.ppf);
    pv=pv*(data.windowSize*data.fps/frwrd/data.ppf);
    
    norms=sqrt(pu.^2+pv.^2);
    pu(norms>data.maxSpeed)=(pu(norms>data.maxSpeed)./norms(norms>data.maxSpeed))*data.maxSpeed;
    pv(norms>data.maxSpeed)=(pv(norms>data.maxSpeed)./norms(norms>data.maxSpeed))*data.maxSpeed;
    
    if frNum>1
        pv=(1-memory)*pv+memory*pastPv;
        pu=(1-memory)*pu+memory*pastPu;
    end
    
    data.pu(frNum,:,:)=pu;
    data.pv(frNum,:,:)=pv;
    
    pastPu=pu;
    pastPv=pv;
    
    %data.h=data.h+hist(reshape(norms,1,size(norms,1)*size(norms,2)),bins);
    
    disp(fr);
    fr=fr+data.skipFrames;
    
end %for fr=1:1:data.nFrames

speeds=sqrt(data.pu.^2+data.pv.^2);
speedsArray=reshape(speeds,(numberOfFrames-1)*data.vfsize(1)*data.vfsize(2),1);
data.h=hist(speedsArray,bins);

save(['OFdata/data_' data.experimentName '_' num2str(data.startFrame) '.mat'],'data');




