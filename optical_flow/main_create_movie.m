%clear all

%load('data.mat');
%load('emptyMovie500.mat');
load(['OFdata\data_' data.experimentName '_' num2str(data.startFrame) '.mat'],'data');
disp(data.experimentName);

nMovie=1;
ttlLarge=['results\' data.experimentName ' - start at ' num2str(data.startFrame) '. coloring=' colorAccordingTo ' part' num2str(nMovie) '.avi'];
movie_path_sim = VideoWriter(ttlLarge);
open(movie_path_sim);


%includeOriginalVideo=false;

plotQuiverSize=1.7;
redDirection=[1 1]; %[right up]

%h_path_movie = figure(1);
scrsz = get(0,'ScreenSize');
if colorAccordingTo(1)=='t'
    im3=zeros(data.frameSize(1),data.frameSize(1),3);
    h_path_movie = figure('Position',[scrsz(3)/8 scrsz(4)/4 1400 600]); %[left, bottom, width, height]
else
    h_path_movie = figure('Position',[scrsz(3)/2 scrsz(4)/4 700 700]); %[left, bottom, width, height]
end

bins=0:0.01:data.maxSpeed;
sumH=cumsum(data.h)/sum(data.h);
[~,i]=find(sumH<0.33 & circshift(sumH,[0 -1])>0.33);
c1=bins(i);
[~,i]=find(sumH<0.66 & circshift(sumH,[0 -1])>0.66);
c2=bins(i);
accuracyString=['%0.' num2str(data.nDigits) 'i'];

is=data.diluteQuiverFactor/2:data.diluteQuiverFactor:data.frameSize(1);
js=data.diluteQuiverFactor/2:data.diluteQuiverFactor:data.frameSize(2);
[iii,jjj]=meshgrid(js,is);

%calc curls
if colorAccordingTo(1)=='v' || colorAccordingTo(1)=='l' || colorAccordingTo(1)=='t'
    curls=zeros(data.nFrames,size(iii,1),size(iii,2));
    for fr=1:1:data.nFrames
        pu=reshape(data.pu(fr,:,:),size(data.pu,2),size(data.pu,3));
        pv=reshape(data.pv(fr,:,:),size(data.pu,2),size(data.pu,3));
        
        %curls(fr,:,:)=curl(iii,jjj,pu,pv);
        curls(fr,:,:)=curl(pu,pv)/data.dx;
    end
    maxcurl=max(max(max(abs(curls))));
end

nfr=data.nFrames;
if nfr>maxNframes+startAt
    nfr=maxNframes+startAt;
end

nMovie=1;
f=1;
for fr=startAt:1:nfr
    %disp(fr);
    
    pu=reshape(data.pu(fr,:,:),size(data.pu,2),size(data.pu,3));
    pv=reshape(data.pv(fr,:,:),size(data.pu,2),size(data.pu,3));
    
    norms=sqrt(pu.^2+pv.^2);
    ch=colorAccordingTo(1);
    hold off
    switch ch
        case 's'
            idxr=find(norms>=c2);
            idxy=find(norms<c2 & norms>=c1);
            idxg=find(norms<c1);
            idxk=[];
            %xlabel('red=fast, purple=mid, green=slow','fontsize',16);
        case 'd'
            idxr=find(pu*redDirection(1)-pv*redDirection(2)>=0);
            idxy=find(pu*redDirection(1)-pv*redDirection(2)<0);
            idxg=[];
            %xlabel(['red=[' num2str(redDirection(1)) ',' num2str(redDirection(2)) '], purple=other direction'],'fontsize',16);
        case 'v'
            curls1=reshape(curls(fr,:,:),size(curls,2),size(curls,3));
            idxr=find(curls1>=maxcurl/20);
            idxy=find(curls1<=-maxcurl/20);
            idxg=find(abs(curls1)<maxcurl/20);
            idxk=[];
            %xlabel('red=CCW, purple=CW, green=zero','fontsize',16);
        case 'a'
            redDirection(1)=mean(mean(pu));
            redDirection(2)=mean(mean(pv));
            redDirection=redDirection/norm(redDirection);
            cosine=(pu*redDirection(1)+pv*redDirection(2))./norms;
            idxr=find(cosine>=cos(pi/4));
            idxy=find(cosine<=-cos(pi/4));
            idxg=find(cosine<cos(pi/4) & cosine>-cos(pi/4));
            idxk=[];
            %xlabel('red=with average, purple=against, green=perpendicular','fontsize',16);
        case {'l','t'}
            curls1=reshape(curls(fr,:,:),size(curls,2),size(curls,3));
            idxr=[];
            idxy=[];
            idxg=[];
            idxk=find(curls1~=0);
            power=0.4;
            if ch=='t'
                subplot(1,2,2);
            end
            imagesc(iii(1,:),jjj(:,1)',sign(curls1).*abs(curls1).^power,[-maxcurl^power maxcurl^power]);
            hold on
        otherwise
            error('NA colorAccordingTo');
    end
    
    if includeOriginalVideo==true || ch=='t'
        fn=[data.mainDirectory '\' data.subFolderName '\' data.experimentName '-' num2str(fr+data.startFrame-1,accuracyString) '.tif'];
        im=imread(fn);
        if ch=='t'
            subplot(1,2,1);
            if data.crop(1)>0
                cim=im(data.crop(1):data.crop(2),data.crop(3):data.crop(4));
            else
                cim=im;
            end
            im3(:,:,1)=double(cim)/256;
            im3(:,:,2)=double(cim)/256;
            im3(:,:,3)=double(cim)/256;
            imagesc(im3);
            subplot(1,2,2);
        else
            imshow(im);
        end
        hold on
    elseif ch~='l'
        hold off;
    end
    
    if ch~='l' && ch~='t'
        hold off;
        quiver(iii(idxr),jjj(idxr),plotQuiverSize*pu(idxr),plotQuiverSize*pv(idxr),0.0,'color',[0.7 0 0],'linewidth',1.5);
        hold on
        quiver(iii(idxy),jjj(idxy),plotQuiverSize*pu(idxy),plotQuiverSize*pv(idxy),0.0,'color',[0 0 0.7],'linewidth',1.5);
        quiver(iii(idxg),jjj(idxg),plotQuiverSize*pu(idxg),plotQuiverSize*pv(idxg),0.0,'color',[0 0.7 0],'linewidth',1.5);
        quiver(iii(idxk),jjj(idxk),plotQuiverSize*pu(idxk),plotQuiverSize*pv(idxk),0.0,'k','linewidth',1.5);
    else
        quiver(iii(idxk),jjj(idxk),plotQuiverSize*pu(idxk),plotQuiverSize*pv(idxk),0.0,'k','linewidth',1.5);
    end
    
    if ch=='t'
        subplot(1,2,1);
    end
    title([num2str((fr+data.startFrame-startAt-1)/data.fps,'%.2f') ' secs'],'fontsize',16);
    
    if includeOriginalVideo==false
        axis([0 data.frameSize(1) 0 data.frameSize(2)]);
    end
    
    set(gca,'xtick',[]);
    set(gca,'ytick',[]);
    
    %scale
    hold on
    plot([50 150],[50 50],'-','linewidth',7,'color',[0.8 0 0]);
    text(50,90,'10\mum','color',[0.8 0 0],'fontsize',20);
    
    switch ch
        case 's'
            xlabel('red=fast, blue=mid, green=slow','fontsize',16);
        case 'd'
            xlabel(['red=[' num2str(redDirection(1)) ',' num2str(redDirection(2)) '], blue=other direction'],'fontsize',16);
        case 'v'
            xlabel('red=CW, blue=CCW, green=zero','fontsize',16);
        case 'a'
            xlabel('red=with average, blue=against, green=perpendicular','fontsize',16);
        case 'l'
            xlabel('red=CW, blue=CCW, green=zero','fontsize',16);
        case 't'
            subplot(1,2,2);
            xlabel('red=CW, blue=CCW, green=zero','fontsize',16);
            axis([0 data.frameSize(1) 0 data.frameSize(2)]);
            set(gca,'xtick',[]);
            set(gca,'ytick',[]);
            %axis square
        otherwise
            error('NA colorAccordingTo');
    end
    
    frm = getframe(h_path_movie);
    writeVideo(movie_path_sim,frm);
    pause(0.01);
    
    f=f+1;
    if f==blocks+1 
        nMovie=nMovie+1;
        close(movie_path_sim);
        %movie2avi(movie_path_sim,ttlLarge,'compression','none');
        %close(h_path_movie);
        %clear movie_path_sim;
        if colorAccordingTo(1)=='t'
            h_path_movie = figure('Position',[scrsz(3)/8 scrsz(4)/4 1400 600]); %[left, bottom, width, height]
        else
            h_path_movie = figure('Position',[scrsz(3)/2 scrsz(4)/4 700 700]); %[left, bottom, width, height]
        end
        f=1;
        ttlLarge=['results\' data.experimentName ' - start at ' num2str(data.startFrame) '. coloring=' colorAccordingTo ' part' num2str(nMovie) '.avi'];
        
        movie_path_sim = VideoWriter(ttlLarge);
        open(movie_path_sim);
    end
end %for fr=1:data.nFrames

close(movie_path_sim);
close(h_path_movie);
nMovie=nMovie-1;

%add line to conversion batch file
% currentFolder = pwd;
% inputDir=[currentFolder '\'];
% outputDir=[currentFolder '\results\'];
% ttlCompressed=[data.experimentName ' - start at ' num2str(data.startFrame) '. coloring=' colorAccordingTo '.avi'];

% quality=5;
% fprintf(fid,'ffmpeg  -i "concat:');
% for i=1:nMovie
%     ttlLarge=['results\' data.experimentName ' - start at ' num2str(data.startFrame) '. coloring=' colorAccordingTo ' part' num2str(i) ' LARGE.avi'];
%     fprintf(fid,'%s',[inputDir ttlLarge]);
%     if i<nMovie
%         fprintf(fid,'|');
%     end
% end
% fprintf(fid,'%s\n',[ '" -q ' num2str(quality) ' "' outputDir ttlCompressed '"  ']);
% for i=1:nMovie
%     ttlLarge=['results\' data.experimentName ' - start at ' num2str(data.startFrame) '. coloring=' colorAccordingTo ' part' num2str(i) ' LARGE.avi'];
%     fprintf(fid,'%s\n',['del "' inputDir ttlLarge '"  ']);
% end

%fprintf(fid,'%s\n',['ffmpeg  -i "' inputDir ttlLarge '" -q ' num2str(quality) ' "' outputDir ttlCompressed '"  ']);
%fprintf(fid,'%s\n',['del "' inputDir ttlLarge '"  ']);

